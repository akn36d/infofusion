#%%
import json
from abc import ABC
from itertools import filterfalse, product

import dill as pickle
from albumentations import (CenterCrop, Compose, Normalize, PadIfNeeded,
                            RandomCrop, Resize)
from albumentations import to_dict as txm_to_dict
from git import config

from aug_templates import get_train_aug_list, get_val_aug_list

diag_names = ["MEL","NV","BKL"]       
feat_names = [
    'pigment_network', 
    'dots_globules', 
    'negative_network', 
    'streaks_incl_rad_streaming',	                       
    #'milia_like_cyst', 
    'granularity', 
    'ker_plugs', 
    'wobble', 
    'scale', 
    'blood', 
    'atyp_net',
    'blood_vessels', 
    'scarlike_regression',
]


def grid_item(grid_inst, key):
    '''
        Function to be used in restriction lamdas for the config dict iterator
    '''
    for elem in grid_inst:
        if key in elem:
            return elem[key] 
        else:
            continue
        return None 

class ConfigParserLoader():
    '''
        My config parser and loader class which adds specific keys which depend on the
        given config generated by the `Config_grid_gen` iterator. An instance of this 
        will be present in `Config_grid_gen`.
        The function `call_all_loaders()` will be called just before the iterator
        yields a config.
    '''
    def __init__(self, config_dict_iterator):
        self.config_dict_iterator = config_dict_iterator
    
    def call_all_loaders(self, config_dict_inst):
        self.load_augs(config_dict_inst)
        self.load_loss_weights(config_dict_inst)
        self.load_lrscheduler(config_dict_inst)
        self.load_end_fc_scalers(config_dict_inst)
    
    def load_augs(self, config_dict):
        config_dict_addendum = {}
        # Augmentation prefixes and postfixes which deal with image size
        if config_dict['input_size'] == 300:
            config_dict['data_dir'] = 'isic19_300'
            train_aug_prefix = []
        elif config_dict['input_size'] == 368:
            config_dict['data_dir'] = 'isic19_368'
            train_aug_prefix = []
        elif config_dict['input_size'] == 512:
            config_dict['data_dir'] = 'isic19_512'
            train_aug_prefix = []
        else:
            print('Invalid input size `{}`.'.format(config_dict['input_size']))
            assert config_dict['input_size'] in [300, 368, 512]

        train_aug_postfix = []

        val_aug_prefix = train_aug_prefix
        val_aug_postfix = []

        train_aug_list = Compose(train_aug_prefix + get_train_aug_list() + train_aug_postfix)
        val_aug_list = Compose(val_aug_prefix + get_val_aug_list() + val_aug_postfix)

        config_dict_addendum['augmentation_txms'] = {}
        config_dict_addendum['augmentation_txms']['train'] = txm_to_dict(train_aug_list)
        config_dict_addendum['augmentation_txms']['val'] = txm_to_dict(val_aug_list)

        config_dict.update(config_dict_addendum)

    def load_loss_weights(self, config_dict):
        config_dict_addendum = {}
        if config_dict['arch_type'] == 'heirarchical':
            config_dict_addendum['loss_weight_dict'] = {}
            config_dict_addendum['loss_weight_dict']['loss-total-pass2'] = {
                'loss-diags': 1,
                'loss-feats': 1,
                'loss-feats-l1': 1
            }
            config_dict_addendum['loss_weight_dict']['loss-total-pass1'] = {
                'loss-diags': 1,
                'loss-feats': 1,
                'loss-fake-feats': 1,
                'loss-feats-l1': 1
            }

        if config_dict['arch_type'] == 'multi-input':
            config_dict_addendum['loss_weight_dict'] = {}
            config_dict_addendum['loss_weight_dict'] = {'loss-diags': 1}

        if config_dict['arch_type'] == 'diags-only':
            pass

        if config_dict['arch_type'] == 'diags-feats':
            config_dict_addendum['loss_weight_dict'] = {}
            config_dict_addendum['loss_weight_dict'] = {
                'loss-diags': 1,
                'loss-feats': 1,
                'loss-feats-l1': 1
            }

        if config_dict['arch_type'] == 'diags-feats-pcgrad':
            config_dict_addendum['loss_weight_dict'] = {}
            config_dict_addendum['loss_weight_dict'] = {
                'loss-diags': 1,
                'loss-feats': 1,
            }

        config_dict.update(config_dict_addendum)

    def load_lrscheduler(self, config_dict):
        itr = self.config_dict_iterator
         
        config_dict_addendum = {}
        if config_dict['arch_type'] == 'heirarchical':
            # Setup defaults for lr scheduler params
            config_dict_addendum['lr_scheduler_config'] = {}
            config_dict_addendum['lr_scheduler_config'] = {
                'type' : itr.config_dict_constants['lr_scheduler_type'],
                'lr_scheduler_patience': itr.config_dict_constants['lr_scheduler_patience'],
                'init_lr' : itr.config_dict_constants['init_lr'],
                'lr_scheduled_on' : itr.config_dict_constants['lr_scheduled_on']
            }  
            config_dict_addendum['lr_scheduler_config']['base'] = {
                'init_lr' : itr.config_dict_constants['init_lr'],
            }  
            config_dict_addendum['lr_scheduler_config']['diags'] = {
                'init_lr' : itr.config_dict_constants['init_lr'],
            }  
            config_dict_addendum['lr_scheduler_config']['feats'] = {
                'init_lr' : itr.config_dict_constants['init_lr'],
            }  

        if config_dict['arch_type'] == 'multi-input':
            # Setup defaults for lr scheduler params
            config_dict_addendum['lr_scheduler_config'] = {}
            config_dict_addendum['lr_scheduler_config'] = {
                'type' : itr.config_dict_constants['lr_scheduler_type'],
                'lr_scheduler_patience': itr.config_dict_constants['lr_scheduler_patience'],
                'init_lr' : itr.config_dict_constants['init_lr'],
                'lr_scheduled_on' :  itr.config_dict_constants['lr_scheduled_on']
            }  
            config_dict_addendum['lr_scheduler_config']['base'] = {
                'init_lr' : itr.config_dict_constants['init_lr'],
            }  
            config_dict_addendum['lr_scheduler_config']['diags'] = {
                'init_lr' : itr.config_dict_constants['init_lr'],
            }  

        if config_dict['arch_type'] == 'diags-only':
            config_dict_addendum['lr_scheduler_config'] = {}
            config_dict_addendum['lr_scheduler_config'] = {
                'type' : itr.config_dict_constants['lr_scheduler_type'],
                'lr_scheduler_patience': itr.config_dict_constants['lr_scheduler_patience'],
                'init_lr' : itr.config_dict_constants['init_lr'],
                'lr_scheduled_on' :  itr.config_dict_constants['lr_scheduled_on']
            }  

        if config_dict['arch_type'] == 'diags-feats':
            config_dict_addendum['lr_scheduler_config'] = {}
            config_dict_addendum['lr_scheduler_config'] = {
                'type' : itr.config_dict_constants['lr_scheduler_type'],
                'lr_scheduler_patience': itr.config_dict_constants['lr_scheduler_patience'],
                'init_lr' : itr.config_dict_constants['init_lr'],
                'lr_scheduled_on' : itr.config_dict_constants['lr_scheduled_on']
            }  
            config_dict_addendum['lr_scheduler_config']['base'] = {
                'init_lr' : itr.config_dict_constants['init_lr'],
            }  
            config_dict_addendum['lr_scheduler_config']['diags'] = {
                'init_lr' : itr.config_dict_constants['init_lr'],
            }  
            config_dict_addendum['lr_scheduler_config']['feats'] = {
                'init_lr' : itr.config_dict_constants['init_lr'],
            }  
            
        if config_dict['arch_type'] == 'diags-feats-pcgrad':
            config_dict_addendum['lr_scheduler_config'] = {}
            config_dict_addendum['lr_scheduler_config'] = {
                'type' : itr.config_dict_constants['lr_scheduler_type'],
                'lr_scheduler_patience': itr.config_dict_constants['lr_scheduler_patience'],
                'init_lr' : itr.config_dict_constants['init_lr'],
                'lr_scheduled_on' : itr.config_dict_constants['lr_scheduled_on']
            }  
            config_dict_addendum['lr_scheduler_config']['base'] = {
                'init_lr' : itr.config_dict_constants['init_lr'],
            }  
            config_dict_addendum['lr_scheduler_config']['diags'] = {
                'init_lr' : itr.config_dict_constants['init_lr'],
            }  
            config_dict_addendum['lr_scheduler_config']['feats'] = {
                'init_lr' : itr.config_dict_constants['init_lr'],
            }  
        config_dict.update(config_dict_addendum)


    def load_end_fc_scalers(self, config_dict):
        config_dict_addendum = {}
        if config_dict['arch_type'] == 'heirarchical':               
            config_dict_addendum['end_fc_lr_scaler'] = {}
            config_dict_addendum['end_fc_lr_scaler']['base'] = 1
            config_dict_addendum['end_fc_lr_scaler']['diags'] = 1
            config_dict_addendum['end_fc_lr_scaler']['feats'] = 1

        if config_dict['arch_type'] == 'multi-input':               
            config_dict_addendum['end_fc_lr_scaler'] = {}
            config_dict_addendum['end_fc_lr_scaler']['base'] = 1
            config_dict_addendum['end_fc_lr_scaler']['diags'] = 1

        if config_dict['arch_type'] == 'diags-only':                
            config_dict_addendum['end_fc_lr_scaler'] = 1

        if config_dict['arch_type'] == 'diags-feats':                
            config_dict_addendum['end_fc_lr_scaler'] = {}
            config_dict_addendum['end_fc_lr_scaler']['base'] = 1
            config_dict_addendum['end_fc_lr_scaler']['diags'] = 1
            config_dict_addendum['end_fc_lr_scaler']['feats'] = 1
            
        if config_dict['arch_type'] == 'diags-feats-pcgrad':                
            config_dict_addendum['end_fc_lr_scaler'] = {}
            config_dict_addendum['end_fc_lr_scaler']['base'] = 1
            config_dict_addendum['end_fc_lr_scaler']['diags'] = 1
            config_dict_addendum['end_fc_lr_scaler']['feats'] = 1

        config_dict.update(config_dict_addendum)


#%%
class Config_grid_gen():
    '''
        Generate a config grid using a json file. The json will have the following 
        fields with examples;

            - constant params in each call or to be used in `load_*()` calls
                'config_dict_constants': {
                    "const1": val1
                }
       
            - defaults and edge case handlers
                'config_dict_defaults': {
                    "default_list": [
                        "x1",
                        "x2"
                    ]
                }     

            - config attrs modified anytime after iterator has yielded the config dict
                config_dict_volatiles: {
                    "program_excecution_time": null
                }

            - the grid generation list
                grid_dict: {
                    "batch_size": [
                        10,
                        32
                    ],
                    "model_type": [
                        "model-0",
                        "model-1"
                    ],
                } 
    '''
    def __init__(self, config_init_json, iterator_checkpoint, config_checkpoint):
        self.index = 0
        self.iterator_checkpoint = iterator_checkpoint
        self.cofig_checkpoint = config_checkpoint
        self.config_init(config_init_json)
        
        # directly modified in grid generation
        self.config_dict_grid_inits = {}
        for key in self.grid_dict.keys():
            self.config_dict_grid_inits[key] = None     

    def gen_grid(self):
        grid_list = []
        for key, item in self.grid_dict.items():
            item = [{key:val} for val in item]
            grid_list.append(item)

        cart_prod = product(*grid_list)
        
        cart_prod_filtered = filterfalse(self.restriction_lambda, cart_prod )
        return cart_prod_filtered

    def config_init(self, config_init_json):
        with open(config_init_json) as file:
            config_init_dict = json.load(file)

        for key , value in config_init_dict.items():
            self.__setattr__(key, value)

    def generate_config_grid(self, restriction_lambda):
        '''
            Create the cofig grid. Needs to be called before 
            the iterator is used.

            NOTE : if restriction_lambda result is 
            false then yeild item from iterator
        '''
        self.restriction_lambda = restriction_lambda
        self.config_dict_grid_nodes = self.gen_grid()        
        self.cfg_parser_loader = ConfigParserLoader(self)

    def __iter__(self):            
        return self

    def __next__(self):
        config_dict = {}
        config_dict.update(self.config_dict_constants)
        config_dict.update(self.config_dict_defaults)
        config_dict.update(self.config_dict_grid_inits)
        config_dict.update(self.config_dict_volatiles)

        grid_nodes = next(self.config_dict_grid_nodes)
        for cfg in grid_nodes:
            config_dict.update(cfg)

        for var_key, sources_dict  in self.grid_vars.items():
            if len(sources_dict.keys()) > 1:
                print("Warning : Cannot have more than one config_dict key that the variable key to be added can depend on.")
                print("\t skipping modification of  {} for current generated grid node.".format(var_key))
                print("\t Current variable key dict is \n  {}".format(sources_dict))
                
            else:
                for src_key, src_dict in sources_dict.items():
                    config_dict[var_key] = src_dict[config_dict[src_key]]
                    
        # conditional loads for certain dict attrs
        self.cfg_parser_loader.call_all_loaders(config_dict_inst=config_dict)
        self.index += 1
        return config_dict




#%%
# cfg = Config_grid_gen('r08_multitask_cfg_init.json','it_test.pkl', 'cfg_test.json')

# def grid_restrictions(grid_inst): 
#     return (
#         ((grid_item(grid_inst, 'batch_size') > 16) and (grid_item(grid_inst, 'input_size') > 224))
#             or 
#         ((grid_item(grid_inst, 'arch_type') in ['diags-only', 'diags-feats-pcgrad']) and (grid_item(grid_inst, 'trainer_type') in ['ganlike']))
#             or 
#         ((grid_item(grid_inst, 'arch_type') == 'diags-feats') and (grid_item(grid_inst, 'trainer_type') is None))
#     )

# cfg.generate_config_grid(restriction_lambda=grid_restrictions)

# for c in iter(cfg):
#     a = c['arch_type']
#     b = c['trainer_type']
#     print('arch : {} , trn : {}'.format(a,b))

#%%         
