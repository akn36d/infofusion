# %%
# %matplotlib inline

from __future__ import division, print_function

import copy
import csv
import os
import sys
import time
from typing import ClassVar
import warnings
from collections import Counter
from pprint import pprint

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy
from pandas import DataFrame as df
from sklearn.model_selection import StratifiedShuffleSplit
from skmultilearn.model_selection import IterativeStratification
from skmultilearn.model_selection.measures import \
    get_combination_wise_output_matrix
from torch.utils import data

# from utils.data_utils import extract_csv_data, calc_class_weights, my_csv2df
from utils.dataset import ISIC_dataset
from utils.transforms import albu_aug_train, albu_aug_val
from utils.utils import *
# from sanity_checks import *

# %%
# LOAD DATA
csv_dir = os.path.join('csv_files')
fold_save_dir = os.path.join('./data_folds/5foldTrainVal_featbalanced')
csv_files = {}

csv_files['full'] = os.path.join(csv_dir, 'Final_dataset_to_train.csv')
csv_files['partial'] = os.path.join(csv_dir, 'anand_to_annote_ALL.csv')

data_dir = os.path.join(
    '/usr/local/home/akn36d/Downloads/ISIC_2019_Training_Input')

class_names = ["MEL", "NV", "BKL"]  # remove the 9th "UNK" class
non_discriminative_feat_names = [
    "pigment_network", "globules", "negative_network", "streaks", "milia_like_cyst"]

discriminative_feat_names = [
    'pigmt_net_typical_includes_pseudonet',
    'dots_globs_reg',
    'negative_network',
    'strks_or_rad_strming',
    'MLCs_cloud',
    'granularity',
    'ker_plugs',
    'wobble',
    'scale',
    'blood',
    'atyp_net',
    'bld_vessels',
    'scarlike_regress',
    'ann_gran',
    'irreg_dots_globs',
    'blotch',
    'homog_tan_yellowish',
    'frambes_cerebriform_fernlike_pattern',
    'rough_covered_with_irreg_hyperkeratosis'
]


# %%
# Extract train csv to df
full_dataset = ISIC_dataset(File_list_csv=csv_files['full'],
                            diag_list=class_names,
                            feat_list=discriminative_feat_names,
                            phase='val',
                            transform=None,
                            no_feats_class=False)

# %%
print('Class nums : ')
print(full_dataset.class_nums)

data_df = full_dataset.data_df
class_dfs = {}
for class_ in class_names:
    class_dfs[class_] = data_df[data_df[class_] == 1]


# %%
def get_orderwise_stats(stratification_on_arr, multilabel_names_list, order):
    cnt = Counter(combination for row in get_combination_wise_output_matrix(stratification_on_arr, order=order) for combination in row)
    # total_combo_cnt = 0
    combo_cnt_dict = {}
    for combo, count in cnt.items():
        combo = np.array(list(combo))
        # print(combo)
        check_key = np.sort(np.unique(combo)).tolist()
        check_key = tuple([multilabel_names_list[idx] for idx in check_key])
        if check_key not in combo_cnt_dict:
            combo_cnt_dict[check_key] = count
            # total_combo_cnt += count
    return combo_cnt_dict


# %% Iterative stratification
n_splits = 5
order = 2
test_size = 0.30
k_fold = IterativeStratification(n_splits=n_splits, order=order)

# classwise_fold = {}
train_final_folds = n_splits * [pd.DataFrame()]
val_final_folds = n_splits * [pd.DataFrame()]


# %%
for class_, class_df in class_dfs.items():
    stratification_on =  scipy.sparse.lil_matrix(class_df.loc[:,discriminative_feat_names].to_numpy())
    class_index_vals =  np.array(class_df.index.values.tolist()).transpose()
    fold_idx = 0
    for train_index, val_index in k_fold.split(class_df, stratification_on):

        train_diagfeat_discriminative_df  = class_df.iloc[train_index, :]
        val_diagfeat_discriminative_df = class_df.iloc[val_index, :]

        train_final_folds[fold_idx] = train_final_folds[fold_idx].append(train_diagfeat_discriminative_df)
        val_final_folds[fold_idx] =  val_final_folds[fold_idx].append(val_diagfeat_discriminative_df)
        fold_idx += 1


# %%
def get_orderwise_count_dict(set_df, set_name, multilabel_names_list, order):
    stratification_on_full_set = set_df.loc[:, discriminative_feat_names].to_numpy()
    stratification_on_for_mel = set_df[set_df['MEL'] == 1].loc[:, discriminative_feat_names].to_numpy()
    stratification_on_for_bkl = set_df[set_df['BKL'] == 1].loc[:, discriminative_feat_names].to_numpy()
    stratification_on_for_nv = set_df[set_df['NV'] == 1].loc[:, discriminative_feat_names].to_numpy()
    return {
        '{}_set'.format(set_name): get_orderwise_stats(stratification_on_full_set, multilabel_names_list, order),
        '{}_MEL'.format(set_name): get_orderwise_stats(stratification_on_for_mel, multilabel_names_list, order),
        '{}_BKL'.format(set_name): get_orderwise_stats(stratification_on_for_bkl, multilabel_names_list, order),
        '{}_NV'.format(set_name): get_orderwise_stats(stratification_on_for_nv, multilabel_names_list, order),
    }


# %%
combined_set_dict = {}

combined_set_dict.update(
    get_orderwise_count_dict(
        class_df, 'full', discriminative_feat_names, order=order
    )
)

for idx in range(n_splits):
    train_fold_df = train_final_folds[idx]
    val_fold_df = val_final_folds[idx]

    combined_set_dict.update(
        get_orderwise_count_dict(
            train_fold_df, 'train_Fold{}'.format(idx),
            discriminative_feat_names, order=order
        )
    )
    combined_set_dict.update(
        get_orderwise_count_dict(
            val_fold_df, 'val_Fold{}'.format(idx),
            discriminative_feat_names, order=order
        )
    )

    train_fold_df.to_csv(
        os.path.join(
            fold_save_dir,
            'train_featbalanced_{}fold{}.csv'.format(n_splits, idx)
        ),
        index=False, header=True
    )

    val_fold_df.to_csv(
        os.path.join(
            fold_save_dir,
            'val_featbalanced_{}fold{}.csv'.format(n_splits, idx)
        ),
        index=False, header=True
    )

combined_stats_df = pd.DataFrame(combined_set_dict).fillna(0.0)


# %%
sum_dict = {}

for idx in range(n_splits):
    train_fold_df = train_final_folds[idx]
    tr_s = train_fold_df.sum(numeric_only=True) 
    tr_s = tr_s[discriminative_feat_names] # / tr_s[discriminative_feat_names].sum()

    val_fold_df = val_final_folds[idx]
    vl_s = val_fold_df.sum(numeric_only=True)
    vl_s = vl_s[discriminative_feat_names] #/ vl_s[discriminative_feat_names].sum()

    sum_dict.update(
        {
            'tr_fld{}_feats'.format(idx): tr_s,
            'vl_fld{}_feats'.format(idx): vl_s,
        }
    )
    

# %%
hist_df = pd.DataFrame(sum_dict).fillna(0.0)
ax = hist_df.plot.area(stacked=False, figsize=(16, 6));

# We want to show all ticks...
ax.set_xticks(np.arange(len(discriminative_feat_names)));
# ... and label them with the respective list entries
ax.set_xticklabels(discriminative_feat_names);

# Rotate the tick labels and set their alignment.
plt.setp(ax.get_xticklabels(), rotation=90, ha="right",
         rotation_mode="anchor");


# %%
