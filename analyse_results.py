#%%
import json
import os

import numpy as np
import pandas as pd
import torch

from utils.data_utils import *
from utils.utils import *

# LOAD DATA
csv_dir = os.path.join('csv_files')
data_dir = os.path.join('/usr/local/home/akn36d/Downloads/ISIC_2019_Training_Input')
gt_dir = os.path.join('/usr/local/home/akn36d/Downloads/')

# class_names = ["MEL","NV","BCC","AK","BKL","DF","VASC","SCC"] # remove the 9th "UNK" class 
diag_names = ["MEL","NV","BKL"] 
# feat_names = ["pigment_network","globules","negative_network","streaks","milia_like_cyst"]
extended_feat_names = ['pigment_network', 'dots_globules', 'negative_network', 'streaks_incl_rad_streaming',	\
                       'milia_like_cyst', 'granularity', 'ker_plugs', 'wobble', 'scale', 'blood', 'atyp_net',   \
                       'blood_vessels', 'scarlike_regression']

### Setting up environment
#%%
# PRELIM Environment SETUP
config_fpath = os.path.join('Models/multilevel_with_feats_overtrain/diags_noPigNet_2020-11-03 16:47:22/configs/config_info.json')
# config_fpath = os.path.join('Models/multilevel_with_feats_overtrain/diags_model1_2020-10-18 12:17:59/configs/config_info.json')

# #### Setting GPU availability
#%%
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('GPU is available : {}'.format(torch.cuda.is_available()))
print(device)

with open(config_fpath, 'r') as f:
    config_dict = json.load(f)

### Training
#%%
task_type = config_dict['task_type'] 
diag_names = config_dict['diag_names']
feat_names = config_dict['feat_names']
no_feats_class = config_dict['no_feats_class']

# Saving the params used for training the specific model
num_folds = config_dict['num_folds']
model_type = config_dict['model_type'] 
input_size = config_dict['input_size'] 
tr_params_dict = config_dict['img_preprocessing_params']['tr_params_dict']
val_params_dict = config_dict['img_preprocessing_params']['val_params_dict']
freeze_len = config_dict['freeze_len']
use_normalization = config_dict['find_mean']
use_last_activation = config_dict['use_last_activation']

if config_dict['use_gpu']:
    device = torch.device('cuda')
else:
    device = torch.device('cpu')
device = torch.device('cpu')

environ_list = config_dict['environ_list']
model_weights_saved_dir = environ_list['save_weights_to_path']
log_path = environ_list['log_path']

#%%
for fold_num in range(config_dict['num_folds']):
    print('\n')
    print(70*"=")
    print("FOLD - {}".format(fold_num))
    print(70*"=")

    
    dataset_prefix = 'test'
    callback_prefix = 'preds-logger'
    callback_suffix = '_preds_log'

    logs = {}
    logs['feats'] = os.path.join(
                                    environ_list['log_path'],
                                    'Fold{}_log'.format(fold_num),
                                    '{}_feats-{}{}.csv'.format( 
                                                                dataset_prefix,
                                                                callback_prefix,
                                                                callback_suffix
                                                            )
                                )
    logs['diags'] = os.path.join(
                                    environ_list['log_path'],
                                    'Fold{}_log'.format(fold_num),
                                    '{}_diags-{}{}.csv'.format( 
                                                                dataset_prefix,
                                                                callback_prefix,
                                                                callback_suffix
                                                            )
                                )

    log_df = {}
    log_df['feats'] = pd.read_csv(logs['feats'])
    log_df['diags'] = pd.read_csv(logs['diags'])

    log_df['diags']['pred_class'] = log_df['diags'].loc[:,['MEL_probabs','NV_probabs','BKL_probabs']].idxmax(axis='columns')
    log_df['diags'].drop(columns=['MEL_preds','NV_preds','BKL_preds'], inplace=False)

    for id in diag_names:
        log_df['diags'].loc[log_df['diags']['pred_class'] == '{}_probabs'.format(id) , 'pred_class'] = id


    log_df['diags']['true_class'] = log_df['diags'].loc[:,['MEL_targets','NV_targets','BKL_targets']].idxmax(axis='columns')
    log_df['diags'].drop(columns=['MEL_targets','NV_targets','BKL_targets'], inplace=False)

    for id in diag_names:
        log_df['diags'].loc[log_df['diags']['true_class'] == '{}_targets'.format(id) , 'true_class'] = id




    corrects = 1*(log_df['diags']['pred_class'] == log_df['diags']['true_class'])
    acc = sum(corrects) / len(corrects)
    print('acc for {} data in fold {} is : {}'.format(dataset_prefix,fold_num,acc))

#%%

    