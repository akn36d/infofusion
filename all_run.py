# %%
import copy
import json
import os

# import pickle
import dill as pickle
import neptune as neptune
import numpy as np
import torch
from albumentations import from_dict as txm_from_dict
from flatten_dict import flatten as dict_flatten
from flatten_dict import unflatten as dict_unflatten
from torch import utils

from config_dct_templates import Config_grid_gen, grid_item
from config_helpers import print_helpers
from setup_helpers import (display_save_model_info, loss_setup_helper,
                           lr_optim_setup_helper, model_setup_helper,
                           system_run_helper, system_test_helper,
                           test_callbacks_evals_helper,
                           train_callbacks_evals_helper,
                           train_dataloader_setup_helper,
                           val_dataloader_setup_helper)
from utils.dataset import ISIC_dataset
from utils.utils import Environ, makefolder_ifnotexists, remove_logs
from utils.callbacks import FoldEvalsCallback
# %% LOAD DATA
with open('platform_id.json', 'rb') as fp:
    platform_id = json.load(fp)


if platform_id['project'] == 'infofusion':
    if platform_id['platform'] == 'r08stanleyj':
        model_suite_type = 'multitask_r08'
        data_dir = os.path.join('/usr/local/home/akn36d/Downloads/')
        environ = Environ()
        environ.create_suite_folders(model_suite_type)
        iterator_checkpoint = os.path.join(
            environ.suite_dirname,
            'config_iterator.pkl'
        )
        config_chkpt = os.path.join(
            environ.suite_dirname,
            'config_chkpt.json'
        )
        config_init_json = 'r08_multitask_cfg_init.json'
        cfg_dict_iterator = Config_grid_gen(
            config_init_json,
            iterator_checkpoint,
            config_chkpt
        )

        def grid_restrictions(grid_inst): 
            return (
                ((grid_item(grid_inst, 'batch_size') > 16) and (grid_item(grid_inst, 'input_size') > 224))
                    or 
                ((grid_item(grid_inst, 'arch_type') in ['diags-only', 'diags-feats-pcgrad']) and (grid_item(grid_inst, 'trainer_type') in ['ganlike']))
                    or 
                ((grid_item(grid_inst, 'arch_type') == 'diags-feats') and (grid_item(grid_inst, 'trainer_type') is None))
            )
        cfg_dict_iterator.generate_config_grid(
            restriction_lambda=grid_restrictions
        )

    elif platform_id['platform'] == 'paperspace':
        data_dir = os.path.join('./data/')
        model_suite_type = 'multitask_paperspace'
        data_dir = os.path.join('./data/')
        environ = Environ()
        environ.create_suite_folders(model_suite_type)
        iterator_checkpoint = os.path.join(
            environ.suite_dirname,
            'config_iterator.pkl'
        )
        config_chkpt = os.path.join(
            environ.suite_dirname,
            'config_chkpt.json'
        )
        config_init_json = 'paperspace_multitask_cfg_init.json'
        cfg_dict_iterator = Config_grid_gen(
            config_init_json,
            iterator_checkpoint,
            config_chkpt
        )

        def grid_restrictions(grid_inst): 
            return (
                ((grid_item(grid_inst, 'batch_size') > 16) and (grid_item(grid_inst, 'input_size') > 224))
                    or 
                ((grid_item(grid_inst, 'arch_type') in ['diags-only', 'diags-feats-pcgrad']) and (grid_item(grid_inst, 'trainer_type') in ['ganlike']))
                    or 
                ((grid_item(grid_inst, 'arch_type') == 'diags-feats') and (grid_item(grid_inst, 'trainer_type') is None))
            )
        cfg_dict_iterator.generate_config_grid(
            restriction_lambda=grid_restrictions
        )

# %%
fold_csv_dir = os.path.join('./data_folds/5foldTrainVal_ftbal_no_lesnid_leak_src_balcd_nets_only')
last_activation = torch.nn.Sigmoid()

# init check vars
is_continue_train = False
is_config_chkpt = False
is_continue_config = False

# using neptune to create experiment for tracking and comparison
api_token = "eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vdWkubmVwdHVuZS5haSIsImFwaV91cmwiOiJodHRwczovL3VpLm5lcHR1bmUuYWkiLCJhcGlfa2V5IjoiYTRkN2RiM2QtN2YyOC00YjQ4LTg4YzctMjEzN2JjOTM2NTllIn0="
npt_project = neptune.init(
    'ajaxis001/{}'.format(
        cfg_dict_iterator.config_dict_constants['suite_type']),
    api_token=api_token
)

try:
    with open(iterator_checkpoint, 'rb') as fp:
        cfg_dict_iterator = pickle.load(fp)
        is_continue_config = True
        print("--------------- Previous state of config dict iterator exists will try to restart ---------------")
        print("Previously stopped at index : {}".format(cfg_dict_iterator.index))
        print("--------------- ----------------------------------------------------------------- ---------------")
except (FileNotFoundError, EOFError):
    # save iterator state for reset in case of unforseen events
    with open(iterator_checkpoint, 'wb') as fp:
        pickle.dump(cfg_dict_iterator, fp)
    print("--------------- ----------------------------------------------------------- ---------------")
    print("              NO previous state of config dict iterator exists will grid anew              ")
    print("--------------- ----------------------------------------------------------- ---------------")

# %%
for config_dict in cfg_dict_iterator:
    if is_continue_config:
        try:
            with open(config_chkpt) as file:
                config_dict_chkpt = json.load(file)
                config_dict_chkpt = dict_unflatten(
                    config_dict_chkpt, splitter='path')
            config_dict = config_dict_chkpt

            is_continue_train = True
            is_continue_config = False
        except (FileNotFoundError, EOFError):
            print('No previous config dict checkpoint found')
            is_continue_train = False
            is_continue_config = False

    # %% PRELIM SETUP
    if is_continue_train:
        npt_experiment = npt_project.get_experiments(
            id=config_dict['neptune_id'])[0]
        environ_list = config_dict['environ_list']
        print_helpers(config_dict, case='continue')
    else:
        npt_experiment = neptune.create_experiment(
            name=config_dict['suite_type'],
            params=dict_flatten(config_dict.copy(), reducer='path'),
            tags=[config_dict['experiment']]
        )

        # Setup the environment
        run_prefix = str(npt_experiment.id) + '_'
        config_dict['neptune_id'] = npt_experiment.id

        environ.create_run_folders(run_prefix=run_prefix)
        environ_list = environ.__dict__
        config_dict['environ_list'] = environ_list
        print_helpers(config_dict)

    with open(os.path.join(environ_list['config_path'], 'config_info.json'), 'w') as fp:
        json.dump(config_dict, fp, sort_keys=False, indent=4)

    with open(config_chkpt, 'w') as fp:
        json.dump(config_dict, fp, sort_keys=False, indent=4)


    rand_seed = np.r_[0:config_dict['num_folds']]
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print('GPU is available : {}'.format(torch.cuda.is_available()))
    print(device)

    # %% TRAINING
    diags_fold_evals_callback = FoldEvalsCallback(
        class_names=config_dict['diag_names'], 
        scalar_set_name='diagsFoldAvgd', 
        config_dict=config_dict,
    )
    feats_fold_evals_callback = FoldEvalsCallback(
        class_names=config_dict['feat_names'], 
        scalar_set_name='featsFoldAvgd', 
        config_dict=config_dict,
    )
    diags_fold_evals_callback.set_dl_eval_keys(
            eval_keys=[
                'acc-diags',
                'macro-prec-diags',
                'macro-rec-diags',
                'macro-f1-diags',
            ],
            dl_keys=['val', 'diags-test']
        )
    feats_fold_evals_callback.set_dl_eval_keys(
        eval_keys=[
            'hamming-loss-feats',
            'macro-prec-feats',
            'macro-rec-feats',
            'macro-f1-feats',
            'weighted-prec-feats',
            'weighted-rec-feats',
            'weighted-f1-feats',
            'macro-ovo-auc-feats', 
            'macro-ovr-auc-feats'
        ],
        dl_keys=['val']
    )

    folds_iter_num = config_dict['num_folds']
    for fold_num in range(folds_iter_num):
        # config_dict['early_stopping_patience'] = 5
        config_dict['fold_num'] = fold_num
        print_helpers(config_dict, case='fold')

        # file to save weights
        model_wts_fname = os.path.join(
            environ_list['save_weights_to_path'],
            "model_best_wts_fld{}.pt".format(fold_num)
        )

        if is_continue_train:
            if os.path.exists(model_wts_fname):
                print('Best weights for fold ({}) already found skipping to next fold. '.format(
                    fold_num))
                continue
            else:
                print('Resetting Fold callbacks ({})'.format(fold_num))
                fold_is_correct, diag_folds_reload = diags_fold_evals_callback.check_if_save_exists()
                if fold_is_correct:
                    diags_fold_evals_callback = diag_folds_reload
                    print('Diag Fold callbacks reset.')

                fold_is_correct, feat_folds_reload = feats_fold_evals_callback.check_if_save_exists()
                if fold_is_correct:
                    feats_fold_evals_callback = feat_folds_reload
                    print('feat Fold callbacks reset.')

                config_dict['try_reset'] = True

        # Specifying the train transforms, dataset and dataloader
        train_final_file = 'train_featbalanced_{}fold{}.csv'.format(
            config_dict['num_folds'], fold_num)
        train_dataset, train_dataloader, class_weights, pos_weights = train_dataloader_setup_helper(
            config_dict,
            train_file=os.path.join(fold_csv_dir, train_final_file),
            device=device
        )
        train_dataset.append_img_dir_to_df(
            img_dir=os.path.join(data_dir, config_dict['data_dir']))
        
        # We use the imagenet mean and std since the pretrained networks 
        # are trained using imagenet
        train_dataset.transform = txm_from_dict(
            config_dict['augmentation_txms']['train'])

        # Specifying the val transforms and dataset
        val_tuning_file = 'val_featbalanced_{}fold{}.csv'.format(
            config_dict['num_folds'], fold_num
        )
        val_tuning_dataset, val_tuning_dataloader = val_dataloader_setup_helper(
            config_dict,
            val_file=os.path.join(fold_csv_dir, val_tuning_file),
            transforms=txm_from_dict(config_dict['augmentation_txms']['val']),
            phase='val'
        )
        val_tuning_dataset.append_img_dir_to_df(
            img_dir=os.path.join(data_dir, config_dict['data_dir'])
        )

        # Setting up the train loss
        train_losswrapper_dict = loss_setup_helper(
            config_dict, pos_weights=pos_weights)

        # Model creation and param list with init lr set for all model params
        full_model, param_lr_list = model_setup_helper(config_dict, device)

        # Initialize a learning rate scheduler and optimizer
        lr_scheduler_callback, optimizer_full_model = lr_optim_setup_helper(
            config_dict,
            param_lr_list=param_lr_list,
            npt_experiment=npt_experiment
        )

        # path to save stats generated from callbacks and evaluators
        fold_logdir = os.path.join(
            environ_list['log_path'],
            'Fold{}_log'.format(fold_num)
        )
        makefolder_ifnotexists(fold_logdir)

        # Setting up train time evaluators and callbacks
        evals_dict, callbacks = train_callbacks_evals_helper(
            config_dict,
            train_losswrapper_dict,
            lr_scheduler_callback,
            fold_logdir,
            npt_experiment,
            device
        )

        # sends models params info to neptune
        display_save_model_info(
            config_dict,
            full_model,
            val_tuning_dataloader,
            npt_experiment,
            device
        )

        # dict of dataloaders for eval during training
        test_dls = {
            'val': val_tuning_dataloader,
        }

        # init and train model system
        full_system = system_run_helper(
            config_dict,
            full_model,
            train_losswrapper_dict,
            train_dataloader,
            test_dls,
            optimizer_full_model,
            evals_dict,
            callbacks,
            npt_experiment,
            fold_num,
            last_activation,
            device
        )

        # saving the model on the current fold
        model_best_wts = copy.deepcopy(full_system.model.state_dict())
        save_with_wts = {
            'model_types': config_dict['model_type'],
            'best_wts': model_best_wts,
            'last_activation': last_activation,
        }

        if pos_weights:
            save_with_wts.update({'pos_weights': pos_weights})

        if class_weights:
            save_with_wts.update({'class_weights': class_weights})

        torch.save(save_with_wts, model_wts_fname)
        del save_with_wts

        config_dict['try_reset'] = False
        # TEST PHASE
        # this DL and dataset is only for use during test phase
        diags_test_dataset = ISIC_dataset(
            os.path.join(fold_csv_dir, 'test_dset_class_balanced.csv'),
            diag_list=config_dict['diag_names'],
            feat_list=None,
            phase='test',
            transform=txm_from_dict(config_dict['augmentation_txms']['val']),
        )
        diags_test_dataset.append_img_dir_to_df(
            img_dir=os.path.join(data_dir, config_dict['data_dir']))
        diags_test_dataloader = utils.data.DataLoader(
            diags_test_dataset,
            batch_size=config_dict['batch_size'],
            shuffle=True,
            num_workers=0
        )
        test_dls.update({'diags-test': diags_test_dataloader})

        # get test evaluators and callbacks
        test_evals, test_callbacks = test_callbacks_evals_helper(
            config_dict,
            fold_logdir,
            test_dls,
            npt_experiment
        )

        # test the model
        fold_results_history = system_test_helper(
            config_dict,
            test_dls,
            full_system,
            test_evals,
            test_callbacks
        )

        feats_fold_evals_callback.on_every_fold_end(fold_results_history)
        diags_fold_evals_callback.on_every_fold_end(fold_results_history)
        is_continue_train = False

        del test_evals, test_callbacks, full_system, full_model
        del test_dls, evals_dict, callbacks,
        del model_best_wts, fold_results_history
        del val_tuning_dataset, val_tuning_dataloader
        del diags_test_dataset, diags_test_dataloader
        del train_dataloader, train_dataset


    diags_fold_evals_callback.on_all_folds_end(npt_experiment)
    feats_fold_evals_callback.on_all_folds_end(npt_experiment)
    # save iterator state for restart
    with open(iterator_checkpoint, 'wb') as fp:
        pickle.dump(cfg_dict_iterator, fp)
