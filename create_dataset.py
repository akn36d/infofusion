# %%
import os

import matplotlib.pyplot as plt
import numpy as np
from numpy.core.numeric import _correlate_dispatcher
import pandas as pd
import scipy
from skmultilearn.model_selection import IterativeStratification
from torch import frac
from utils.utils import makefolder_ifnotexists
from collections import Counter
from skmultilearn.model_selection.measures import \
    get_combination_wise_output_matrix

class_names = ["MEL", "NV", "BKL"]  # remove the 9th "UNK" class
non_discriminative_feat_names = [
    "pigment_network", "globules", "negative_network", "streaks", "milia_like_cyst"]

discriminative_feat_names = [
    'pigmt_net_typical_includes_pseudonet',
    'dots_globs_reg',
    'negative_network',
    'strks_or_rad_strming',
    'MLCs_cloud',
    'granularity',
    'ker_plugs',
    'wobble',
    'scale',
    'blood',
    'atyp_net',
    'bld_vessels',
    'scarlike_regress',
    'ann_gran',
    'irreg_dots_globs',
    'blotch',
    'homog_tan_yellowish',
    'frambes_cerebriform_fernlike_pattern',
    'rough_covered_with_irreg_hyperkeratosis'
]


# %%
def get_orderwise_stats(stratification_on_arr, multilabel_names_list, order):
    cnt = Counter(combination for row in get_combination_wise_output_matrix(stratification_on_arr, order=order) for combination in row)
    # total_combo_cnt = 0
    combo_cnt_dict = {}
    for combo, count in cnt.items():
        combo = np.array(list(combo))
        # print(combo)
        check_key = np.sort(np.unique(combo)).tolist()
        check_key = tuple([multilabel_names_list[idx] for idx in check_key])
        if check_key not in combo_cnt_dict:
            combo_cnt_dict[check_key] = count
            # total_combo_cnt += count
    return combo_cnt_dict


def get_value_counts(data_Series):
    value_list = list(data_Series.value_counts().values)
    if any(value_list):
        return value_list[0]
    else:
        return 0

def get_orderwise_count_dict(set_df, set_name, multilabel_names_list, order):
    stratification_on_full_set = set_df.loc[:, discriminative_feat_names].to_numpy()
    stratification_on_for_mel = set_df[set_df['MEL'] == 1].loc[:, discriminative_feat_names].to_numpy()
    stratification_on_for_bkl = set_df[set_df['BKL'] == 1].loc[:, discriminative_feat_names].to_numpy()
    stratification_on_for_nv = set_df[set_df['NV'] == 1].loc[:, discriminative_feat_names].to_numpy()
    return {
        '{}_set'.format(set_name): get_orderwise_stats(stratification_on_full_set, multilabel_names_list, order),
        '{}_MEL'.format(set_name): get_orderwise_stats(stratification_on_for_mel, multilabel_names_list, order),
        '{}_BKL'.format(set_name): get_orderwise_stats(stratification_on_for_bkl, multilabel_names_list, order),
        '{}_NV'.format(set_name): get_orderwise_stats(stratification_on_for_nv, multilabel_names_list, order),
    }

# my_df.to_csv('my_df_meta.csv')
def set_row_class(row):
    if row.MEL == 1:
        return 'MEL'
    if row.BKL == 1:
        return 'BKL'
    if row.NV == 1:
        return 'NV'

def add_dataset_class(source_df):
    source_df = source_df.replace({'dataset_class': r'.*?BCN.*\/?'}, {'dataset_class': 'BCN'}, regex=True)
    source_df = source_df.replace({'dataset_class': r'.*?HAM.*\/?'}, {'dataset_class': 'HAM'}, regex=True)
    source_df = source_df.replace({'dataset_class': r'.*?MSK.*\/?'}, {'dataset_class': 'MSK'}, regex=True)
    source_df = source_df.replace({'dataset_class':  r'.*?ISIC.*\/?'}, {'dataset_class': 'ISIC'}, regex=True)
    return source_df

# %%
csv_dir = os.path.join('csv_files')
fold_save_dir = os.path.join('./data_folds/5foldTrainVal_featbalanced_no_lesion_id_leak')
makefolder_ifnotexists(fold_save_dir)

csv_file_my_train = os.path.join(csv_dir, 'Final_dataset_to_train.csv')


# %%
isic_19_gt_meta_df = pd.read_csv('isic19_gt_meta.csv', skipinitialspace=True)
values = {'age_approx':0,	'anatom_site_general' : 'NA',	'lesion_id' : 'no_id',	'sex': 'unk'}
isic_19_gt_meta_df = isic_19_gt_meta_df.fillna(value=values)
# %%
isic_19_gt_meta_df.loc[(isic_19_gt_meta_df['lesion_id'] == 'no_id'), 'lesion_id'] = isic_19_gt_meta_df['image'][isic_19_gt_meta_df['lesion_id'] == 'no_id'] 
isic_19_gt_meta_df['dataset_class'] = isic_19_gt_meta_df['lesion_id']
unique_ids = isic_19_gt_meta_df.lesion_id.unique()

isic_19_gt_meta_df = add_dataset_class(isic_19_gt_meta_df)
# %%
isic_19_gt_meta_df_dset_percents = isic_19_gt_meta_df['dataset_class'].value_counts(normalize=True)

# %%
class_dset_sum_dict = {}
for class_ in class_names:
    class_df_test = isic_19_gt_meta_df[isic_19_gt_meta_df[class_] == 1]

    class_dset_sum_dict[class_] = {}

    for dset in list(isic_19_gt_meta_df['dataset_class'].unique()):
        dset_counts = get_value_counts(class_df_test[class_df_test['dataset_class'] ==  dset]['dataset_class'])
        class_dset_sum_dict[class_][dset] = dset_counts

# %%
bar_df = pd.DataFrame(class_dset_sum_dict).fillna(0.0)

# %%
ax = bar_df.plot.bar(figsize=(10, 6), align='center');



# %%
my_df = pd.read_csv(csv_file_my_train, skipinitialspace=True)
my_df = my_df.fillna(0)

# %%
my_df = my_df.merge(isic_19_gt_meta_df[['image', 'lesion_id']], how='inner', on='image')
my_df['dataset_class'] = my_df['lesion_id']

my_unique_ids = my_df.lesion_id.unique()

my_df = add_dataset_class(my_df)
my_df_dset_percents = my_df['dataset_class'].value_counts(normalize=True)
# %%


# create a column with class name
my_df['class'] = my_df.apply(lambda row: set_row_class(row), axis = 1)

# %%
# find rows with same lesion_id but different class names (i.e. diagnosis)
prob_df = my_df[my_df.groupby(['lesion_id'])['class'].transform('nunique') > 1]

# multiindexed df for extracting all rows with same lesion id
# e.g. heirarchical_df.xs(key="BCN_0001728", level="lesion_id")
heirarchical_df = my_df.set_index(["lesion_id", "image"])


# %%
data_df = my_df
class_dfs = {}
for class_ in class_names:
    class_dfs[class_] = data_df[data_df[class_] == 1]

# %%
# Create new class based features so (3 diags x 19 feats) = 57 new class based feats

for feat_ in discriminative_feat_names:
    for diag_ in class_names:
        my_df[diag_+'_'+feat_] = my_df[feat_] * my_df[diag_]
    
    my_df = my_df.drop(columns=[feat_])
    

my_df.head()

# %%
pd.options.display.max_rows = 4000

my_df.sum(numeric_only=True)

# %% Iterative stratification
n_splits = 5
order = 3
k_fold = IterativeStratification(n_splits=n_splits, order=order)

# classwise_fold = {}
train_final_folds = n_splits * [pd.DataFrame()]
val_final_folds = n_splits * [pd.DataFrame()]


# %% Stratify based on class and features
for class_, class_df in class_dfs.items():
    stratification_on =  scipy.sparse.lil_matrix(class_df.loc[:,discriminative_feat_names].to_numpy())
    class_index_vals =  np.array(class_df.index.values.tolist()).transpose()
    fold_idx = 0
    for train_index, val_index in k_fold.split(class_df, stratification_on):

        train_diagfeat_discriminative_df  = class_df.iloc[train_index, :]
        val_diagfeat_discriminative_df = class_df.iloc[val_index, :]

        train_final_folds[fold_idx] = train_final_folds[fold_idx].append(train_diagfeat_discriminative_df)
        val_final_folds[fold_idx] =  val_final_folds[fold_idx].append(val_diagfeat_discriminative_df)
        fold_idx += 1


# %%


# %% For each unique lesion id ensure that the same id doesnt appear across the
# train and val sets for each fold. If a unique id is overrepresented in a set 
# then move all of that id to that set
my_df_lesion_id_cnts = my_df['lesion_id'].value_counts()
for fold_idx in range(n_splits):
    train_df = train_final_folds[fold_idx]
    val_df = val_final_folds[fold_idx]

    for lesion_id in list(my_df_lesion_id_cnts.keys()):
        train_lesion_id_df = train_df[train_df['lesion_id'] == lesion_id]
        train_lesion_id_cnts = get_value_counts(train_lesion_id_df['lesion_id'])
        
        val_lesion_id_df = val_df[val_df['lesion_id'] == lesion_id]
        val_lesion_id_cnts = get_value_counts(val_lesion_id_df['lesion_id'])
        if train_lesion_id_cnts <= val_lesion_id_cnts:
            # move rows to val dataset
            val_final_folds[fold_idx] = val_final_folds[fold_idx].append(train_lesion_id_df)
            train_final_folds[fold_idx] = train_final_folds[fold_idx].drop(train_lesion_id_df.index)

        else:
            # move rows to train dataset
            train_final_folds[fold_idx] = train_final_folds[fold_idx].append(val_lesion_id_df)
            val_final_folds[fold_idx] = val_final_folds[fold_idx].drop(val_lesion_id_df.index)


# %% Sanity check to see if train and val sets have no leaks
for idx in range(n_splits):
    train_fold_df = train_final_folds[idx]
    val_fold_df = val_final_folds[idx]

    bool_df = train_fold_df.isin({'lesion_id': list(val_fold_df['lesion_id'].unique()) })
    if bool_df['lesion_id'].sum() > 0:
        print('Data leak in fold {}'.format(idx))
    else:
        print('No data leak in {}'.format(idx))


# %% save folds
for idx in range(n_splits):
    train_fold_df = train_final_folds[idx]
    val_fold_df = val_final_folds[idx]

    train_fold_df.to_csv(
        os.path.join(
            fold_save_dir,
            'train_featbalanced_{}fold{}.csv'.format(n_splits, idx)
        ),
        index=False, header=True
    )

    val_fold_df.to_csv(
        os.path.join(
            fold_save_dir,
            'val_featbalanced_{}fold{}.csv'.format(n_splits, idx)
        ),
        index=False, header=True
    )


# %%
class_sum_dict = {}
for idx in range(n_splits):
    train_fold_df = train_final_folds[idx]
    tr_s = train_fold_df.sum(numeric_only=True) 
    tr_s = tr_s[discriminative_feat_names] # / tr_s[discriminative_feat_names].sum()

    val_fold_df = val_final_folds[idx]
    vl_s = val_fold_df.sum(numeric_only=True)
    vl_s = vl_s[discriminative_feat_names] #/ vl_s[discriminative_feat_names].sum()

    class_sum_dict.update(
        {
            'tr_fld{}_feats'.format(idx): tr_s,
            'vl_fld{}_feats'.format(idx): vl_s,
        }
    )


# %%
plt.rcParams["figure.dpi"] = 600

# %%
bar_df = pd.DataFrame(class_sum_dict).fillna(0.0)
bar_Df = pd.DataFrame().fillna(0.0)
# %%
ax = bar_df.plot.bar(figsize=(16, 6));

# We want to show all ticks...
ax.set_xticks(np.arange(len(discriminative_feat_names)));
# ... and label them with the respective list entries
ax.set_xticklabels(discriminative_feat_names);

# Rotate the tick labels and set their alignment.
plt.setp(ax.get_xticklabels(), rotation=90, ha="right",
         rotation_mode="anchor");

# %%


# %%

# %%
combined_set_dict = {}

combined_set_dict.update(
    get_orderwise_count_dict(
        class_df, 'full', discriminative_feat_names, order=order
    )
)

for idx in range(n_splits):
    train_fold_df = train_final_folds[idx]
    val_fold_df = val_final_folds[idx]

    combined_set_dict.update(
        get_orderwise_count_dict(
            train_fold_df, 'train_Fold{}'.format(idx),
            discriminative_feat_names, order=order
        )
    )
    combined_set_dict.update(
        get_orderwise_count_dict(
            val_fold_df, 'val_Fold{}'.format(idx),
            discriminative_feat_names, order=order
        )
    )
# %%
combined_stats_df = pd.DataFrame(combined_set_dict).fillna(0.0)

# %%
my_df_lesion_id_cnts_list = list(my_df_lesion_id_cnts.keys())
cleaned_isic19_df = isic_19_gt_meta_df[~isic_19_gt_meta_df['lesion_id'].isin(my_df_lesion_id_cnts_list)]

# %%
for class_ in class_names:
    train_fold_df = train_final_folds[idx]
    val_fold_df = val_final_folds[idx]

    bool_df = train_fold_df.isin({'lesion_id': list(val_fold_df['lesion_id'].unique()) })
    if bool_df['lesion_id'].sum() > 0:
        print('Data leak in fold {}'.format(idx))
    else:
        print('No data leak in {}'.format(idx))

# %% creating clean balanced test set
test_df = pd.DataFrame()
for class_ in class_names:    
    unique_dsets = list(cleaned_isic19_df[cleaned_isic19_df[class_] == 1]['dataset_class'].value_counts().to_dict().keys())
    
    for dset in unique_dsets:
        filtered_df = cleaned_isic19_df[(cleaned_isic19_df[class_] == 1)]  
        filtered_df = filtered_df[(filtered_df['dataset_class'] == dset )]
        if  len(filtered_df) <= 100:
            test_df = test_df.append(filtered_df)
        else:            
            test_df = test_df.append(filtered_df.sample(frac=1)[:100])
# %%
test_df.to_csv(
        os.path.join(
            fold_save_dir,
            'test_dset_class_balanced.csv'
        ),
        index=False, header=True
    )


# %%
# %%
test_df_sum = test_df.sum(numeric_only=True) 
dsets = list(test_df['dataset_class'].unique())
# %%
test_df_sum[class_names].plot.bar()
# %%
test_df['dataset_class'].value_counts().plot.bar()



# %%
class_dset_sum_dict = {}
for class_ in class_names:
    class_df_test = test_df[test_df[class_] == 1]

    class_dset_sum_dict[class_] = {}

    for dset in dsets:
        dset_counts = get_value_counts(class_df_test[class_df_test['dataset_class'] ==  dset]['dataset_class'])
        class_dset_sum_dict[class_][dset] = dset_counts

# %%
bar_df = pd.DataFrame(class_dset_sum_dict).fillna(0.0)

# %%
ax = bar_df.plot.bar(figsize=(10, 6), align='center');

# %%
