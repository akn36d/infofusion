import json
import os
from cv2 import data

import numpy as np
import torch
from pytorch_model_summary import summary


from torch import nn
from torch.utils.data.dataloader import DataLoader
from torch.utils.data.dataset import Dataset

from PIL import Image

from utils.data_utils import *
from utils.dataset import *
from utils.dysion_models import *

# from utils.model_utils import *
from utils.models import model, multilevel_model
from utils.transforms import albu_aug_train, albu_aug_val
from utils.utils import *

from utils.pyvis.gradcam import GradCam
from utils.pyvis.misc_functions import get_example_params, save_class_activation_images, recreate_image

# LOAD DATA
csv_dir = os.path.join('csv_files')
data_dir = os.path.join('/usr/local/home/akn36d/Downloads/ISIC_2019_Training_Input')
gt_dir = os.path.join('/usr/local/home/akn36d/Downloads/')

# class_names = ["MEL","NV","BCC","AK","BKL","DF","VASC","SCC"] # remove the 9th "UNK" class 
diag_names = ["MEL","NV","BKL"] 
# feat_names = ["pigment_network","globules","negative_network","streaks","milia_like_cyst"]
extended_feat_names = ['pigment_network', 'dots_globules', 'negative_network', 'streaks_incl_rad_streaming',	\
                       'milia_like_cyst', 'granularity', 'ker_plugs', 'wobble', 'scale', 'blood', 'atyp_net',   \
                       'blood_vessels', 'scarlike_regression']


#%%
annotations_file = os.path.join(csv_dir, 'ISIC_2018_Training_diag_gt.csv')
test_annotations_file = os.path.join(csv_dir, 'test_gt_annotations.csv') 
test_files = os.path.join('test_fold_with_gt.csv')

### Setting up environment
#%%
# PRELIM Environment SETUP
config_fpath = os.path.join('Models/multilevel_with_feats_overtrain/diags_noPigNet_2020-11-03 16:47:22/configs/config_info.json')


# #### Setting GPU availability
#%%
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('GPU is available : {}'.format(torch.cuda.is_available()))
print(device)

with open(config_fpath, 'r') as f:
    config_dict = json.load(f)

### Training
#%%
task_type = config_dict['task_type'] 
diag_names = config_dict['diag_names']
feat_names = config_dict['feat_names']
no_feats_class = config_dict['no_feats_class']

# Saving the params used for training the specific model
num_folds = config_dict['num_folds']
model_type = config_dict['model_type'] 
input_size = config_dict['input_size'] 
tr_params_dict = config_dict['img_preprocessing_params']['tr_params_dict']
val_params_dict = config_dict['img_preprocessing_params']['val_params_dict']
freeze_len = config_dict['freeze_len']
use_normalization = config_dict['find_mean']
use_last_activation = config_dict['use_last_activation']


if config_dict['use_gpu']:
    device = torch.device('cuda')
else:
    device = torch.device('cpu')
device = torch.device('cpu')

environ_list = config_dict['environ_list']
model_weights_saved_dir = environ_list['save_weights_to_path']
log_path = environ_list['log_path']


#%%
for fold_num in range(num_folds):
    print('\n')
    print(70*"=")
    print("FOLD - {}".format(fold_num))
    print(70*"=")
    # pprint(fold)

    train_final_files = 'train18_diagfeatExt_final_{}fold{}.csv'.format(config_dict['num_folds'],fold_num)
    val_tuning_files = 'val18_diagfeatExt_tuning_{}fold{}.csv'.format(config_dict['num_folds'],fold_num)    
    val_final_files = 'val18_diagfeatExt_final_{}fold{}.csv'.format(config_dict['num_folds'],fold_num)    

    # Specifying the train transforms, dataset and dataloader
    # Specifying the train transforms, dataset and dataloader
    train_dataset = ISIC_dataset(
        train_final_files,
        diag_list=diag_names, 
        feat_list=feat_names,
        phase='train',
        transform=None,
        no_feats_class=config_dict['no_feats_class'],
    ) 

    if config_dict['use_class_weights'] or config_dict['use_balanced_sampler']:
        class_weights = {}
        pos_weights = train_dataset.get_pos_weights()
        for key,value in pos_weights.items():
            pos_weights[key] = torch.tensor(value, device=device).float()
            class_weights[key] = 1 / (torch.tensor(train_dataset.class_nums[key].values, device=device).float())

    # Use the diagnosis classes `diags` for the balancing of the data 
    if config_dict['use_balanced_sampler']:
        train_dataloader = torch.utils.data.DataLoader(
            train_dataset, 
            batch_size=1, 
            shuffle=False, 
            num_workers=0, 
            sampler=get_balanced_sampler(class_weights['diags'], len(train_dataset))
        )
    else:
        train_dataloader = torch.utils.data.DataLoader(
            train_dataset, 
            batch_size=1,  
            shuffle=True, 
            num_workers=0
        )

    # Finding Training image set mean and std for normalization 
    saved_stats_for_fld = os.path.join(environ_list['suite_dirname'],'stats_fld{}.pkl'.format(fold_num))
    if config_dict['find_mean']:
        pop_mean, pop_std0, pop_std1 = train_dataset.save_dataset_mean_std(saved_stats_for_fld, albu_aug_val([0,0,0,],[1,1,1]))
    else: 
        (pop_mean, pop_std0, pop_std1) = ([0,0,0],[1,1,1],[1,1,1])

    # including the transforms for the train dataset after calculating the 
    # training sets mean and std which will be used for Standardization
    train_dataset.transform = albu_aug_train(pop_mean, pop_std0, **config_dict['img_preprocessing_params']['tr_params_dict'])

    # Specifying the val transforms and dataset
    val_transforms = albu_aug_val(pop_mean, pop_std0, **config_dict['img_preprocessing_params']['val_params_dict'])

    val_tuning_dataset = ISIC_dataset(
        val_tuning_files, 
        diag_list=diag_names, 
        feat_list=feat_names, 
        phase='val',
        transform=val_transforms,
        no_feats_class=config_dict['no_feats_class'],
    )     

    val_tuning_dataloader = torch.utils.data.DataLoader(
        val_tuning_dataset, 
        batch_size=1, 
        shuffle=True, 
        num_workers=0
    )

    val_final_dataset = ISIC_dataset(
        val_final_files, 
        diag_list=diag_names, 
        feat_list=feat_names, 
        phase='test',
        transform=val_transforms,
        no_feats_class=config_dict['no_feats_class'],
    )     

    val_final_dataloader = torch.utils.data.DataLoader(
        val_final_dataset, 
        batch_size=1, 
        shuffle=True, 
        num_workers=0
    )

    # MODEL CREATION   
    # update feat_names if `no_feats` class was added after both tain and val datasets have been created
    feat_names = train_dataset.feats.copy()

    diagnosis_model = multilevel_model(
        input_image_sz=config_dict['input_size'],
        num_diags=len(diag_names), num_ftrs=len(feat_names), 
        num_secondary_img_ftrs=config_dict['num_secondary_img_ftrs'], 
        num_secondary_feat_ftrs=config_dict['num_secondary_feat_ftrs'], 
        image_to_ftrs_model_type=config_dict['model_type']['image_to_ftrs_model'], 
        feats_to_ftrs_model_type=config_dict['model_type']['feats_to_ftrs_model'], 
        final_diags_model_type=config_dict['model_type']['final_diags_model'], 
        feats_detector_model_type=config_dict['model_type']['feats_detector_model'],
        image_to_ftrs_model_freeze_len=config_dict['freeze_len']['image_to_ftrs_model'], 
        feats_to_ftrs_model_freeze_len=config_dict['freeze_len']['feats_to_ftrs_model'],
        final_diags_model_freeze_len=config_dict['freeze_len']['final_diags_model'], 
        feats_detector_model_freeze_len=config_dict['freeze_len']['feats_detector_model'],
        is_freeze_cnn=config_dict['freeze_cnn']
    )

    model_wts_fname = os.path.join(environ_list['save_weights_to_path'],"model_best_wts_fld{}.pt".format(fold_num))
    model_wts = torch.load(model_wts_fname, map_location=device)

    if model_wts['model_types'] == diagnosis_model.model_type:
        diagnosis_model.load_state_dict(model_wts['best_wts'])
        diagnosis_model.output_as_dict = True

    else:
        print("Trained on '{}' model type but loading to '{}' model type".format(model_wts['model_type'], diagnosis_model.model_type))

    if use_last_activation:
        if model_wts['last_activation']:
            last_activation = model_wts['last_activation']


    #%% display model 
    if fold_num == 0:
        for name, module in diagnosis_model.named_modules():
            print('Name : {}'.format(name))

    break

    #%% set target layer for activations and gradients
    target_layer_name = 'feats_detector_model.model_net._conv_head'
    model_output_key = 'feats'

    #%%
    feats_grad_cam_folder = os.path.join(environ_list['log_path'], 'Fold{}_feats_gradcam'.format(fold_num))
    makefolder_ifnotexists(feats_grad_cam_folder)

    #%% Run Grad-cam on specified dataloader
    dloader = val_final_dataloader
    
    # Visualize model and select target layer
    for i, data_batch in enumerate(dloader):
        for class_idx, class_name in enumerate(feat_names):
            gradcam = GradCam(diagnosis_model, target_layer_name, model_output_key)
            cam = gradcam.generate_cam(data_batch['image'], target_class=class_idx)
            
            original_image = recreate_image(data_batch['image'], mean= pop_mean, std=pop_std0)
            print('{} shape : {}'.format(data_batch['image_id'],original_image.shape))
            
            if type(original_image) != Image.Image:
                original_image = Image.fromarray(original_image)
            
            file_name_to_export = os.path.join('{}_{}'.format(data_batch['image_id'][0], class_name))

            save_class_activation_images(original_image, cam, feats_grad_cam_folder, file_name_to_export, cmap='rainbow')
        
        
    # %%

