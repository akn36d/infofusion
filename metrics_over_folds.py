#%%
from numpy.lib.npyio import save
import pandas as pd
import os 
from glob import glob
import numpy as np
import matplotlib.pyplot as plt
from sklearn import metrics
from itertools import cycle
from numpy import interp
from utils.utils import makefolder_ifnotexists
#%%
class_names = ["MEL", "NV", "BKL"]  # remove the 9th "UNK" class
non_discriminative_feat_names = [
    "pigment_network", "globules", "negative_network", "streaks", "milia_like_cyst"]

discriminative_feat_names = [
    'pigmt_net_typical_includes_pseudonet',
    'dots_globs_reg',
    'negative_network',
    'strks_or_rad_strming',
    'MLCs_cloud',
    'granularity',
    'ker_plugs',
    'wobble',
    'scale',
    'blood',
    'atyp_net',
    'bld_vessels',
    'scarlike_regress',
    'ann_gran',
    'irreg_dots_globs',
    'blotch',
    'homog_tan_yellowish',
    'frambes_cerebriform_fernlike_pattern',
    'rough_covered_with_irreg_hyperkeratosis'
]

short_feat_names=[
    'pigmtnet_typ_pseud',
    'dotsglobs_reg',
    'negnet',
    'strks_rad',
    'MLCs_cloud',
    'granularity',
    'kerplugs',
    'wobble',
    'scale',
    'blood',
    'atypnet',
    'bldvessels',
    'scar_regress',
    'ann_gran',
    'dotsglobs_irreg',
    'blotch',
    'homog_tan_yellow',
    'frambes',
    'rgh_irreg_hypkeratosis'
]

#%%
# models_dir = os.path.join('./Models/multitask_r08/MUL1-51_2021-05-23 19:27:53/logs/')
# diags_test_log = 'diags-test_diags-probabs-logger.csv'
# train_log = 'train_diags-probabs-logger.csv'
# val_log = 'val_diags-probabs-logger.csv'

# #%%
# num_folds = 5
# accumulate_log = diags_test_log

# probabs_cols = [x+'_probabs' for x in class_names]
# targets_cols = ['targets_'+x for x in class_names]

# diags_probabs_avgd = 0


# for fold_num in range(num_folds):
#     log_dir = 'Fold{}_log'.format(fold_num)
#     csv_file = os.path.join(
#         models_dir,
#         log_dir, 
#         accumulate_log
#     )

#     log_df = pd.read_csv(csv_file, index_col=0).sort_values(by=['image_id'])

#     diags_probabs_avgd += log_df[probabs_cols].values
#     print(log_df.head())
# targets_df = pd.get_dummies(log_df.targets_class, prefix='targets')
# diags_targets = targets_df[targets_cols].values

# diags_probabs_avgd /= num_folds
    

# # %% REF : https://scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html
# # Compute ROC curve and ROC area for each class
# fpr = dict()
# tpr = dict()
# roc_auc = dict()
# n_classes = len(class_names)
# y_test = diags_targets
# y_score = diags_probabs_avgd

# for i in range(n_classes):
#     fpr[i], tpr[i], _ = metrics.roc_curve(y_test[:, i], y_score[:, i])
#     roc_auc[i] = metrics.auc(fpr[i], tpr[i])

# # Compute micro-average ROC curve and ROC area
# fpr["micro"], tpr["micro"], _ =  metrics.roc_curve(y_test.ravel(), y_score.ravel())
# roc_auc["micro"] =  metrics.auc(fpr["micro"], tpr["micro"])
# # First aggregate all false positive rates
# all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))

# # Then interpolate all ROC curves at this points
# mean_tpr = np.zeros_like(all_fpr)
# for i in range(n_classes):
#     mean_tpr +=  interp(all_fpr, fpr[i], tpr[i])

# # Finally average it and compute AUC
# mean_tpr /= n_classes

# fpr["macro"] = all_fpr
# tpr["macro"] = mean_tpr
# roc_auc["macro"] =  metrics.auc(fpr["macro"], tpr["macro"])

# # Plot all ROC curves
# plt.figure()
# plt.plot(fpr["micro"], tpr["micro"],
#          label='micro-average ROC curve (area = {0:0.2f})'
#                ''.format(roc_auc["micro"]),
#          color='deeppink', linestyle=':', linewidth=4)

# plt.plot(fpr["macro"], tpr["macro"],
#          label='macro-average ROC curve (area = {0:0.2f})'
#                ''.format(roc_auc["macro"]),
#          color='navy', linestyle=':', linewidth=4)

# lw = 2
# colors = cycle(['aqua', 'darkorange', 'cornflowerblue'])
# for i, color in zip(range(n_classes), colors):
#     plt.plot(fpr[i], tpr[i], color=color, lw=lw,
#              label='ROC curve of class {0} (area = {1:0.2f})'
#              ''.format(class_names[i], roc_auc[i]))
# plt.plot([0, 1], [0, 1], 'k--', lw=lw)
# plt.xlim([0.0, 1.0])
# plt.ylim([0.0, 1.05])
# plt.xlabel('False Positive Rate')
# plt.ylabel('True Positive Rate')
# plt.title('Some extension of Receiver operating characteristic to multi-class')
# plt.legend(loc="lower right")
# plt.show()




#%% Feats roc combined over multiple folds
models_dir = os.path.join('./Models/multitask_r08/MUL1-55_2021-06-16 13:45:05/logs/')
feats_val_log = 'val_feats-probabs-logger.csv'
save_folder = os.path.join(models_dir, 'feats_rocs')
makefolder_ifnotexists(save_folder)
#%%
num_folds = 5
accumulate_log = feats_val_log

probabs_cols = [x+'_probabs' for x in discriminative_feat_names]
targets_cols = [x+'_targets' for x in discriminative_feat_names]

log_df = []
for fold_num in range(num_folds):
    log_dir = 'Fold{}_log'.format(fold_num)
    csv_file = os.path.join(
        models_dir,
        log_dir, 
        accumulate_log
    )

    log_df.append(pd.read_csv(csv_file, index_col=0).sort_values(by=['image_id']))

mean_tpr_list = []
mean_auc_list = []

#%%
for i,feat in enumerate(discriminative_feat_names):
    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)
    fig, ax = plt.subplots()
    for fold_num in range(num_folds):

        curr_log_df = log_df[fold_num]

        y_test = curr_log_df[feat+'_targets'].values
        y_score = curr_log_df[feat+'_probabs'].values

        fpr, tpr, _ = metrics.roc_curve(y_test, y_score)
        interp_tpr = np.interp(mean_fpr, fpr, tpr)
        interp_tpr[0] = 0.0

        roc_auc = metrics.auc(mean_fpr, interp_tpr)

        tprs.append(interp_tpr)
        aucs.append(roc_auc)

    ax.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r',
        label='Chance', alpha=.8)

    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_tpr_list.append(mean_tpr)

    mean_auc = metrics.auc(mean_fpr, mean_tpr)
    mean_auc_list.append(mean_auc)

    std_auc = np.std(aucs)
    ax.plot(mean_fpr, mean_tpr, color='b',
            label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
            lw=2, alpha=.8)

    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    ax.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                    label=r'$\pm$ 1 std. dev.')

    ax.set(xlim=[-0.05, 1.05], ylim=[-0.05, 1.05],
        title="ROC : {}".format(short_feat_names[i]))
    ax.legend(loc="lower right")
    plt.savefig(
        os.path.join(save_folder,'roc_valfold_combined_{}.png'.format(short_feat_names[i])) 
    )   
        
#%%

fig, ax = plt.subplots()
cmap = plt.get_cmap('jet')
n_classes = len(discriminative_feat_names)

colors = cmap(np.linspace(0, 1.0, n_classes))

for i in range(n_classes):
    ax.plot(mean_fpr, mean_tpr_list[i], color=colors[i], lw=1,
            label='ROC curve of class {0} (area = {1:0.2f})'
            ''.format(short_feat_names[i], mean_auc_list[i]))

ax.plot([0, 1], [0, 1], 'k--', lw=2)
ax.set_xlim([0.0, 1.0])
ax.set_ylim([0.0, 1.05])
ax.set_xlabel('False Positive Rate')
ax.set_ylabel('True Positive Rate')
ax.set_title('Fold-Aggregated ROC curves')
lgd = ax.legend(loc='upper left', bbox_to_anchor=(1.05, 1))
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.savefig(
    os.path.join(save_folder,'roc_valfold_combined_all.png') ,
    dpi=900,
    bbox_extra_artists=(lgd,), 
    bbox_inches='tight'
) 

#%%
selective_list =[
    'pigmt_net_typical_includes_pseudonet',
    'negative_network',
    'MLCs_cloud',
    'granularity',
    'ker_plugs',
    'atyp_net',
    'scarlike_regress',
    'blotch',
]

fig, ax = plt.subplots()
cmap = plt.get_cmap('jet')
n_classes = len(discriminative_feat_names)

colors = cmap(np.linspace(0, 1.0, n_classes))

for i, feat in enumerate(discriminative_feat_names):
    if feat in selective_list:
        ax.plot(mean_fpr, mean_tpr_list[i], color=colors[i], lw=1,
                label='ROC curve of class {0} (area = {1:0.2f})'
                ''.format(short_feat_names[i], mean_auc_list[i]))

ax.plot([0, 1], [0, 1], 'k--', lw=2)
ax.set_xlim([0.0, 1.0])
ax.set_ylim([0.0, 1.05])
ax.set_xlabel('False Positive Rate')
ax.set_ylabel('True Positive Rate')
ax.set_title('Fold-Aggregated ROC curves')
lgd = ax.legend(loc='upper left', bbox_to_anchor=(1.05, 1))
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.savefig(
    os.path.join(save_folder,'roc_valfold_combined_for_DRStoecker.png') ,
    dpi=900,
    bbox_extra_artists=(lgd,), 
    bbox_inches='tight'
) 

# %%
import numpy

index_in_feat_class_list = []
selected_aucs_list = []
selected_tprs_list = [] 
short_featname_list = []

for i, feat in enumerate(discriminative_feat_names):
    if feat in selective_list:
        index_in_feat_class_list.append(i)
        selected_aucs_list.append(mean_auc_list[i])
        selected_tprs_list.append(mean_tpr_list[i])
        short_featname_list.append(short_feat_names[i])

mean_auc_arr= numpy.array(selected_aucs_list)
sort_index = np.argsort(-mean_auc_arr, kind='stable')


#%% Sort alll the necessary lists

fig, ax = plt.subplots()

colors = cmap(np.linspace(0, 1.0, 10))

for i, idx in enumerate(sort_index):

    ax.plot(mean_fpr, selected_tprs_list[idx], color=colors[idx], lw=1,
            label='ROC curve of class {0} (area = {1:0.2f})'
            ''.format(short_featname_list[idx], selected_aucs_list[idx]))

ax.plot([0, 1], [0, 1], 'k--', lw=2)
ax.set_xlim([0.0, 1.0])
ax.set_ylim([0.0, 1.05])
ax.set_xlabel('False Positive Rate')
ax.set_ylabel('True Positive Rate')
ax.set_title('Fold-Aggregated ROC curves')
lgd = ax.legend(loc='upper left', bbox_to_anchor=(1.05, 1))
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.savefig(
    os.path.join(save_folder,'roc_valfold_combined_for_DRStoecker_sorted.png') ,
    dpi=900,
    bbox_extra_artists=(lgd,), 
    bbox_inches='tight'
) 
# %%
