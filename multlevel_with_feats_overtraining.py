#%%
import json
import os

import neptune
import numpy as np
import torch
from flatten_dict import flatten as dict_flatten
from pytorch_model_summary import summary
from pytorch_wrapper import System
from pytorch_wrapper import evaluators as evaluators
from pytorch_wrapper.loss_wrappers import GenericPointWiseLossWrapper
from pytorch_wrapper.training_callbacks import (
    EarlyStoppingCriterionCallback, NumberOfEpochsStoppingCriterionCallback)
from torch import nn

from utils.callbacks import (ClearCacheCallback, LearningRateSchedulerCallback,
                             NeptuneEvalScalarsCallback, PredictionsCallBack,
                             StatsLogCallBack,
                             TensorBoardEvalCombinedScalarsCallBack,
                             TensorBoardEvalScalarsCallBack,
                             TensorBoardGradCallBack,
                             TensorBoardTrainLossCallBack)
from utils.data_utils import *
from utils.dataset import *
from utils.dysion_models import *
from utils.losses import MultiLossWrapper
from utils.metrics import (EvaluatorProbabsLogger,
                           GenericPointWiseMultiTargetLossEvaluator,
                           MultiTargetAccuracyEvaluator, MY_AUROCEvaluator)
from utils.models import model, multilevel_model
from utils.transforms import albu_aug_train, albu_aug_val
from utils.utils import Environ, remove_logs

# LOAD DATA
csv_dir = os.path.join('csv_files')
data_dir = os.path.join('/usr/local/home/akn36d/Downloads/ISIC_2019_Training_Input')
gt_dir = os.path.join('/usr/local/home/akn36d/Downloads/')

# class_names = ["MEL","NV","BCC","AK","BKL","DF","VASC","SCC"] # remove the 9th "UNK" class 
diag_names = ["MEL","NV","BKL"] 

feat_names = ['dots_globules', 
              'negative_network', 
              'streaks_incl_rad_streaming',	                       
              'milia_like_cyst']

# feat_names = ['pigment_network', 
#               'dots_globules', 
#               'negative_network', 
#               'streaks_incl_rad_streaming',	                       
#               'milia_like_cyst', 
#               'granularity', 
#               'ker_plugs', 
#               'wobble', 
#               'scale', 
#               'blood', 
#               'atyp_net',
#               'blood_vessels', 
#               'scarlike_regression']


### Setting up environment
#%%
is_continue_train = False

# PRELIM SETUP
# Creating a array of numpy seed values to be used for each fold
# Saving the params used for training the specific models
last_activation = torch.nn.Sigmoid()
if is_continue_train:
    config_dict_path = 'Models/multilevel_with_overtrain/diags_model1_2020-10-14 01:40:49/configs/config_info.json'
    with open(config_dict_path) as file:
        config_dict = json.load(file)
else:
    config_dict = {}
    config_dict['suite_type'] = 'multilevel-with-feats-overtrain'
    config_dict['task_type'] = 'diags' # model2 suffix to represent that this is the model to compare with multilevel
    config_dict['experiment'] = 'noPigNet'

    config_dict['use_gpu'] = torch.cuda.is_available()
    config_dict['find_mean']  = True 

    config_dict['diag_names'] = diag_names
    config_dict['feat_names'] = feat_names
    config_dict['no_feats_class'] = True

    config_dict['num_folds'] = 5
    config_dict['train_val_split'] = 0.25
    config_dict['n_epochs'] = 150
    config_dict['batch_size'] = 64

    config_dict['use_balanced_sampler'] = False
    config_dict['use_class_weights'] = True
    config_dict['early_stopping_patience'] = 5
    config_dict['learning_rate_diags'] = 0.00001
    config_dict['learning_rate_feats'] = 0.0000001
    
    config_dict['overtrain_learning_rate_feats'] = 0.00001
    config_dict['overtrain_early_stopping_patience'] = 3
    config_dict['overtrain_epochs_feats'] = 20
    
    config_dict['lr_scheduled_on'] = 'loss-total'
    

    config_dict['early_stopping_on'] = 'loss-total'
    config_dict['loss_weight_dict'] = {'diags_loss' : 4, 'feats_loss': 0.5}

    if last_activation:
        config_dict['use_last_activation'] = True
    else:
        config_dict['use_last_activation'] = False


    # Data preprocess params
    config_dict['input_size'] = 224
    tr_params_dict  = {'tr_RandomCrop'  : config_dict['input_size'], 'tr_Resize'  : 256}
    val_params_dict = {'val_CenterCrop' : config_dict['input_size'], 'val_Resize' : 256}
    config_dict['img_preprocessing_params'] = {
        'tr_params_dict':tr_params_dict, 
        'val_params_dict':val_params_dict
    }

    # Model specific params
    config_dict.update({
                    'model_type' : {'image_to_ftrs_model' : 'efficientnet-b1', 
                                    'feats_to_ftrs_model' : 'featnet', 
                                    'feats_detector_model': 'efficientnet-b1', 
                                    'final_diags_model'   : 'featnet'
                                },
                    'freeze_len' : { 
                                    'image_to_ftrs_model'  : 80, 
                                    'feats_to_ftrs_model'  : 0,
                                    'final_diags_model'    : 0, 
                                    'feats_detector_model' : 80
                                },
                    'freeze_cnn' : False,
                    'num_secondary_img_ftrs' : 32,
                    'num_secondary_feat_ftrs' : 32
                })


   
# using neptune to create experiment for tracking and comparison
npt_project = neptune.init('ajaxis001/{}'.format(config_dict['suite_type']))
if is_continue_train:
    npt_experiment = npt_project.get_experiments(id=config_dict['neptune_id'])[0]
else:
    # Create run name 
    run_name = config_dict['task_type'] + '_' + config_dict['experiment']
    run_prefix =    run_name + '_'

    # Setup the environment 
    environ = Environ(run_prefix=run_prefix)
    environ.create_suite_folders(config_dict['suite_type'])

    npt_experiment = neptune.create_experiment(name=run_name,
                                params=dict_flatten(config_dict, reducer='path'))
    config_dict['neptune_id'] = npt_experiment.id


rand_seed = np.r_[0:config_dict['num_folds']]


# #### Setting GPU availability
#%%
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('GPU is available : {}'.format(torch.cuda.is_available()))
print(device)

### Training
folds_iter_num = config_dict['num_folds']
#%%
for fold_num in range(folds_iter_num): #range():
    
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    diag_names = config_dict['diag_names']
    feat_names = config_dict['feat_names']

     # Setting the seed for the fold
    np.random.seed(rand_seed[fold_num])
    print('\n')
    print(70*"=")
    print("FOLD - {}".format(fold_num))
    print(70*"=")
    # pprint(fold)

    train_final_files = 'train18_diagfeatExt_final_{}fold{}.csv'.format(config_dict['num_folds'],fold_num)
    val_tuning_files = 'val18_diagfeatExt_tuning_{}fold{}.csv'.format(config_dict['num_folds'],fold_num)    
    val_final_files = 'val18_diagfeatExt_final_{}fold{}.csv'.format(config_dict['num_folds'],fold_num)    

    # Setting up folders to store model and info
    if not is_continue_train:
        if fold_num == 0 :
            environ.create_run_folders()
            environ_list = environ.__dict__
            config_dict['environ_list'] = environ_list
            
            print(2*'************',' DIRECTORY INFO',2*'************')
            print('The models will be stored in : {}'.format(environ_list['models_folder']))
            print('This model will be stored in : {}'.format(environ_list['suite_dirname']))
            print('The current run will be in : {}'.format(environ_list['run_dirname']))
            print(2*'************','***************',2*'************') 

            # save the config_dict as json for tracking
            with open(os.path.join(environ_list['config_path'], 'config_info.json'), 'w') as fp:
                json.dump(config_dict, fp, sort_keys=False, indent=4)
    else:
        environ_list = config_dict['environ_list']

        print(2*'************',' DIRECTORY INFO',2*'************')
        print('This is a restart of the model')
        print('This model will be loaded from suite : {}'.format(environ_list['suite_dirname']))
        print('The run that is being continued is : {}'.format(environ_list['run_dirname']))
        print(2*'************','***************',2*'************') 

    # check to see if fold has been trained on already
    model_wts_fname = os.path.join(environ_list['save_weights_to_path'],"model_best_wts_fld{}.pt".format(fold_num))
    if is_continue_train:
        if os.path.exists(model_wts_fname):
            print('Best weights for fold ({}) already found skipping to next fold. '.format(fold_num))    
            continue
        else:
            print('Resetting neptune logs for fold ({})'.format(fold_num))
            remove_logs(npt_experiment, 'Fold{}'.format(fold_num))

    # Specifying the train transforms, dataset and dataloader
    train_dataset = ISIC_dataset(
        train_final_files,
        diag_list=diag_names, 
        feat_list=feat_names,
        phase='train',
        transform=None,
        no_feats_class=config_dict['no_feats_class'],
    ) 

    if config_dict['use_class_weights'] or config_dict['use_balanced_sampler']:
        class_weights = {}
        pos_weights = train_dataset.get_pos_weights()
        for key,value in pos_weights.items():
            pos_weights[key] = torch.tensor(value, device=device).float()
            class_weights[key] = 1 / (torch.tensor(train_dataset.class_nums[key].values, device=device).float())
    else:
        class_weights = None
        pos_weights = None

    # Use the diagnosis classes `diags` for the balancing of the data 
    if config_dict['use_balanced_sampler']:
        train_dataloader = torch.utils.data.DataLoader(
            train_dataset, 
            batch_size=config_dict['batch_size'], 
            shuffle=False, 
            num_workers=0, 
            sampler=get_balanced_sampler(class_weights['diags'], len(train_dataset))
        )
    else:
        train_dataloader = torch.utils.data.DataLoader(
            train_dataset, 
            batch_size=config_dict['batch_size'],  
            shuffle=True, 
            num_workers=0
        )

    # Finding Training image set mean and std for normalization 
    saved_stats_for_fld = os.path.join(environ_list['suite_dirname'],'stats_fld{}.pkl'.format(fold_num))
    if config_dict['find_mean']:
        pop_mean, pop_std0, pop_std1 = train_dataset.save_dataset_mean_std(saved_stats_for_fld, albu_aug_val([0,0,0,],[1,1,1]))
    else: 
        (pop_mean, pop_std0, pop_std1) = ([0,0,0],[1,1,1],[1,1,1])

    # including the transforms for the train dataset after calculating the 
    # training sets mean and std which will be used for Standardization
    train_dataset.transform = albu_aug_train(pop_mean, pop_std0, **config_dict['img_preprocessing_params']['tr_params_dict'])

    # Specifying the val transforms and dataset
    val_transforms = albu_aug_val(pop_mean, pop_std0, **config_dict['img_preprocessing_params']['val_params_dict'])

    val_tuning_dataset = ISIC_dataset(
        val_tuning_files, 
        diag_list=diag_names, 
        feat_list=feat_names, 
        phase='val',
        transform=val_transforms,
        no_feats_class=config_dict['no_feats_class'],
    )     
    
    val_tuning_dataloader = torch.utils.data.DataLoader(
        val_tuning_dataset,
        batch_size=config_dict['batch_size'], 
        shuffle=True, 
        num_workers=0
    )

    val_final_dataset = ISIC_dataset(
        val_final_files, 
        diag_list=diag_names, 
        feat_list=feat_names, 
        phase='test',
        transform=val_transforms,
        no_feats_class=config_dict['no_feats_class'],
    )     
    
    val_final_dataloader = torch.utils.data.DataLoader(
        val_final_dataset, 
        batch_size=config_dict['batch_size'], 
        shuffle=True, 
        num_workers=0
    )
    
    # # set up the place to save the generated images list as csv
    # train_dataset.save_gen_img_list = os.path.join(environ_list['log_path'], 'train_img_list{}.csv'.format(fold_num))
    # val_tuning_dataset.save_gen_img_list =  os.path.join(environ_list['log_path'], 'val_tuning_img_list{}.csv'.format(fold_num))
    # val_final_dataset.save_gen_img_list =  os.path.join(environ_list['log_path'], 'val_final_img_list{}.csv'.format(fold_num))

    # MODEL CREATION   
    # update feat_names if `no_feats` class was added after both tain and val datasets have been created
    feat_names = train_dataset.feats.copy()

    diagnosis_model = multilevel_model(
        input_image_sz=config_dict['input_size'],
        num_diags=len(diag_names), num_ftrs=len(feat_names), 
        num_secondary_img_ftrs=config_dict['num_secondary_img_ftrs'], 
        num_secondary_feat_ftrs=config_dict['num_secondary_feat_ftrs'], 
        image_to_ftrs_model_type=config_dict['model_type']['image_to_ftrs_model'], 
        feats_to_ftrs_model_type=config_dict['model_type']['feats_to_ftrs_model'], 
        final_diags_model_type=config_dict['model_type']['final_diags_model'], 
        feats_detector_model_type=config_dict['model_type']['feats_detector_model'],
        image_to_ftrs_model_freeze_len=config_dict['freeze_len']['image_to_ftrs_model'], 
        feats_to_ftrs_model_freeze_len=config_dict['freeze_len']['feats_to_ftrs_model'],
        final_diags_model_freeze_len=config_dict['freeze_len']['final_diags_model'], 
        feats_detector_model_freeze_len=config_dict['freeze_len']['feats_detector_model'],
        is_freeze_cnn=config_dict['freeze_cnn']
    )
    feats_detector_model = diagnosis_model.feats_detector_model
    
    # Setting up the optimizers
    model_feats_params = filter(lambda p: p.requires_grad, feats_detector_model.parameters()) 
    model_full_params = filter(lambda p: p.requires_grad, diagnosis_model.parameters())
    
    optimizer_feats = torch.optim.Adam(model_feats_params, lr = config_dict['overtrain_learning_rate_feats'])
    optimizer_full_model = torch.optim.Adam(
        [
            {'params' : model_feats_params, 'lr' : config_dict['learning_rate_feats']},
            {'params' : model_full_params}
        ], 
        lr = config_dict['learning_rate_diags']
    )

    # Initialize a learning rate scheduler
    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer_full_model, mode='min', patience=3)

    # Setting up the loss and evaluation metrics    
    # Setting up the multilevel losses
    train_losswrapper_dict = {}
    train_losswrapper_dict.update({
        'diags_loss' : GenericPointWiseLossWrapper(
                                    nn.CrossEntropyLoss(
                                        weight=pos_weights['diags'], reduction='mean'), 
                                        model_output_key='diags',
                                        batch_target_key='diags'
                                    )                       
                                }
    )

    train_losswrapper_dict.update({
        'feats_loss' : GenericPointWiseLossWrapper(
                                    nn.BCEWithLogitsLoss(   
                                        reduction='mean',
                                        pos_weight=pos_weights['feats']), 
                                        model_output_key='feats',
                                        batch_target_key='feats'
                                    )
                                }
    )
    
    # combine losses
    loss_weight_dict = config_dict['loss_weight_dict']
    train_multilosswrapper = MultiLossWrapper(train_losswrapper_dict, loss_weight_dict)

    # path to save stats generated from callbacks and evaluators
    fold_logdir = os.path.join(environ_list['log_path'], 'Fold{}_log'.format(fold_num))
    makefolder_ifnotexists(fold_logdir)

    statslog_fname = {}
    statslog_fname['diags'] = os.path.join(fold_logdir,'log_fold{}_diags.csv'.format(fold_num))
    statslog_fname['feats'] = os.path.join(fold_logdir,'log_fold{}_feats.csv'.format(fold_num))
    statslog_fname['feats_overtrain'] = os.path.join(fold_logdir,'log_fold{}_feats_overtrain.csv'.format(fold_num))

    # Setting up evaluators
    diag_evals = {}
    feat_evals =  {}

    # add diag evaluators
    diag_evals = {
                    # 'prec-diags': evaluators.MultiClassPrecisionEvaluator( model_output_key='diags', batch_target_key='diags', average='micro'),
                    # 'rec-diags': evaluators.MultiClassRecallEvaluator(model_output_key='diags', batch_target_key='diags', average='micro'),
                    # 'f1-diags': evaluators.MultiClassF1Evaluator(model_output_key='diags', batch_target_key='diags', average='micro'),
                    'acc-diags': evaluators.MultiClassAccuracyEvaluator(model_output_key='diags', batch_target_key='diags'),                      
                    'loss-diags': evaluators.GenericPointWiseLossEvaluator(
                                    train_losswrapper_dict['diags_loss'], 
                                    label='loss-diags', 
                                    score_format='%f', 
                                    batch_target_key='diags'
                                ),
                    'loss-total': evaluators.GenericPointWiseLossEvaluator(
                                    train_multilosswrapper, 
                                    label='loss-total', 
                                    score_format='%f', 
                                    batch_target_key='diags'
                                ),
            'micro-auc-diags': MY_AUROCEvaluator(
                                    model_output_key='diags', 
                                    batch_target_key='diags-onehot', 
                                    class_names=diag_names, 
                                    metric='auc', 
                                    mode='micro'
                                ),
            'macro-auc-diags': MY_AUROCEvaluator(
                                    model_output_key='diags', 
                                    batch_target_key='diags-onehot', 
                                    class_names=diag_names, 
                                    metric='auc', 
                                    mode='macro'
                                ),
    }

    feat_evals = {
                    'acc-feats': MultiTargetAccuracyEvaluator(
                                    model_output_key='feats', 
                                    batch_target_key='feats',
                                    class_names=feat_names, 
                                    mode='total', 
                                    thresh=0.5
                                ),
                    'loss-feats': evaluators.GenericPointWiseLossEvaluator(
                                    train_losswrapper_dict['feats_loss'], 
                                    label='loss-feats', 
                                    score_format='%f', 
                                    batch_target_key='feats'
                                ),
        'classwise-auc-feats': MY_AUROCEvaluator(
                                    model_output_key='feats', 
                                    batch_target_key='feats', 
                                    class_names=feat_names, 
                                    metric='auc', 
                                    mode='classwise'
                                ),
        'classwise-acc-feats': MultiTargetAccuracyEvaluator(
                                    model_output_key='feats', 
                                    batch_target_key='feats',
                                    class_names=feat_names, 
                                    mode='classwise', 
                                ),                             
    }

    # Setting up Callbacks
    callbacks = {}
    tensorboard_logs = environ_list['tensorboard_log_path']
    save_checkpoint_fname = os.path.join(environ_list['save_weights_to_path'],"model_chkpnt.pt")

    # add diagnosis callbacks
    callbacks['diags'] = [
        ClearCacheCallback(device=device),
        NumberOfEpochsStoppingCriterionCallback(config_dict['n_epochs']),
        EarlyStoppingCriterionCallback(
            patience=config_dict['early_stopping_patience'],
            evaluation_data_loader_key='val',
            evaluator_key=config_dict['early_stopping_on'],
            tmp_best_state_filepath=save_checkpoint_fname
        ),
        NeptuneEvalScalarsCallback(
            evaluation_data_loader_keys=['train', 'val', 'test'], 
            evaluator_keys=[
                'loss-total', 
                'loss-diags',
                'acc-diags', 
                'micro-auc-diags', 
                'macro-auc-diags',
            ], 
            class_names=diag_names, 
            scalar_set_name='Fold{}'.format(fold_num),
            experiment=npt_experiment
        ),
        StatsLogCallBack(
            log_filepath=statslog_fname['diags'],
            evaluation_data_loader_keys=['train', 'val'],
            evaluator_keys=['loss-diags', 'acc-diags', 'loss-total'],
            class_names=diag_names
        ),
        LearningRateSchedulerCallback(
            lr_scheduler,
            dataloader_key='val',
            evaluator_key=config_dict['lr_scheduled_on']
        )
    ]

    # add feats callbacks
    callbacks['feats'] = [
            TensorBoardEvalCombinedScalarsCallBack(
                log_dir=tensorboard_logs,
                dataloader_evaluator_dict={'train':'loss-feats', 'val':'loss-feats'},
                scalar_set_name='{}'.format(fold_num)
            ),
            StatsLogCallBack(
                log_filepath=statslog_fname['feats'],
                evaluation_data_loader_keys=['train', 'val'],
                evaluator_keys=[
                    'classwise-acc-feats', 
                    'loss-feats', 
                    'acc-feats', 
                    'classwise-auc-feats', 
                ],
                class_names=feat_names
            ),
            NeptuneEvalScalarsCallback(
                evaluation_data_loader_keys=['train', 'val', 'test'], 
                evaluator_keys=[
                    'loss-feats', 
                    'acc-feats',
                    'classwise-auc-feats', 
                    'classwise-acc-feats'
                ], 
                class_names=feat_names, 
                scalar_set_name='Fold{}'.format(fold_num),
                experiment=npt_experiment
            )
        ]

    # add feats overtrain callbacks
    overtrain_save_checkpoint_fname = os.path.join(environ_list['save_weights_to_path'],"model_chkpnt_overtrain.pt")
    callbacks['feats_overtrain'] = [
            NumberOfEpochsStoppingCriterionCallback(config_dict['overtrain_epochs_feats']),
            EarlyStoppingCriterionCallback(
                patience=config_dict['overtrain_early_stopping_patience'],
                evaluation_data_loader_key='val',
                evaluator_key='loss-feats',
                tmp_best_state_filepath=overtrain_save_checkpoint_fname
            ),
            TensorBoardEvalCombinedScalarsCallBack(
                log_dir=tensorboard_logs,
                dataloader_evaluator_dict={'train':'loss-feats', 'val':'loss-feats'},
                scalar_set_name='{}_feats_overtrain'.format(fold_num)
            ),
            StatsLogCallBack(
                log_filepath=statslog_fname['feats_overtrain'],
                evaluation_data_loader_keys=['train', 'val'],
                evaluator_keys=[
                    'classwise-acc-feats', 
                    'loss-feats', 
                    'acc-feats', 
                    'classwise-auc-feats', 
                ],
                class_names=feat_names
            ),
            NeptuneEvalScalarsCallback(
                evaluation_data_loader_keys=['train', 'val', 'test'], 
                evaluator_keys=[
                    'loss-feats', 
                    'acc-feats',
                    'classwise-auc-feats', 
                    'classwise-acc-feats'
                ], 
                class_names=feat_names,
                scalar_set_name='Fold{}_feats_overtrain'.format(fold_num),
                experiment=npt_experiment
            )
        ]


    if fold_num == 0: 
        # display model in tensorboard
        dataiter = iter(val_final_dataloader)
        data_dict = dataiter.next()
        _img =  data_dict['image'][0].unsqueeze(0).to(device=device)
        diagnosis_model = diagnosis_model.to(device)
        print(summary(diagnosis_model, _img, show_input=True)) # Print Model summary

    diagnosis_model.output_as_dict = True

    # overtrain the attribute prediction model
    System_dict = {}
    feats_detector_model.output_as_dict = True
    feats_detector_model.model_output_key = 'feats'

    System_dict['feats'] = System(feats_detector_model, last_activation=last_activation, device=device)
    _ = System_dict['feats'].train(
        loss_wrapper=train_losswrapper_dict['feats_loss'],
        optimizer=optimizer_feats,
        train_data_loader=train_dataloader,
        evaluation_data_loaders={
            'train' : train_dataloader,
            'val'   : val_tuning_dataloader,
            'test'  : val_final_dataloader
        },
        batch_input_key='image',
        evaluators=feat_evals,
        callbacks=callbacks['feats_overtrain'],
        verbose=True,
        eval_verbose=False
    )

    feats_detector_model.output_as_dict = False

    # train the whole model
    System_dict['diags'] = System(diagnosis_model, last_activation=last_activation, device=device)
    _ = System_dict['diags'].train(
        loss_wrapper=train_multilosswrapper,
        optimizer=optimizer_full_model,
        train_data_loader=train_dataloader,
        evaluators=dict_merge(feat_evals, diag_evals),
        evaluation_data_loaders={
            'train' : train_dataloader,
            'val'   : val_tuning_dataloader,
            'test'  : val_final_dataloader
        },
        batch_input_key='image',
        callbacks=callbacks['feats'] + callbacks['diags'],
        verbose=True,
        eval_verbose=False
    )

    # saving the model on the current fold
    model_best_wts = copy.deepcopy(diagnosis_model.state_dict())

    save_with_wts = {
        'model_types':config_dict['model_type'],
        'best_wts':model_best_wts,
        'last_activation': last_activation,
    }

    if pos_weights:
        save_with_wts.update({'pos_weights' : pos_weights})

    if class_weights:
        save_with_wts.update({'class_weights': class_weights})
   
    torch.save(save_with_wts,model_wts_fname)

    # TESTING THE MODEL
    test_evals = {
            'diags-probabs-logger' : EvaluatorProbabsLogger(
                model_output_key='diags', 
                batch_target_key='diags-onehot', 
                batch_target_id_key='image_id',
                class_names=diag_names.copy()
            ),
            'feats-probabs-logger' : EvaluatorProbabsLogger(
                model_output_key='feats', 
                batch_target_key='feats', 
                batch_target_id_key='image_id',
                class_names=feat_names.copy()
            ),
    }

    test_callbacks = [ 
        PredictionsCallBack( 
                batch_target_key='diags',
                batch_target_id_key='image_id',
                evaluation_data_loader_keys=['train','val','test'], 
                evaluator_keys=['diags-probabs-logger'], 
                class_names=diag_names.copy(),
                use_argmax=True,
                use_class_names_pred_column=True,
                use_neptune_for_log=True,
                log_dir=fold_logdir,
            ),

        PredictionsCallBack(
                batch_target_key='feats',
                batch_target_id_key='image_id',
                evaluation_data_loader_keys=['train','val','test'], 
                evaluator_keys=['feats-probabs-logger'], 
                class_names=feat_names.copy(),
                use_thresh_from_dl_key='val',
                use_neptune_for_log=True,
                log_dir=fold_logdir
            ),
    ]

    _ = System_dict['diags'].test(
        evaluators=test_evals,
        evaluation_data_loaders={
            'train' : train_dataloader,
            'val'   : val_tuning_dataloader,
            'test'  : val_final_dataloader
        },
        batch_input_key='image',
        callbacks=test_callbacks,
        verbose=True,
        eval_verbose=False
    )
#%%
