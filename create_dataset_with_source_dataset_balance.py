# %%
import os

import matplotlib.pyplot as plt
import numpy as np
from numpy.core.numeric import _correlate_dispatcher
import pandas as pd
import scipy
from skmultilearn.model_selection import IterativeStratification
from torch import frac
from utils.utils import makefolder_ifnotexists
from collections import Counter
from skmultilearn.model_selection.measures import \
    get_combination_wise_output_matrix

class_names = ["MEL", "NV", "BKL"]  # remove the 9th "UNK" class
non_discriminative_feat_names = [
    "pigment_network", "globules", "negative_network", "streaks", "milia_like_cyst"]

discriminative_feat_names_full = [
    'pigmt_net_typical_includes_pseudonet',
    'dots_globs_reg',
    'negative_network',
    'strks_or_rad_strming',
    'MLCs_cloud',
    'granularity',
    'ker_plugs',
    'wobble',
    'scale',
    'blood',
    'atyp_net',
    'bld_vessels',
    'scarlike_regress',
    'ann_gran',
    'irreg_dots_globs',
    'blotch',
    'homog_tan_yellowish',
    'frambes_cerebriform_fernlike_pattern',
    'rough_covered_with_irreg_hyperkeratosis'
]

# %%
def get_value_counts(data_Series):
    value_list = list(data_Series.value_counts().values)
    if any(value_list):
        return value_list[0]
    else:
        return 0

# my_df.to_csv('my_df_meta.csv')
def set_row_class(row):
    if row.MEL == 1:
        return 'MEL'
    if row.BKL == 1:
        return 'BKL'
    if row.NV == 1:
        return 'NV'

def add_dataset_class(source_df):
    source_df = source_df.replace({'dataset_class': r'.*?BCN.*\/?'}, {'dataset_class': 'BCN'}, regex=True)
    source_df = source_df.replace({'dataset_class': r'.*?HAM.*\/?'}, {'dataset_class': 'HAM'}, regex=True)
    source_df = source_df.replace({'dataset_class': r'.*?MSK.*\/?'}, {'dataset_class': 'MSK'}, regex=True)
    source_df = source_df.replace({'dataset_class': r'.*?ISIC.*\/?'}, {'dataset_class': 'ISIC'}, regex=True)
    return source_df

def get_diags_to_source_dset_nums(source_df):
    class_dset_sum_dict = {}
    for class_ in class_names:
        class_df_test = source_df[source_df[class_] == 1]
        class_dset_sum_dict[class_] = class_df_test['dataset_class'].value_counts().to_dict()

    return class_dset_sum_dict



# %%
csv_dir = os.path.join('csv_files')
fold_save_dir = os.path.join('./data_folds/5foldTrainVal_ftbal_no_lesnid_leak_src_balcd_nets_only')


# %% Load isic19 dataset and fill all empty cells
isic_19_gt_meta_df = pd.read_csv('isic19_gt_meta.csv', skipinitialspace=True)
values = {'age_approx':0,	'anatom_site_general' : 'NA',	'lesion_id' : 'no_id',	'sex': 'unk'}
isic_19_gt_meta_df = isic_19_gt_meta_df.fillna(value=values)


# %% Add a lesion id and dataset class
isic_19_gt_meta_df.loc[(isic_19_gt_meta_df['lesion_id'] == 'no_id'), 'lesion_id'] = isic_19_gt_meta_df['image'][isic_19_gt_meta_df['lesion_id'] == 'no_id'] 
isic_19_gt_meta_df['dataset_class'] = isic_19_gt_meta_df['lesion_id']
unique_ids = isic_19_gt_meta_df.lesion_id.unique()
isic_19_gt_meta_df = add_dataset_class(isic_19_gt_meta_df)


# %% Load train df and add a str class label and add the meta data to it
csv_file_my_test = os.path.join('./data_folds/5foldTrainVal_featbalanced_no_lesion_id_leak/test_dset_class_balanced.csv')
csv_file_my_train1 = os.path.join(csv_dir, 'Final_dataset_to_train.csv')
csv_file_my_train2 = os.path.join(csv_dir, 'anand_to_annote_ALL.csv')

my_df1 = pd.read_csv(csv_file_my_train1, skipinitialspace=True)
my_df1 = my_df1.drop(['sum_class','sum'], axis=1)
my_df1 = my_df1.fillna(0)

my_df2 = pd.read_csv(csv_file_my_train2, skipinitialspace=True)
my_df2 = my_df2.drop(['age_approx','comment','anatom_site_general','lesion_id', 'sex','dataset_class'], axis=1)
my_df2 = my_df2.fillna(0)


#%%
my_df = pd.concat([my_df1, my_df2],  ignore_index=True)
#%% correct annotations that are not 1
# use this to remove all non-numeric stuff -> df['B'] = df['B'].str.replace(r'[^\d]+', '')
for feat in discriminative_feat_names_full:
    my_df.loc[my_df[feat]>0, feat] = 1


#%%
my_df = my_df.merge(isic_19_gt_meta_df[['image', 'lesion_id']], how='inner', on='image')


#%%
my_df['dataset_class'] = my_df['lesion_id']
my_unique_ids = my_df.lesion_id.unique()

my_df = add_dataset_class(my_df)
my_df_dset_percents = my_df['dataset_class'].value_counts(normalize=True)

# create a column with class name
my_df['class'] = my_df.apply(lambda row: set_row_class(row), axis = 1)

# find rows with same lesion_id but different class names (i.e. diagnosis)
prob_df = my_df[my_df.groupby(['lesion_id'])['class'].transform('nunique') > 1]


#%%
# Remove rows with duplicate lesion ids from my train df if they exist (they shouldnt)
# my_unique_lesion_df = my_df.drop_duplicates(['lesion_id'])
my_unique_lesion_df = my_df


# %% load test dataframe 
my_test_df = pd.read_csv(csv_file_my_test, skipinitialspace=True)
unique_test_ids = my_test_df.lesion_id.unique()


#%% ensure Removal of all images in the train/val dataset which are in my test dataset
my_unique_lesion_df = my_unique_lesion_df[my_unique_lesion_df['lesion_id'].isin(unique_test_ids) == False]
my_unique_lesion_df.to_csv(os.path.join(fold_save_dir,'full_bal_dset.csv'), index=False, header=True)


#%% Remove unwanted feats
# used reduced feats first a
discriminative_feat_names = [
    'pigmt_net_typical_includes_pseudonet',
    'atyp_net'
]

for feat in discriminative_feat_names_full:
    if not (feat in discriminative_feat_names):
        my_unique_lesion_df = my_unique_lesion_df.drop([feat], axis=1)


#%%
data_df = my_unique_lesion_df
class_dfs = {}
for class_ in class_names:
    class_dfs[class_] = data_df[data_df[class_] == 1]

#  Iterative stratification
n_splits = 5
order = 3
k_fold = IterativeStratification(n_splits=n_splits, order=order)

# classwise_fold = {}
train_final_folds = n_splits * [pd.DataFrame()]
val_final_folds = n_splits * [pd.DataFrame()]


#%%
# Stratify based on class and features
for class_, class_df in class_dfs.items():
    stratification_on =  scipy.sparse.lil_matrix(class_df.loc[:,discriminative_feat_names].to_numpy())
    class_index_vals =  np.array(class_df.index.values.tolist()).transpose()
    fold_idx = 0
    for train_index, val_index in k_fold.split(class_df, stratification_on):

        train_diagfeat_discriminative_df  = class_df.iloc[train_index, :]
        val_diagfeat_discriminative_df = class_df.iloc[val_index, :]

        train_final_folds[fold_idx] = train_final_folds[fold_idx].append(train_diagfeat_discriminative_df)
        val_final_folds[fold_idx] =  val_final_folds[fold_idx].append(val_diagfeat_discriminative_df)
        fold_idx += 1


# %% For each unique lesion id ensure that the same id doesnt appear across the
# train and val sets for each fold. If a unique id is overrepresented in a set 
# then move all of that id to that set
my_df_lesion_id_cnts = data_df['lesion_id'].value_counts()
for fold_idx in range(n_splits):
    train_df = train_final_folds[fold_idx]
    val_df = val_final_folds[fold_idx]

    for lesion_id in list(my_df_lesion_id_cnts.keys()):
        train_lesion_id_df = train_df[train_df['lesion_id'] == lesion_id]
        train_lesion_id_cnts = get_value_counts(train_lesion_id_df['lesion_id'])
        
        val_lesion_id_df = val_df[val_df['lesion_id'] == lesion_id]
        val_lesion_id_cnts = get_value_counts(val_lesion_id_df['lesion_id'])
        if train_lesion_id_cnts <= val_lesion_id_cnts:
            # move rows to val dataset
            val_final_folds[fold_idx] = val_final_folds[fold_idx].append(train_lesion_id_df)
            train_final_folds[fold_idx] = train_final_folds[fold_idx].drop(train_lesion_id_df.index)

        else:
            # move rows to train dataset
            train_final_folds[fold_idx] = train_final_folds[fold_idx].append(val_lesion_id_df)
            val_final_folds[fold_idx] = val_final_folds[fold_idx].drop(val_lesion_id_df.index)


#%% Sanity check to see if train and val sets have no leaks
for idx in range(n_splits):
    train_fold_df = train_final_folds[idx]
    val_fold_df = val_final_folds[idx]

    bool_df = train_fold_df.isin({'lesion_id': list(val_fold_df['lesion_id'].unique()) })
    if bool_df['lesion_id'].sum() > 0:
        print('Data leak in fold {}'.format(idx))
        print(train_fold_df[bool_df].head())
        print('\n\n')
    else:
        print('No data leak in {}'.format(idx))


#%%
class_sum_dict = {}
for idx in range(n_splits):
    train_fold_df = train_final_folds[idx]
    tr_s = train_fold_df.sum(numeric_only=True) 
    tr_s = tr_s[discriminative_feat_names] # / tr_s[discriminative_feat_names].sum()

    val_fold_df = val_final_folds[idx]
    vl_s = val_fold_df.sum(numeric_only=True)
    vl_s = vl_s[discriminative_feat_names] #/ vl_s[discriminative_feat_names].sum()

    class_sum_dict.update(
        {
            'tr_fld{}_feats'.format(idx): tr_s,
            'vl_fld{}_feats'.format(idx): vl_s,
        }
    )

bar_df = pd.DataFrame(class_sum_dict).fillna(0.0)
ax = bar_df.plot.bar(figsize=(16, 6))


#%%
dset_classes = ['ISIC', 'MSK', 'BCN', 'HAM']

class_dset_sum_dict = {}
for class_ in class_names:
    class_df_test = data_df[data_df[class_] == 1]

    class_dset_sum_dict[class_] = {}

    for dset in dset_classes:
        dset_counts = get_value_counts(class_df_test[class_df_test['dataset_class'] ==  dset]['dataset_class'])
        class_dset_sum_dict[class_][dset] = dset_counts

bar_df = pd.DataFrame(class_dset_sum_dict).fillna(0.0)
ax = bar_df.plot.bar(figsize=(10, 6), align='center')


#%% save folds
makefolder_ifnotexists(fold_save_dir)
for idx in range(n_splits):
    train_fold_df = train_final_folds[idx]
    val_fold_df = val_final_folds[idx]

    train_fold_df.to_csv(
        os.path.join(
            fold_save_dir,
            'train_featbalanced_{}fold{}.csv'.format(n_splits, idx)
        ),
        index=False, header=True
    )

    val_fold_df.to_csv(
        os.path.join(
            fold_save_dir,
            'val_featbalanced_{}fold{}.csv'.format(n_splits, idx)
        ),
        index=False, header=True
    )


#%%



















# %%
# Create new class based features so (3 diags x 19 feats) = 57 new class based feats

for feat_ in discriminative_feat_names:
    for diag_ in class_names:
        my_df[diag_+'_'+feat_] = my_df[feat_] * my_df[diag_]
    
    my_df = my_df.drop(columns=[feat_])
    

my_df.head()

# %%
# pd.options.display.max_rows = 4000

my_df.sum(numeric_only=True)



# %%

