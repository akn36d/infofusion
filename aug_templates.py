#%%
from albumentations import (CLAHE, Blur, CenterCrop, Compose, Flip, GaussNoise,
                            GridDistortion, HorizontalFlip, HueSaturationValue,
                            GaussNoise, IAAEmboss,
                            IAAPerspective, IAAPiecewiseAffine, IAASharpen,
                            MedianBlur, MotionBlur, Normalize, OneOf,
                            OpticalDistortion, RandomBrightnessContrast,
                            RandomCrop, RandomRotate90, Resize,PadIfNeeded,
                            ShiftScaleRotate, Transpose)

(imagenet_mean, imagenet_std) = ([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]) 

def get_train_aug_list():
    
    return [
        Flip(p=0.6),
        ShiftScaleRotate(p=0.7),
        GaussNoise(p=0.25),
        OneOf([
            MotionBlur(p=.1),
            MedianBlur(blur_limit=3, p=0.05),
            Blur(blur_limit=3, p=0.05),
        ], p=0.5),
        RandomBrightnessContrast(p=0.5),            
        HueSaturationValue(p=0.3),
        Normalize(
            mean=imagenet_mean,
            std=imagenet_std,
        )
    ]

def get_val_aug_list():
    return [
        Normalize(
            mean=imagenet_mean,
            std=imagenet_std,
        )
    ]   


# %%
