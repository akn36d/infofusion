#%%
import shutil, os
import pandas as pd

#%%
data_dir = os.path.join('/usr/local/home/akn36d/Downloads/ISIC_2019_Training_Input')
fold_save_dir = os.path.join('./data_folds/5foldTrainVal_featbalanced_no_lesion_id_leak_src_balanced')
csv_dir = os.path.join(fold_save_dir,'to_annote.csv')
df_to_annotate = pd.read_csv(csv_dir)

classes = ["MEL", "BKL", "NV"]

dest_dir = os.path.join('/usr/local/home/akn36d/Downloads/Anand_to_annote_isic19')


#%%

for c_ in classes:
    class_df = df_to_annotate[df_to_annotate[c_]==1]

    img_list = class_df['image'].tolist()

    c_dest_path = os.path.join(dest_dir,c_)
    for img_id in img_list:
        img_path = os.path.join(data_dir, img_id+'.jpg')
        shutil.copy(img_path, c_dest_path)
#%%