#%%
from __future__ import division, print_function

import copy
import csv
import os
import sys
import time
import warnings
from pprint import pprint

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pandas import DataFrame as df
from sklearn.model_selection import StratifiedShuffleSplit

# from utils.data_utils import extract_csv_data, calc_class_weights, my_csv2df
from utils.dataset import ISIC_dataset
from utils.transforms import albu_aug_train, albu_aug_val
from utils.utils import *
# from sanity_checks import *

# %%
# LOAD DATA
csv_dir = os.path.join('csv_files')
csv_files = {}
csv_files['test'] = {}
csv_files['train'] = {}

csv_files['test']['diag'] = os.path.join(csv_dir,'Testing_diag_gt_reducedClasses.csv')

csv_files['train']['diag'] = os.path.join(csv_dir, 'Training_diag_gt_reducedClasses.csv')
csv_files['train']['feat'] = os.path.join(csv_dir, 'Training_feat_gt_reducedFeats.csv')
csv_files['train']['feat_extended'] = os.path.join(csv_dir, 'combined_corrected_Dataset_feat_gt_allFeats.csv')
csv_files['train']['diagfeat'] = os.path.join(csv_dir, 'Training_diagfeat_gt_reducedClasses_reducedFeats.csv')
csv_files['train']['diagfeat_extended'] = os.path.join(csv_dir, 'combined_corrected_Dataset_diagfeat_gt_reducedClasses_allFeats.csv')

data_dir = os.path.join('/usr/local/home/akn36d/Downloads/ISIC_2019_Training_Input')

class_names = ["MEL","NV","BKL"] # remove the 9th "UNK" class
feat_names = ["pigment_network","globules","negative_network","streaks","milia_like_cyst"]
extended_feat_names = ['pigment_network', 'dots_globules', 'negative_network', 'streaks_incl_rad_streaming',	\
                       'milia_like_cyst', 'granularity', 'ker_plugs', 'wobble', 'scale', 'blood', 'atyp_net',   \
                       'blood_vessels', 'scarlike_regression']
                       
class_list = {}
class_list['feat'] = feat_names
class_list['diagfeat']= class_names + feat_names
class_list['diagfeat_extended'] = class_names + extended_feat_names

fold_mode = 'diagfeat_extended' # 'feat', 'diag', 'diagfeat', 'diagfeat_extended'
# %%
# Extract train csv to df
full_dataset = ISIC_dataset(File_list_csv=csv_files['train']['diagfeat_extended'],
                            diag_list=class_names,
                            feat_list=extended_feat_names,
                            phase='val',
                            transform=None,
                            no_feats_class=True)
                            
# %%

print('Class nums : ')
print(full_dataset.class_nums)

n_splits = 5
r_state = 3
sss = StratifiedShuffleSplit(n_splits=n_splits, random_state=r_state)

strat_class_onehot =  np.array(full_dataset.data_df.loc[:,class_names].values.tolist())

stratification_class_vals = np.argmax(strat_class_onehot[:,:len(class_names)], axis=1)
print('ids for strat : {}'.format(stratification_class_vals))
print('shape of strat ids : {}'.format(stratification_class_vals.shape))
print('The unique class ids : {}'.format(np.unique(stratification_class_vals)))

full_dataset.append_img_dir_to_df(img_dir=data_dir)

#%%

data_df = full_dataset.data_df

fold_idx = 0
for train_index, val_index in sss.split(data_df, stratification_class_vals):
    train_index = np.asarray(train_index)
    val_index = np.asarray(val_index)

    train_diagfeat_extended_df, val_diagfeat_extended_final_df = data_df.iloc[train_index,:], data_df.iloc[val_index,:]
    train_diagfeat_extended_final_df = train_diagfeat_extended_df.sample(frac=0.75,random_state=r_state)
    val_diagfeat_extended_tuning_df = train_diagfeat_extended_df.drop(train_diagfeat_extended_final_df.index)


    train_diagfeat_extended_final_df.to_csv('train18_diagfeatExt_final_{}fold{}.csv'.format(n_splits,fold_idx),  index=False, header=True)
    val_diagfeat_extended_tuning_df.to_csv('val18_diagfeatExt_tuning_{}fold{}.csv'.format(n_splits,fold_idx),  index=False, header=True)
    val_diagfeat_extended_final_df.to_csv('val18_diagfeatExt_final_{}fold{}.csv'.format(n_splits,fold_idx),  index=False, header=True)

    fold_idx += 1

