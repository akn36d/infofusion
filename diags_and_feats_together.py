#%%
import copy
import json
import os

import neptune
import numpy as np
import torch
from albumentations import (CenterCrop, Compose, Normalize, PadIfNeeded,
                            RandomCrop, Resize)
from albumentations import from_dict as txm_from_dict
from albumentations import to_dict as txm_to_dict
from flatten_dict import flatten as dict_flatten
from flatten_dict import unflatten as dict_unflatten
from pytorch_model_summary import summary
from pytorch_wrapper import System
from pytorch_wrapper.loss_wrappers import GenericPointWiseLossWrapper
from pytorch_wrapper.training_callbacks import (
    EarlyStoppingCriterionCallback, NumberOfEpochsStoppingCriterionCallback)
from torch import nn

from aug_templates import get_train_aug_list, get_val_aug_list
from callbacks_evals_templates import get_diags_callbacks, get_diags_evaluators
from config_helpers import print_helpers
from setup_helpers import (get_model_setup_helper, lr_optim_setup_helper,
                           train_dataloader_setup_helper,
                           val_dataloader_setup_helper)
from utils.callbacks import (ClearCacheCallback, FoldEvalsCallback,
                             NeptuneMacroAUCPlotsCallback, PredictionsCallBack)
from utils.metrics import EvaluatorProbabsLogger
from utils.utils import Environ, makefolder_ifnotexists, remove_logs
from utils.models import model

# LOAD DATA
csv_dir = os.path.join('csv_files')
data_dir = os.path.join('/usr/local/home/akn36d/Downloads/ISIC_2019_Training_Input')
gt_dir = os.path.join('/usr/local/home/akn36d/Downloads/')

diag_names = ["MEL","NV","BKL"] 

#%% PRELIM SETUP
last_activation = torch.nn.Sigmoid()
is_continue_train = False
if is_continue_train:
    config_dict_path = 'Models/diags-only/MUL-97_2021-02-24 17:20:07/configs/config_info.json'
    with open(config_dict_path) as file:
        config_dict = json.load(file)
        config_dict = dict_unflatten(config_dict,splitter='path')
    
else:
    config_dict = {}
    config_dict['suite_type'] = 'diags-only'
    config_dict['experiment'] = 'compare-with-multinput'
    config_dict['task_type'] = 'diagfeat' 
    config_dict['diag_names'] = diag_names
    config_dict['feat_names'] = None
    config_dict['no_feats_class'] = False

    config_dict['num_folds'] = 5
    config_dict['train_val_split'] = 0.25
    config_dict['n_epochs'] = 200
    config_dict['batch_size'] = 10
    config_dict['input_size'] = 224 # or 1024

    # Augmentation prefixes and postfixes which deal with image size
    train_aug_prefix = [
        Resize(256, 256), 
        CenterCrop(config_dict['input_size'], config_dict['input_size'])
    ]
    train_aug_postfix = []

    val_aug_prefix = train_aug_prefix
    val_aug_postfix = []

    train_aug_list =  Compose(train_aug_prefix + get_train_aug_list() + train_aug_postfix)  
    val_aug_list = Compose(val_aug_prefix + get_val_aug_list() + val_aug_postfix)

    config_dict['augmentation_txms'] = {}
    config_dict['augmentation_txms']['train'] = txm_to_dict(train_aug_list)
    config_dict['augmentation_txms']['val'] = txm_to_dict(val_aug_list)

    config_dict['balanced_sampler'] = {
        'enable' : False, 
        'dataset_scaler' : 1, 
        'balance_on' : 'diags'
    }

    config_dict['use_class_weights'] = True
    config_dict['early_stopping_patience'] = 5
    config_dict['early_stopping_on'] = 'loss-diags'

    config_dict['optim_type'] = 'adam' # 'sgd', 'adam'
    config_dict['use_last_activation'] = True
    lr_scheduler_type = 'ReduceLROnPlateau'
    lr_scheduled_on = 'loss-diags'

    # Setup defaults for lr scheduler params
    config_dict['lr_scheduler_config'] = {}
    config_dict['lr_scheduler_config'][config_dict['task_type']] = {
        'type' : 'ReduceLROnPlateau',
        'lr_scheduler_patience': 3,
        'init_lr' : 1e-5,
        'lr_scheduled_on' : 'loss-{}'.format(config_dict['task_type']) 
    }  
    config_dict['end_fc_lr_scaler'] = 1

    # Model specific params
    model_config = {
        'model_type' : 'efficientnet-b1',
        'freeze_blocks_len' : 0, 
    }
    config_dict.update(model_config)


#%%
# using neptune to create experiment for tracking and comparison
npt_project = neptune.init('ajaxis001/{}'.format(config_dict['suite_type']))
if is_continue_train:
    npt_experiment = npt_project.get_experiments(id=config_dict['neptune_id'])[0]
else:
    # Create run name 
    run_name = config_dict['task_type'] 
    npt_experiment = neptune.create_experiment(
        name=run_name,
        params=dict_flatten(config_dict.copy(), reducer='path'),
        tags=[config_dict['experiment']]
    )

    # Setup the environment 
    run_prefix = str(npt_experiment.id) + '_'
    environ = Environ(run_prefix=run_prefix)
    environ.create_suite_folders(config_dict['suite_type'])
    config_dict['neptune_id'] = npt_experiment.id

rand_seed = np.r_[0:config_dict['num_folds']]
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('GPU is available : {}'.format(torch.cuda.is_available()))
print(device)

#%%
folds_iter_num = config_dict['num_folds']
# fold_evals_callback = FoldEvalsCallback(class_names=config_dict['diag_names'], scalar_set_name='FoldAvgd', experiment=npt_experiment)
# ************************************************************************
# *                               Training                               *
# ************************************************************************
for fold_num in range(folds_iter_num): #range():
    
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    config_dict['fold_num'] = fold_num

    diag_names = config_dict['diag_names']

    # Setting the seed for the fold
    print_helpers(config_dict, case='fold')

    train_final_files = 'train18_diagfeatExt_final_{}fold{}.csv'.format(config_dict['num_folds'],fold_num)
    val_tuning_files = 'val18_diagfeatExt_tuning_{}fold{}.csv'.format(config_dict['num_folds'],fold_num)    
    val_final_files = 'val18_diagfeatExt_final_{}fold{}.csv'.format(config_dict['num_folds'],fold_num)    

    # Setting up folders to store model and info
    if not is_continue_train:
        if fold_num == 0 :
            environ.create_run_folders()
            environ_list = environ.__dict__
            config_dict['environ_list'] = environ_list
            print_helpers(config_dict)
            
            # save the config_dict as json for tracking
            with open(os.path.join(environ_list['config_path'], 'config_info.json'), 'w') as fp:
                json.dump(config_dict, fp, sort_keys=False, indent=4)
    else:
        environ_list = config_dict['environ_list']
        print_helpers(config_dict, case='continue')



    # file to save weights
    model_wts_fname = os.path.join(
        environ_list['save_weights_to_path'],
        "model_best_wts_fld{}.pt".format(fold_num)
    )
    if is_continue_train:
        if os.path.exists(model_wts_fname):
            print('Best weights for fold ({}) already found skipping to next fold. '.format(fold_num))    
            continue
        else:
            print('Resetting neptune logs for fold ({})'.format(fold_num))
            remove_logs(npt_experiment, 'Fold{}'.format(fold_num))

    # Specifying the train transforms, dataset and dataloader
    train_dataset, train_dataloader, class_weights, pos_weights = train_dataloader_setup_helper(
        config_dict, 
        train_final_files=train_final_files,
        device=device
    )
 
    # We use the imagenet mean and std since the pretrained networks are trained using imagenet
    train_dataset.transform = txm_from_dict(config_dict['augmentation_txms']['train'])

    # Specifying the val transforms and dataset
    val_tuning_dataset, val_tuning_dataloader = val_dataloader_setup_helper(
        config_dict,
        val_file=val_tuning_files,
        transforms=txm_from_dict(config_dict['augmentation_txms']['val']),
        phase='val'
    )

    val_final_dataset, val_final_dataloader = val_dataloader_setup_helper(
        config_dict,
        val_file=val_final_files,
        transforms=txm_from_dict(config_dict['augmentation_txms']['val']),
        phase='test'
    )
    
    # Setting up the loss   
    train_losswrapper_dict = {}
    train_losswrapper_dict.update({
        'loss-diags' : GenericPointWiseLossWrapper(
            nn.CrossEntropyLoss(
                weight=pos_weights['diags'], reduction='mean'), 
                model_output_key='diags',
                batch_target_key='diags'
            )                       
        }
    )   

    # Model creation   
    diagnosis_model = model(len(diag_names), model_type=config_dict['model_type'])

    end_fc_num_inputs = diagnosis_model.out_features + len(config_dict['feat_names'])
    end_fc = nn.Sequential(                                          
        nn.Linear(
            end_fc_num_inputs, 
            len(config_dict['diag_names'])
        )
    )  
    diagnosis_model.add_end_fc(end_fc, end_fc_num_inputs, len(config_dict['diag_names']))

    # Setting up the optimizers
    param_lr_list = [
        {'params' : diagnosis_model.model_net.parameters()}, 
        {
            'params' : diagnosis_model.end_fc.parameters(), 
            'lr' : config_dict['end_fc_lr_scaler'] * config_dict['lr_scheduler_config'][config_dict['task_type']]['init_lr']
        }    
    ]

    # Initialize a learning rate scheduler
    lr_scheduler_callback, optimizer_full_model = lr_optim_setup_helper(
        config_dict, 
        param_lr_list=param_lr_list,
        npt_experiment=npt_experiment
    )   

    # path to save stats generated from callbacks and evaluators
    fold_logdir = os.path.join(environ_list['log_path'], 'Fold{}_log'.format(fold_num))
    makefolder_ifnotexists(fold_logdir)

    statslog_fname = {}
    statslog_fname['diags'] = os.path.join(fold_logdir,'log_fold{}_diags.csv'.format(fold_num))

    tensorboard_logs = environ_list['tensorboard_log_path']
    save_checkpoint_fname = os.path.join(environ_list['save_weights_to_path'],"model_chkpnt.pt")
    
    # Setting up evaluators
    diags_evals = get_diags_evaluators(config_dict, train_losswrapper_dict)    

    # add callbacks
    callbacks = []
    diag_callbacks = get_diags_callbacks(
        config_dict, 
        statslog_fname['diags'], 
        tensorboard_logs,
        npt_experiment
    )
    callbacks += diag_callbacks

    standard_callbacks = [
        ClearCacheCallback(device=device),
        NumberOfEpochsStoppingCriterionCallback(config_dict['n_epochs']),
        EarlyStoppingCriterionCallback(
            patience=config_dict['early_stopping_patience'],
            evaluation_data_loader_key='val',
            evaluator_key=config_dict['early_stopping_on'],
            tmp_best_state_filepath=save_checkpoint_fname
        ),
    ]
    callbacks += standard_callbacks + lr_scheduler_callback

    if fold_num == 0: 
        # display model in tensorboard
        dataiter = iter(val_final_dataloader)
        data_dict = dataiter.next()
        _img =  data_dict['image'][0].unsqueeze(0).to(device=device)
        diagnosis_model = diagnosis_model.to(device)
        str_summary, info_dict= summary(diagnosis_model, _img, show_input=True, return_info_dict=True) # Print Model summary
        print(str_summary)
        npt_experiment.set_property('trainable_params',info_dict['trainable_params'])
        npt_experiment.set_property('total_params',info_dict['total_params'])
        
    # train the whole model
    diagnosis_model.output_as_dict = True
    System_diags = System(diagnosis_model, last_activation=last_activation, device=device)
    _ = System_diags.train(
        loss_wrapper=train_losswrapper_dict['loss-diags'],
        optimizer=optimizer_full_model,
        train_data_loader=train_dataloader,
        evaluators=diags_evals,
        evaluation_data_loaders={
            'train' : train_dataloader,
            'val'   : val_tuning_dataloader,
            'test'  : val_final_dataloader
        },
        batch_input_key='image',
        callbacks=callbacks,
        verbose=True,
        eval_verbose=False
    )

    # saving the model on the current fold
    model_best_wts = copy.deepcopy(diagnosis_model.state_dict())
    save_with_wts = {
        'model_types':config_dict['model_type'],
        'best_wts':model_best_wts,
        'last_activation': last_activation,
    }

    if pos_weights:
        save_with_wts.update({'pos_weights' : pos_weights})

    if class_weights:
        save_with_wts.update({'class_weights': class_weights})
   
    torch.save(save_with_wts,model_wts_fname)
    del save_with_wts

    # TESTING THE MODEL
    test_dls = {
        'train' : train_dataloader,
        'val'   : val_tuning_dataloader,
        'test'  : val_final_dataloader
    }
    test_dl_keys = list(test_dls.keys())

    # if fold_num == 0:
    #     fold_evals_callback.set_dl_eval_keys(eval_keys=diags_evals, dl_keys=test_dl_keys)



    test_evals = {
        'diags-probabs-logger' : EvaluatorProbabsLogger(
            model_output_key='diags', 
            batch_target_key='diags-onehot', 
            batch_target_id_key='image_id',
            class_names=diag_names.copy()
        ),
        'diags-onehot-probabs-logger' : EvaluatorProbabsLogger(
            model_output_key='diags', 
            batch_target_key='diags-onehot', 
            batch_target_id_key='image_id',
            class_names=diag_names.copy()
        ),
    }
    
    test_callbacks = [ 
        PredictionsCallBack( 
            batch_target_key='diags-onehot',
            batch_target_id_key='image_id',
            evaluation_data_loader_keys=test_dl_keys, 
            evaluator_keys=['diags-probabs-logger'], 
            class_names=diag_names.copy(),
            use_argmax=True,
            use_class_names_pred_column=True,
            use_thresh_from_dl_key='val',
            use_neptune_for_log=True,
            log_dir=fold_logdir,
            experiment=npt_experiment
        ),
        NeptuneMacroAUCPlotsCallback(
            batch_target_key='diags-onehot',
            eval_key = ['diags-onehot-probabs-logger'],
            class_names=diag_names.copy(),
            dl_keys=['val', 'test'],
            scalar_set_name='fld{}_roc_plot'.format(fold_num),
            experiment=npt_experiment,
            log_dir=fold_logdir
        )
    ]
    
    test_evals.update(diags_evals)
    fold_results_history = System_diags.test(
        evaluators=test_evals,
        evaluation_data_loaders=test_dls,
        batch_input_key='image',
        callbacks=test_callbacks,
        verbose=True,
        eval_verbose=False
    )

    del test_evals,test_callbacks
    del model_best_wts, System_diags, fold_results_history, diag_names
    # fold_evals_callback.on_every_fold_end(fold_results_history)

#%%
# fold_evals_callback.on_all_folds_end() 
