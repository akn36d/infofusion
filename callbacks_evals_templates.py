from pytorch_wrapper.loss_wrappers import GenericPointWiseLossWrapper
from pytorch_wrapper import evaluators as evaluators
from pytorch_wrapper.training_callbacks import (
    EarlyStoppingCriterionCallback, NumberOfEpochsStoppingCriterionCallback)

from utils.callbacks import (NeptuneEvalScalarsCallback,
                             PredictionsCallBack,
                             NeptuneConfusionMatPlotsCallback,
                             StatsLogCallBack,
                             TensorBoardEvalCombinedScalarsCallBack,
                             NeptuneMacroAUCPlotsCallback)

from utils.metrics import (HammingLossEvaluator,
                           MultiLabelF1Evaluator,
                           MultiLabelPrecisionEvaluator,
                           MultiLabelRecallEvaluator,
                           AUROCEvaluator)

from utils.metrics import EvaluatorProbabsLogger




def get_diags_evaluators(config_dict, train_losswrapper_dict):
    evals = {
        'loss-diags': evaluators.GenericPointWiseLossEvaluator(
            train_losswrapper_dict['loss-diags'],
            label='loss-diags',
            score_format='%f',
            batch_target_key='diags'
        ),
        'acc-diags': evaluators.MultiClassAccuracyEvaluator(
            model_output_key='diags',
            batch_target_key='diags'
        ),
        'macro-prec-diags': evaluators.MultiClassPrecisionEvaluator(
            model_output_key='diags',
            batch_target_key='diags',
            average='macro'
        ),
        'macro-rec-diags': evaluators.MultiClassRecallEvaluator(
            model_output_key='diags',
            batch_target_key='diags',
            average='macro'
        ),
        'macro-f1-diags': evaluators.MultiClassF1Evaluator(
            model_output_key='diags',
            batch_target_key='diags',
            average='macro'
        ),
        # 'macro-ovr-auc-diags': AUROCEvaluator(
        #     model_output_key='diags',
        #     batch_target_key='diags-onehot',
        #     class_names=config_dict['diag_names'],
        #     average='macro',
        #     multi_class='ovr'
        # ),
        # 'macro-ovo-auc-diags': AUROCEvaluator(
        #     model_output_key='diags',
        #     batch_target_key='diags-onehot',
        #     class_names=config_dict['diag_names'],
        #     average='macro',
        #     multi_class='ovo'
        # ),
    }

    if config_dict['task_type'] == 'diagfeat': 
        
        for _key, _loss_wrapper in train_losswrapper_dict.items():
            if config_dict['arch_type']!= 'diags-feats-pcgrad':
                if 'loss-total-pass2' in _key:
                    evals.update(
                        {
                            _key: evaluators.GenericPointWiseLossEvaluator(
                                _loss_wrapper,
                                label=_key,
                                score_format='%f',
                                batch_target_key='diags'
                            ),
                        }
                    )
                if 'loss-feats-l1' in _key:
                    evals.update(
                        {
                            _key: evaluators.GenericPointWiseLossEvaluator(
                                _loss_wrapper,
                                label=_key,
                                score_format='%f',
                                batch_target_key='diags'
                            ),
                        }
                    )
            else:
                if 'loss-total-val' in _key:
                    evals.update(
                        {
                            _key: evaluators.GenericPointWiseLossEvaluator(
                                _loss_wrapper,
                                label=_key,
                                score_format='%f',
                                batch_target_key='diags'
                            ),
                        }
                    )
    return evals


def get_diags_callbacks(config_dict, statslog_fname_diags, npt_experiment):
    tensorboard_logs = config_dict['environ_list']['tensorboard_log_path']

    npt_eval_scalars = [
        'loss-diags',
        'acc-diags',
        'macro-prec-diags',
        'macro-rec-diags',
        'macro-f1-diags',
        # 'macro-ovr-auc-diags',
    ]
    stats_log_scalars = ['loss-diags', 'acc-diags']
    if config_dict['task_type'] == 'diagfeat':
        if config_dict['arch_type']!= 'diags-feats-pcgrad':
            if 'loss-total-pass2' in config_dict['loss_weight_dict']:
                npt_eval_scalars += ['loss-total-pass2', ]
                stats_log_scalars += ['loss-total-pass2', ]
            if 'loss-feats-l1' in config_dict['loss_weight_dict']:
                npt_eval_scalars += ['loss-feats-l1', ]
                stats_log_scalars += ['loss-feats-l1', ]
        else:
            npt_eval_scalars += ['loss-total-val', ]
            stats_log_scalars += ['loss-total-val', ]
        

    callbacks = [
        NeptuneEvalScalarsCallback(
            evaluation_data_loader_keys=['val'],
            evaluator_keys=npt_eval_scalars,
            class_names=config_dict['diag_names'],
            scalar_set_name='Fold{}'.format(config_dict['fold_num']),
            experiment=npt_experiment
        ),
        # StatsLogCallBack(
        #     log_filepath=statslog_fname_diags,
        #     evaluation_data_loader_keys=['train', 'val'],
        #     evaluator_keys=stats_log_scalars,
        #     class_names=config_dict['diag_names']
        # ),
        TensorBoardEvalCombinedScalarsCallBack(
            log_dir=tensorboard_logs,
            dataloader_evaluator_dict={
                'val': ['loss-diags']
            },
            scalar_set_name='{}'.format(config_dict['fold_num'])
        ),
        TensorBoardEvalCombinedScalarsCallBack(
            log_dir=tensorboard_logs,
            dataloader_evaluator_dict={
                'val': ['loss-diags', 'acc-diags']
            },
            evaluator_scaling_dict={'acc-diags': 0.01},
            scalar_set_name='{}'.format(config_dict['fold_num']),
            metrics_set_name='loss-acc-diags'
        ),
    ]

    return callbacks


def get_diags_test_evals(config_dict):
    return {
        'diags-probabs-logger': EvaluatorProbabsLogger(
            model_output_key='diags',
            batch_target_key='diags-onehot',
            batch_target_id_key='image_id',
            class_names=config_dict['diag_names'].copy()
        ),
        'diags-onehot-probabs-logger': EvaluatorProbabsLogger(
            model_output_key='diags',
            batch_target_key='diags-onehot',
            batch_target_id_key='image_id',
            class_names=config_dict['diag_names'].copy()
        ),
        'acc-diags': evaluators.MultiClassAccuracyEvaluator(
            model_output_key='diags',
            batch_target_key='diags'
        ),
        'macro-prec-diags': evaluators.MultiClassPrecisionEvaluator(
            model_output_key='diags',
            batch_target_key='diags',
            average='macro'
        ),
        'macro-rec-diags': evaluators.MultiClassRecallEvaluator(
            model_output_key='diags',
            batch_target_key='diags',
            average='macro'
        ),
        'macro-f1-diags': evaluators.MultiClassF1Evaluator(
            model_output_key='diags',
            batch_target_key='diags',
            average='macro'
        ),
        # 'macro-ovr-auc-diags': AUROCEvaluator(
        #     model_output_key='diags',
        #     batch_target_key='diags-onehot',
        #     class_names=config_dict['diag_names'].copy(),
        #     average='macro',
        #     multi_class='ovr'
        # ),
        # 'macro-ovo-auc-diags': AUROCEvaluator(
        #     model_output_key='diags',
        #     batch_target_key='diags-onehot',
        #     class_names=config_dict['diag_names'].copy(),
        #     average='macro',
        #     multi_class='ovo'
        # ),
    }


def get_diags_test_callbacks(config_dict, fold_logdir, test_dl_keys, npt_experiment):
    npt_eval_scalars = [
        'acc-diags',
        'macro-prec-diags',
        'macro-rec-diags',
        'macro-f1-diags',
        # 'macro-ovr-auc-diags',
    ]

    return [
        PredictionsCallBack(
            batch_target_key='diags-onehot',
            batch_target_id_key='image_id',
            evaluation_data_loader_keys=test_dl_keys,
            evaluator_keys=['diags-probabs-logger'],
            class_names=config_dict['diag_names'].copy(),
            use_argmax=True,
            use_class_names_pred_column=True,
            use_thresh_from_dl_key=None,
            use_neptune_for_log=True,
            log_dir=fold_logdir,
            experiment=npt_experiment
        ),
        NeptuneMacroAUCPlotsCallback(
            batch_target_key='diags-onehot',
            eval_key=['diags-onehot-probabs-logger'],
            class_names=config_dict['diag_names'].copy(),
            dl_keys=test_dl_keys,
            scalar_set_name='fld{}_diags_roc_plot'.format(
                config_dict['fold_num']),
            experiment=npt_experiment,
            log_dir=fold_logdir
        ),
        NeptuneConfusionMatPlotsCallback(
            batch_target_key='diags',
            eval_key=['diags-probabs-logger'],
            class_names=config_dict['diag_names'].copy(),
            dl_keys=test_dl_keys,
            scalar_set_name='fld{}_diags_cm'.format(
                config_dict['fold_num']),
            experiment=npt_experiment,
            log_dir=fold_logdir
        ),
        NeptuneEvalScalarsCallback(
            evaluation_data_loader_keys=test_dl_keys,
            evaluator_keys=npt_eval_scalars,
            class_names=config_dict['diag_names'].copy(),
            scalar_set_name='testing_Fold{}'.format(config_dict['fold_num']),
            experiment=npt_experiment
        ),
    ]













def get_feats_evaluators(config_dict, train_losswrapper_dict):
    evals = {
        'loss-feats': evaluators.GenericPointWiseLossEvaluator(
            train_losswrapper_dict['loss-feats'],
            label='loss-feats',
            score_format='%f',
            batch_target_key='feats'
        ),
        'hamming-loss-feats': HammingLossEvaluator(
            model_output_key='feats',
            batch_target_key='feats',
            class_names=config_dict['feat_names'],
        ),
        # 'weighted-prec-feats': MultiLabelPrecisionEvaluator(
        #     model_output_key='feats',
        #     batch_target_key='feats',
        #     class_names=config_dict['feat_names'],
        #     average='weighted'
        # ),
        # 'weighted-rec-feats': MultiLabelRecallEvaluator(
        #     model_output_key='feats',
        #     batch_target_key='feats',
        #     class_names=config_dict['feat_names'],
        #     average='weighted'
        # ),
        # 'weighted-f1-feats': MultiLabelF1Evaluator(
        #     model_output_key='feats',
        #     batch_target_key='feats',
        #     class_names=config_dict['feat_names'],
        #     average='weighted'
        # ),
        # 'macro-ovo-auc-feats': AUROCEvaluator(
        #     model_output_key='feats',
        #     batch_target_key='feats',
        #     class_names=config_dict['feat_names'],
        #     average='macro',
        #     multi_class='ovo'
        # ),
        'macro-ovr-auc-feats': AUROCEvaluator(
            model_output_key='feats',
            batch_target_key='feats',
            class_names=config_dict['feat_names'],
            average='macro',
            multi_class='ovr'
        ),
    }

    if config_dict['arch_type'] != 'diags-feats-pcgrad':
        evals.update(
            {
                'loss-feats-l1': evaluators.GenericPointWiseLossEvaluator(
                    train_losswrapper_dict['loss-feats-l1'],
                    label='loss-feats-l1',
                    score_format='%f',
                    batch_target_key='feats'
                )
            }
        )

    return evals



def get_feats_callbacks(config_dict, statslog_fname_feats, tensorboard_logs, npt_experiment):

    evaluator_keys = [
        'loss-feats',
        'hamming-loss-feats',
        # 'weighted-prec-feats',
        # 'weighted-rec-feats',
        # 'weighted-f1-feats',
        # 'macro-ovo-auc-feats',
        'macro-ovr-auc-feats'
    ]
    
    if config_dict['arch_type'] != 'diags-feats-pcgrad':
        evaluator_keys += ['loss-feats-l1']

    callbacks = [
        NeptuneEvalScalarsCallback(
            evaluation_data_loader_keys=['val'],
            evaluator_keys=evaluator_keys,
            class_names=config_dict['feat_names'],
            scalar_set_name='Fold{}'.format(config_dict['fold_num']),
            experiment=npt_experiment
        ),
        # StatsLogCallBack(
        #     log_filepath=statslog_fname_feats,
        #     evaluation_data_loader_keys=['train', 'val'],
        #     evaluator_keys=['loss-feats', 'hamming-loss-feats'],
        #     class_names=config_dict['feat_names']
        # ),
        TensorBoardEvalCombinedScalarsCallBack(
            log_dir=tensorboard_logs,
            dataloader_evaluator_dict={
                'val': ['loss-feats', 'hamming-loss-feats'],
            },
            scalar_set_name='{}'.format(config_dict['fold_num']),
            metrics_set_name='loss-hamming_loss'
        ),
    ]
    return callbacks


def get_feats_test_evals(config_dict):
    return {
        'feats-probabs-logger': EvaluatorProbabsLogger(
            model_output_key='feats',
            batch_target_key='feats',
            batch_target_id_key='image_id',
            class_names=config_dict['feat_names'].copy()
        ),
        'hamming-loss-feats': HammingLossEvaluator(
            model_output_key='feats',
            batch_target_key='feats',
            class_names=config_dict['feat_names'].copy(),
        ),
        # 'macro-prec-feats': MultiLabelPrecisionEvaluator(
        #     model_output_key='feats',
        #     batch_target_key='feats',
        #     class_names=config_dict['feat_names'],
        #     average='macro'
        # ),
        # 'macro-rec-feats': MultiLabelRecallEvaluator(
        #     model_output_key='feats',
        #     batch_target_key='feats',
        #     class_names=config_dict['feat_names'],
        #     average='macro'
        # ),
        # 'macro-f1-feats': MultiLabelF1Evaluator(
        #     model_output_key='feats',
        #     batch_target_key='feats',
        #     class_names=config_dict['feat_names'],
        #     average='macro'
        # ),
        # 'weighted-prec-feats': MultiLabelPrecisionEvaluator(
        #     model_output_key='feats',
        #     batch_target_key='feats',
        #     class_names=config_dict['feat_names'],
        #     average='weighted'
        # ),
        # 'weighted-rec-feats': MultiLabelRecallEvaluator(
        #     model_output_key='feats',
        #     batch_target_key='feats',
        #     class_names=config_dict['feat_names'],
        #     average='weighted'
        # ),
        # 'weighted-f1-feats': MultiLabelF1Evaluator(
        #     model_output_key='feats',
        #     batch_target_key='feats',
        #     class_names=config_dict['feat_names'],
        #     average='weighted'
        # ),
        'macro-ovr-auc-feats': AUROCEvaluator(
            model_output_key='feats',
            batch_target_key='feats',
            class_names=config_dict['feat_names'],
            average='macro',
            multi_class='ovr'
        ),
    }


def get_feats_test_callbacks(config_dict, fold_logdir, test_dl_keys, npt_experiment):

    npt_eval_scalars = [
        'hamming-loss-feats',
        # 'macro-prec-feats',
        # 'macro-rec-feats',
        # 'macro-f1-feats',
        # 'weighted-prec-feats',
        # 'weighted-rec-feats',
        # 'weighted-f1-feats',
        # 'macro-ovo-auc-feats',
        'macro-ovr-auc-feats'
    ]
    
    return [
        PredictionsCallBack(
            batch_target_key='feats',
            batch_target_id_key='image_id',
            evaluation_data_loader_keys=test_dl_keys,
            evaluator_keys=['feats-probabs-logger'],
            class_names=config_dict['feat_names'].copy(),
            use_thresh_from_dl_key='val',
            use_neptune_for_log=True,
            log_dir=fold_logdir,
            experiment=npt_experiment
        ),
        NeptuneMacroAUCPlotsCallback(
            batch_target_key='feats',
            eval_key=['feats-probabs-logger'],
            class_names=config_dict['feat_names'].copy(),
            dl_keys=test_dl_keys,
            scalar_set_name='fld{}_feats_roc_plot'.format(
                config_dict['fold_num']),
            experiment=npt_experiment,
            log_dir=fold_logdir
        ),
        NeptuneEvalScalarsCallback(
            evaluation_data_loader_keys=test_dl_keys,
            evaluator_keys=npt_eval_scalars,
            class_names=config_dict['feat_names'],
            scalar_set_name='testing_Fold{}'.format(config_dict['fold_num']),
            experiment=npt_experiment
        ),
    ]