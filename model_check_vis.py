#%%
from collections import OrderedDict
import re

from utils.models import freeze_blocks, model, my_model, getModel, tag_modules_with_names
from utils.dysion_models import getModel
from utils.model_blocks import get_param_blocks
import torch 
from setup_helpers import lr_optim_setup_helper
#%%
# REF : https://www.geeksforgeeks.org/python-grouping-similar-substrings-in-list/
# REF : https://stackoverflow.com/questions/24310945/group-items-by-string-pattern-in-python/24311596
# REF : https://stackoverflow.com/questions/36300158/split-text-after-the-second-occurrence-of-character

model_map_block_lambdas = OrderedDict([('Dense121',  lambda text: (re.findall("(denseblock[\d]+\.denselayer[\d]+)", text) + [text])[0]),
                        ('Dense169' , lambda text: (re.findall("(denseblock[\d]+\.denselayer[\d]+)", text) + [text])[0]),
                        ('Dense161' , lambda text: (re.findall("(denseblock[\d]+\.denselayer[\d]+)", text) + [text])[0]),
                        ('Dense201' , lambda text: (re.findall("(denseblock[\d]+\.denselayer[\d]+)", text) + [text])[0]),
                        ('Resnet50' , lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('Resnet101' , lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),   
                        # ('InceptionV3', None),# models.inception_v3(pretrained=True),
                        # ('se_resnext50', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('se_resnext101', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('se_resnet50', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('se_resnet101', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('se_resnet152', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('resnext101', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('resnext101_64', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('senet154', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('polynet', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('dpn92', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('dpn68b', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('nasnetamobile', lambda text: (re.findall("(cell_[\w]+)", text) + [text])[0]),
                        # ('resnext101_32_8_wsl', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('resnext101_32_16_wsl', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('resnext101_32_32_wsl', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        # ('resnext101_32_48_wsl', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('efficientnet-b0', lambda a: a.partition('._')[0]), 
                        ('efficientnet-b1', lambda a: a.partition('._')[0]), 
                        ('efficientnet-b2', lambda a: a.partition('._')[0]), 
                        ('efficientnet-b3', lambda a: a.partition('._')[0]),  
                        ('efficientnet-b4', lambda a: a.partition('._')[0]), 
                        # ('efficientnet-b5', lambda a: a.partition('._')[0]),  
                        # ('efficientnet-b6', lambda a: a.partition('._')[0]), 
                        # ('efficientnet-b7', lambda a: a.partition('._')[0]),
                    ])


#%% Checking input sizes to pretrained base models
# x = torch.rand(1,3,512,512)
# for model_type in list(model_map_block_lambdas.keys()):
#     mod = my_model(model_type=model_type, input_image_size=224)
#     try:
#         y = mod(x)
#         print(y.size())
#     except Exception as inst:
#         print(inst)

#%% Checking how to add model end classifier 

# class new_comb_model(nn.Module):
#     def __init__(self, num_ftrs, num_ftrs_new):
#         super(new_comb_model, self).__init__()
#         self.base = mod.model_net
#         self.end_fc = nn.Sequential(  
#             nn.Dropout(p=0.6), 
#             nn.Linear(num_ftrs, num_ftrs_new)
#         )          
#     def forward(self, x):
#        x = self.base(x)
#        x = self.end_fc(x)

#        return x
# %% Checking how block sizes will be created with newly modified model class 
# num_ftrs_new = 3
# model_type = 'Resnet50'
# mod = model(num_ftrs_new,model_type=model_type)
# freeze_blocks(mod, 3)
# mod.param_blocks
# %%
