# %%
import copy
import json
import os

# import pickle
import dill as pickle
import neptune
import numpy as np
import torch
from albumentations import from_dict as txm_from_dict
from flatten_dict import flatten as dict_flatten
from flatten_dict import unflatten as dict_unflatten
from torch import utils

from config_dct_templates import Config_grid_gen, grid_item
from setup_helpers import (display_save_model_info, loss_setup_helper,
                           lr_optim_setup_helper, model_setup_helper)

from utils.dataset import ISIC_dataset
from utils.utils import Environ, makefolder_ifnotexists, remove_logs
# from torchinfo import summary
from pytorch_model_summary import summary

# %%
from utils.models import model
from utils.HeirarchicalSystem import (HeirarchicalModel, HeirarchicalSystem,
                                      MultiInputModel, MultiInputSystem,
                                      MultiOutputModel)
from torch import nn

# %% LOAD DATA
with open('platform_id.json', 'rb') as fp:
    platform_id = json.load(fp)


if platform_id['project'] == 'infofusion':
    if platform_id['platform'] == 'r08stanleyj':
        model_suite_type = 'multitask_r08'
        data_dir = os.path.join('/usr/local/home/akn36d/Downloads/')
        environ = Environ()
        environ.create_suite_folders(model_suite_type)
        iterator_checkpoint = os.path.join(
            environ.suite_dirname,
            'config_iterator.pkl'
        )
        config_chkpt = os.path.join(
            environ.suite_dirname,
            'config_chkpt.json'
        )
        config_init_json = 'r08_multitask_cfg_init.json'
        cfg_dict_iterator = Config_grid_gen(
            config_init_json,
            iterator_checkpoint,
            config_chkpt
        )

        def grid_restrictions(grid_inst): return ((grid_item(
            grid_inst, 'batch_size') > 16) and (grid_item(grid_inst, 'input_size') > 224))
        cfg_dict_iterator.generate_config_grid(
            restriction_lambda=grid_restrictions
        )

    elif platform_id['platform'] == 'paperspace':
        data_dir = os.path.join('./data/')
        model_suite_type = 'multitask_paperspace_300'
        data_dir = os.path.join('./data/')
        environ = Environ()
        environ.create_suite_folders(model_suite_type)
        iterator_checkpoint = os.path.join(
            environ.suite_dirname,
            'config_iterator.pkl'
        )
        config_chkpt = os.path.join(
            environ.suite_dirname,
            'config_chkpt.json'
        )
        config_init_json = 'paperspace_multitask_cfg_init.json'
        cfg_dict_iterator = Config_grid_gen(
            config_init_json,
            iterator_checkpoint,
            config_chkpt
        )

        def grid_restrictions(grid_inst): return ((grid_item(
            grid_inst, 'batch_size') > 16) and (grid_item(grid_inst, 'input_size') > 224))
        cfg_dict_iterator.generate_config_grid(
            restriction_lambda=grid_restrictions)

# %%
cfg_list = []
for config_dict in cfg_dict_iterator:
    cfg_list.append(config_dict)


# %%
config_dict = cfg_list[0]
print(config_dict['arch_type'])

config_dict['fold_num'] = 0
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('GPU is available : {}'.format(torch.cuda.is_available()))
print(device)


# %%
if config_dict['arch_type'] == 'diags-only':
    full_model = model(model_type=config_dict['model_type'])

    end_fc_num_inputs = full_model.out_features
    end_fc = nn.Sequential(
        nn.Linear(
            end_fc_num_inputs, 
            len(config_dict['diag_names'])
        )
    )    
       
    full_model.add_end_fc(end_fc, len(config_dict['diag_names']))
    print(end_fc_num_inputs)
#     print(full_model)

if config_dict['arch_type'] == 'diags-feats':
    # Model creation
    models_dict = {}
    models_dict['base'] = model(model_type=config_dict['model_type'])
    print(models_dict['base'].out_features)
    
    
    
    # modify base models end classifier if required
    models_dict['diags'] = nn.Sequential(
            nn.Linear(
                models_dict['base'].out_features, len(config_dict['diag_names'])
            )
    )
    
    new_model = nn.Sequential(
        models_dict['base'],
        models_dict['diags']
    )
    
    models_dict['feats'] = nn.Sequential(
        nn.Dropout(p=0.5),
        nn.Linear(models_dict['base'].out_features, len(config_dict['feat_names']))
    )

    full_model = MultiOutputModel(models_dict, output_as_dict=True, device=device)


# %%
# display model params
_model = full_model.to(device)
_model.output_as_dict = False
# model_summary = summary(
#     _model,
#     input_size=(10, 3, 300, 300),
#     col_names=["kernel_size", "output_size", "num_params", "mult_adds"],
#     depth=4
# )
inp = torch.rand((10,3,300,300), device=device)
model_summary = summary(_model, inp, show_input=False, 
        print_summary=True, max_depth=1, show_parent_layers=True)


# %%
# display model params
_model = full_model.to(device)
_model.output_as_dict = False
# model_summary = summary(
#     _model,
#     input_size=(10, 3, 300, 300),
#     col_names=["kernel_size", "output_size", "num_params", "mult_adds"],
#     depth=4
# )
inp = torch.rand((10,3,300,300), device=device)
model_summary = summary(_model, inp, show_input=False, 
        print_summary=True, max_depth=1, show_parent_layers=True)

