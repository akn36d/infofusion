

def lr_optim_config_helper(config_dict, lr_scheduler_type, **kwargs):
    task_type = config_dict['task_type']
    if config_dict['task_type']=='diags' or config_dict['task_type']=='feats':
        if config_dict['optim_type'] == 'sgd':
            config_dict['sgd_momentum'] = 0.9

        config_dict_addendum = {}
        config_dict_addendum['lr_scheduler_config'] = {}
        if 'CosineAnnealingWarmupRestarts' in lr_scheduler_type:
            assert 'sgd' in config_dict['optim_type']
            config_dict_addendum['lr_scheduler_config'][task_type] = {
                'type' : lr_scheduler_type,
                'first_cycle_steps' : config_dict['n_epochs']-5, 
                'cycle_mult' : 1.0, 
                'max_lr' : 0.001, 
                'min_lr' : 0.0000001, 
                'warmup_steps' : 5, 
                'gamma' : .5,
            }

        if 'MultiStepLR' in lr_scheduler_type:
            config_dict_addendum['lr_scheduler_config'][task_type] = {
                'type' : lr_scheduler_type,
                'gamma' : .1,
                'milestones' : [10,30,70,120],
                'init_lr' : 0.001
            }

        elif 'ReduceLROnPlateau' in lr_scheduler_type:
            lr_scheduled_on = kwargs.pop('lr_scheduled_on', None)

            if not lr_scheduled_on:
                lr_scheduled_on = 'loss-{}'.format(task_type)
                print('No value given for `lr_scheduled_on` will use `{}`'.format(lr_scheduled_on))

            config_dict_addendum['lr_scheduler_config'][task_type] = {
                'type' : 'ReduceLROnPlateau',
                'lr_scheduler_patience': 5,
                'init_lr' : 1e-3,
                'lr_scheduled_on' : lr_scheduled_on 
            }    

        else:
            config_dict_addendum['lr_scheduler_config'][task_type] = {
                'type' : 'ReduceLROnPlateau',
                'lr_scheduler_patience': 5,
                'init_lr' : 3e-4,
                'lr_scheduled_on' : 'loss-{}'.format(task_type) 
            }    
            print('No `lr_scheduler_config` specified will use the following settings: \n : {}'.format(config_dict_addendum['lr_scheduler_config'][task_type]))
        config_dict.update(config_dict_addendum)

    elif config_dict['task_type'] == 'diagfeat':
        config_dict_addendum = {}
        config_dict_addendum['lr_scheduler_config'] = {}
        lr_scheduled_on = kwargs.pop('lr_scheduled_on', None)

        if not lr_scheduled_on:
            lr_scheduled_on = 'loss-total'
            print('No value given for `lr_scheduled_on` will use `{}`'.format(lr_scheduled_on))
            
        config_dict_addendum['lr_scheduler_config'][task_type] = {
                'type' : 'ReduceLROnPlateau',
                'lr_scheduler_patience': 3,
                'init_lr' : 3e-4,
                'lr_scheduled_on' : lr_scheduled_on
            }    
        config_dict.update(config_dict_addendum)
    
    return config_dict

def print_helpers(config_dict, case='default'):
    if case == 'default':
        print(2*'************',' DIRECTORY INFO',2*'************')
        print('The models will be stored in : {}'.format(config_dict['environ_list']['models_folder']))
        print('This model will be stored in : {}'.format(config_dict['environ_list']['suite_dirname']))
        print('The current run will be in : {}'.format(config_dict['environ_list']['run_dirname']))
        print(2*'************','***************',2*'************') 
    
    elif case == 'continue':
        print(2*'************',' DIRECTORY INFO',2*'************')
        print('This is a restart of the model')
        print('This model will be loaded from suite : {}'.format(config_dict['environ_list']['suite_dirname']))
        print('The run that is being continued is : {}'.format(config_dict['environ_list']['run_dirname']))
        print(2*'************','***************',2*'************') 
    
    elif case =='fold':
        print('\n')
        print(70*"=")
        print("FOLD - {}".format(config_dict['fold_num']))
        print(70*"=")

    else:
        print("Incorrect `case`. Expected one of ['default', 'continue', 'fold'] recieved {}.".format(case))
