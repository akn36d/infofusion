from config_helpers import lr_optim_config_helper

diag_names = ["MEL","NV","BKL"] 

# feat_names = ['dots_globules', 
#               'negative_network', 
#               'streaks_incl_rad_streaming',	                       
#               'milia_like_cyst',
#               'no_feats']

feat_names = ['pigment_network', 
              'dots_globules', 
              'negative_network', 
              'streaks_incl_rad_streaming',	                       
              'milia_like_cyst', 
              'granularity', 
              'ker_plugs', 
              'wobble', 
              'scale', 
              'blood', 
              'atyp_net',
              'blood_vessels', 
              'scarlike_regression',
              'no_feats']


# **********************************************************
# **             TEMPLATE FOR FEATS ONLY CASE             **
# **********************************************************
def get_config_template(task_type, overriddes=None):

    config_dict = {}
    if task_type == 'feats':
        config_dict['task_type'] = 'feats' 

        config_dict['diag_names'] = None
        config_dict['feat_names'] = feat_names
        config_dict['no_feats_class'] = True
        config_dict['augmentation_type'] = 'simple' #'v1', 'simple'
        
        config_dict['num_folds'] = 5
        config_dict['train_val_split'] = 0.25
        config_dict['n_epochs'] = 200
        config_dict['batch_size'] = 32

        config_dict['balanced_sampler'] = {
            'enable' : False, 
            'dataset_scaler' : 1, 
            'balance_on' : 'diags'
        }

        config_dict['use_class_weights'] = True
        config_dict['early_stopping_patience'] = 10
        config_dict['early_stopping_on'] = 'loss-feats'

        config_dict['optim_type'] = 'adam' # 'sgd', 'adam'
        config_dict['use_last_activation'] = True

        lr_scheduler_type = 'ReduceLROnPlateau'
        lr_scheduled_on = 'loss-feats'

        # Setup defaults for lr scheduler params
        config_dict = lr_optim_config_helper(config_dict,lr_scheduler_type,lr_scheduled_on=lr_scheduled_on)
        config_dict['input_size'] = 224
        tr_params_dict  = {'tr_RandomCrop'  : config_dict['input_size'], 'tr_Resize'  : 256}
        val_params_dict = {'val_CenterCrop' : config_dict['input_size'], 'val_Resize' : 256}
        config_dict['img_preprocessing_params'] = {
            'tr_params_dict':tr_params_dict, 
            'val_params_dict':val_params_dict
        }

        # Model specific params
        config_dict.update(
            {
                'model_type' : {
                    'image_to_ftrs_model' : 'Resnet50', 
                },
                'unfreeze_blocks_len' : { 
                    'image_to_ftrs_model'  : 'all', 
                },
            }
        )
    elif task_type == 'diags':
        config_dict['task_type'] = 'diags' 
        config_dict['diag_names'] = diag_names
        config_dict['feat_names'] = None
        config_dict['no_feats_class'] = False

        config_dict['num_folds'] = 5
        config_dict['train_val_split'] = 0.25
        config_dict['n_epochs'] = 200
        config_dict['batch_size'] = 32
        config_dict['augmentation_type'] = 'simple' #'v1', 'simple'
        config_dict['input_size'] = 224
        tr_params_dict  = {'tr_RandomCrop'  : config_dict['input_size'], 'tr_Resize'  : 256}
        val_params_dict = {'val_CenterCrop' : config_dict['input_size'], 'val_Resize' : 256}
        config_dict['img_preprocessing_params'] = {
            'tr_params_dict':tr_params_dict, 
            'val_params_dict':val_params_dict
        }

        config_dict['balanced_sampler'] = {
            'enable' : False, 
            'dataset_scaler' : 1, 
            'balance_on' : 'diags'
        }

        config_dict['use_class_weights'] = True
        config_dict['early_stopping_patience'] = 10
        config_dict['early_stopping_on'] = 'loss-diags'

        config_dict['optim_type'] = 'adam' # 'sgd', 'adam'
        config_dict['use_last_activation'] = True
        lr_scheduler_type = 'ReduceLROnPlateau'
        lr_scheduled_on = 'loss-diags'

        # Setup defaults for lr scheduler params
        config_dict = lr_optim_config_helper(config_dict,lr_scheduler_type,lr_scheduled_on=lr_scheduled_on)

        # Model specific params
        model_config = {
                'model_type' : {
                    'image_to_ftrs_model' : 'Resnet50', 
                },
                'unfreeze_blocks_len' : { 
                    'image_to_ftrs_model'  : 'all', 
                },
            }
        config_dict.update(model_config)

    if overriddes:
        config_dict.update(overriddes)
    return config_dict

