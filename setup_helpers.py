import os

import torch
from pytorch_wrapper import System
from pytorch_wrapper.loss_wrappers import GenericPointWiseLossWrapper
from pytorch_wrapper.training_callbacks import (
    EarlyStoppingCriterionCallback, NumberOfEpochsStoppingCriterionCallback)
from torch import nn, optim, utils
from torchinfo import summary

from callbacks_evals_templates import (get_diags_callbacks,
                                       get_diags_evaluators,
                                       get_diags_test_callbacks,
                                       get_diags_test_evals,
                                       get_feats_callbacks,
                                       get_feats_evaluators,
                                       get_feats_test_callbacks,
                                       get_feats_test_evals)
from utils.callbacks import (ClearCacheCallback, LearningRateSchedulerCallback,
                             SystemCheckpointCallback,
                             SystemCheckpointResetCallback)
from utils.dataset import (ImbalancedDatasetSampler, ISIC_dataset,
                           get_label_from_ISIC_Dataset)
from utils.losses import L1Regularizer, MultiLossWrapper
from utils.models import model, SimpleFC
from utils.System import (HeirarchicalModel, HeirarchicalSystem,
                          MultiInputModel, MultiInputSystem, MultiOutputModel,
                          MultiTaskSystem)
from utils.utils import dict_merge


def lr_optim_setup_helper(config_dict, param_lr_list, npt_experiment):
    fold_num = config_dict['fold_num']
    if config_dict['task_type']=='diags' or config_dict['task_type']=='feats':
        # SETTING UP OPTIMIZERS
        if config_dict['optim_type'] == 'adam':
            optimizer_full_model = optim.Adam(
                param_lr_list, 
                lr = config_dict['lr_scheduler_config']['init_lr']
            )
        elif config_dict['optim_type'] == 'sgd':
            optimizer_full_model = optim.SGD(
                param_lr_list, 
                lr = config_dict['lr_scheduler_config']['init_lr'],
                momentum=config_dict['lr_scheduler_config']['momentum']
            )
        else:
            raise "`config_dict['optim_type']` not specified or invalid."
            

        # SETTING UP LR SCHEDULERS
        if config_dict['lr_scheduler_config']['type'] == 'ReduceLROnPlateau':
            lr_scheduler = optim.lr_scheduler.ReduceLROnPlateau(
                optimizer_full_model, 
                mode='min', 
                patience=config_dict['lr_scheduler_config']['lr_scheduler_patience']
            )

            lr_scheduler_callback = [
                LearningRateSchedulerCallback(
                    lr_scheduler,
                    dataloader_key='val',
                    evaluator_key=config_dict['lr_scheduler_config']['lr_scheduled_on'],
                    use_neptune_for_log=True,
                    neptune_log_prefix='{}'.format(fold_num),
                    experiment=npt_experiment
                )
            ]
        elif config_dict['lr_scheduler_config']['type'] == 'MultiStepLR':
            lr_scheduler = optim.lr_scheduler.MultiStepLR(
                optimizer_full_model, 
                milestones = config_dict['lr_scheduler_config']['milestones'], 
                gamma      = config_dict['lr_scheduler_config']['gamma']
            )

            lr_scheduler_callback = [
                LearningRateSchedulerCallback(
                    lr_scheduler,
                    dataloader_key='val',
                    evaluator_key=None,
                    use_neptune_for_log=True,
                    neptune_log_prefix='{}'.format(fold_num),
                    experiment=npt_experiment
                )
            ]
        else:
            print('No scheduler specified will use constant learning rate.')
            lr_scheduler_callback = []
                        
        return lr_scheduler_callback, optimizer_full_model
    
    elif config_dict['task_type']=='diagfeat':

        if 'learning_rate' in config_dict:
            optimizer_full_model = optim.Adam(
                param_lr_list, 
                lr = config_dict['learning_rate']
            )
            print('`learning_rate` key specified in `config_dict` will use it as default for all learning rates not specified explicitly.')
        else:
            optimizer_full_model = optim.Adam(
                param_lr_list
            )

        # Initialize learning rate scheduler
        if config_dict['lr_scheduler_config']['type'] == 'ReduceLROnPlateau': 
            lr_scheduler = optim.lr_scheduler.ReduceLROnPlateau(
                    optimizer_full_model, 
                    mode='min', 
                    patience=config_dict['lr_scheduler_config']['lr_scheduler_patience']
                )
            lr_scheduler_callback = [
                LearningRateSchedulerCallback(
                    lr_scheduler,
                    dataloader_key='val',
                    evaluator_key=config_dict['lr_scheduler_config']['lr_scheduled_on'],
                    use_neptune_for_log=True,
                    neptune_log_prefix='{}'.format(fold_num),
                    experiment=npt_experiment
                )
            ]
            return lr_scheduler_callback, optimizer_full_model

    else:
        print("Invalid `task_type` in `config_dict` expected one of ['diags', 'feats', 'diagfeat'] recieved `{}`.".format(config_dict['task_type']))


def train_dataloader_setup_helper(config_dict, train_file, device):
    # Specifying the train transforms, dataset and dataloader
    train_dataset = ISIC_dataset(
        train_file,
        diag_list=config_dict['diag_names'], 
        feat_list=config_dict['feat_names'],
        phase='train',
        transform=None,
    ) 

    if config_dict['use_class_weights'] or config_dict['balanced_sampler']['enable']:
        class_weights = {}
        pos_weights = train_dataset.get_pos_weights()
        for key,value in pos_weights.items():
            pos_weights[key] = torch.tensor(value, device=device).float()
            class_weights[key] = 1 / (torch.tensor(train_dataset.class_nums[key].values, device=device).float())
    else:
        class_weights = {}
        pos_weights = {}
        if train_dataset.diags:
            pos_weights['diags'] = None
            class_weights['diags'] = None
            
        if train_dataset.feats:
            pos_weights['feats'] =  None
            class_weights['feats'] = None
        

    # Use the `feats` or `diags` classes for the balancing of the data 
    if config_dict['balanced_sampler']['enable']:
        balance_on  = config_dict['balanced_sampler']['balance_on'] 
        num_samples = int(config_dict['balanced_sampler']['dataset_scaler']*len(train_dataset)) 

        sampler = ImbalancedDatasetSampler(
            train_dataset, 
            num_samples=num_samples,
            callback_get_label=get_label_from_ISIC_Dataset
        )

        train_dataloader = utils.data.DataLoader(
            train_dataset, 
            batch_size=config_dict['batch_size'], 
            shuffle=False, 
            num_workers=0, 
            sampler=sampler
        )
    else:
        train_dataloader = utils.data.DataLoader(
            train_dataset, 
            batch_size=config_dict['batch_size'],  
            shuffle=True, 
            num_workers=0
        )
    return train_dataset, train_dataloader, class_weights, pos_weights

def val_dataloader_setup_helper(config_dict, val_file, transforms, phase):
    val_dataset = ISIC_dataset(
        val_file, 
        diag_list=config_dict['diag_names'], 
        feat_list=config_dict['feat_names'], 
        phase=phase,
        transform=transforms,
    )     
    
    val_dataloader = utils.data.DataLoader(
        val_dataset,
        batch_size=config_dict['batch_size'], 
        shuffle=True, 
        num_workers=0
    )
    return val_dataset, val_dataloader


def model_setup_helper(config_dict, device):
    if config_dict['arch_type'] == 'diags-only':
        full_model = model(model_type=config_dict['model_type'], output_as_dict=True, model_output_key='diags')

        end_fc_num_inputs = full_model.out_features 
        end_fc = nn.Sequential(                                          
            nn.Linear(
                end_fc_num_inputs, 
                len(config_dict['diag_names'])
            )
        )  

        if config_dict['num_base_feats'] > 0:
            if 'efficient' in config_dict['model_type']:
                end_fc = nn.Sequential(
                    nn.Linear(full_model.out_features, config_dict['num_base_feats'] + len(config_dict['feat_names'])),
                    nn.Dropout(p=0.5),
                    nn.Linear(config_dict['num_base_feats'] + len(config_dict['feat_names']), len(config_dict['diag_names'])),
                )
            else: 

                end_fc = nn.Sequential(  
                    nn.Dropout(p=0.5), 
                    nn.Linear(full_model.out_features, config_dict['num_base_feats'] + len(config_dict['feat_names'])),
                    nn.Linear(config_dict['num_base_feats'] + len(config_dict['feat_names']), len(config_dict['diag_names'])),
                )

        full_model.add_end_fc(end_fc, len(config_dict['diag_names']))

        # Setting up the optimizers
        param_lr_list = [
            {'params' : full_model.model_net.parameters()}, 
            {
                'params' : full_model.end_fc.parameters(), 
                'lr' : config_dict['end_fc_lr_scaler'] * config_dict['lr_scheduler_config']['init_lr']
            }    
        ]
        return full_model, param_lr_list

    if config_dict['arch_type'] == 'heirarchical':
        # Model creation   
        models_dict = {}
        models_dict['base'] =  model(model_type=config_dict['model_type'])

        # modify base models end classifier if required
        if config_dict['num_base_feats'] > 0:
            if 'efficient' in config_dict['model_type']:
                end_fc = nn.Linear(models_dict['base'].out_features, config_dict['num_base_feats'])
            else: 
                end_fc = nn.Sequential(  
                    nn.Dropout(p=0.5), 
                    nn.Linear(models_dict['base'].out_features, config_dict['num_base_feats'])
                )
            models_dict['base'].add_end_fc(end_fc, num_outputs = config_dict['num_base_feats'])

        models_dict['diags'] =  nn.Sequential(                                          
            nn.Linear(
                models_dict['base'].out_features + len(config_dict['feat_names']), 
                len(config_dict['diag_names'])
            )
        )  
        models_dict['feats'] =  nn.Sequential(  
            nn.Dropout(p=0.5), 
            nn.Linear(models_dict['base'].out_features, len(config_dict['feat_names']))
        )  
      
        # Setting up the optimizers
        param_lr_list = [
            {
                'params' : models_dict['base'].model_net.parameters(),
                'lr' : config_dict['lr_scheduler_config']['base']['init_lr']
            },
            {
                'params' : models_dict['diags'].parameters(), 
                'lr' : config_dict['end_fc_lr_scaler']['diags'] * config_dict['lr_scheduler_config']['diags']['init_lr']
            },
            {
                'params' : models_dict['feats'].parameters(), 
                'lr' : config_dict['end_fc_lr_scaler']['feats'] * config_dict['lr_scheduler_config']['diags']['init_lr']
            },    
        ]

        
        if config_dict['num_base_feats'] > 0:
            param_lr_list.append(
                {
                    'params' : models_dict['base'].end_fc.parameters(), 
                    'lr' : config_dict['end_fc_lr_scaler']['base'] * config_dict['lr_scheduler_config']['base']['init_lr']
                }  
            )

        full_model = HeirarchicalModel(models_dict, output_as_dict=True, device=device)
        return full_model, param_lr_list

    if config_dict['arch_type'] == 'multi-input':
        models_dict = {}
        models_dict['base'] =  model( model_type=config_dict['model_type'])
        # modify base models end classifier if required
        if config_dict['num_base_feats'] > 0:
            if 'efficient' in config_dict['model_type']:
                end_fc = nn.Linear(models_dict['base'].out_features, config_dict['num_base_feats'])
            else: 
                end_fc = nn.Sequential(  
                    nn.Dropout(p=0.5), 
                    nn.Linear(models_dict['base'].out_features, config_dict['num_base_feats'])
                )
            models_dict['base'].add_end_fc(end_fc, num_outputs = config_dict['num_base_feats'])


        models_dict['feats_input'] = SimpleFC(
            in_features=models_dict['base'].out_features, 
            out_features=100
        )

        models_dict['diags'] =  SimpleFC(                                          
                models_dict['base'].out_features + models_dict['feats_input'].out_features, 
                len(config_dict['diag_names'])
        )          


        param_lr_list = [
            {
                'params' : models_dict['base'].model_net.parameters(),
                'lr' : config_dict['lr_scheduler_config']['base']['init_lr']
            },
            {
                'params' : models_dict['feats_input'].parameters(),
                'lr' : config_dict['lr_scheduler_config']['base']['init_lr']
            },
            {
                'params' : models_dict['diags'].parameters(), 
                'lr' : config_dict['end_fc_lr_scaler']['diags'] * config_dict['lr_scheduler_config']['diags']['init_lr']
            },
        ]
    
        if config_dict['num_base_feats'] > 0:
            param_lr_list.append(
                {
                    'params' : models_dict['base'].end_fc.parameters(), 
                    'lr' : config_dict['end_fc_lr_scaler']['base'] * config_dict['lr_scheduler_config']['base']['init_lr']
                }  
            )

        full_model = MultiInputModel(models_dict, output_as_dict=True, device=device)
        return full_model, param_lr_list

    if config_dict['arch_type'] in ['diags-feats', 'diags-feats-pcgrad'] :       
        # Model creation   
        models_dict = {}
        models_dict['base'] =  model(model_type=config_dict['model_type'])

        # modify base models end classifier if required
        if config_dict['num_base_feats'] > 0:
            if 'efficient' in config_dict['model_type']:
                end_fc = nn.Linear(models_dict['base'].out_features, config_dict['num_base_feats'])
            else: 
                end_fc = nn.Sequential(  
                    nn.Dropout(p=0.5), 
                    nn.Linear(models_dict['base'].out_features, config_dict['num_base_feats'])
                )
            models_dict['base'].add_end_fc(end_fc, num_outputs = config_dict['num_base_feats'])

        models_dict['diags'] =  nn.Sequential(                                          
            nn.Linear(models_dict['base'].out_features, len(config_dict['diag_names'])
            )
        )  
        models_dict['feats'] =  nn.Sequential(  
            nn.Dropout(p=0.5), 
            nn.Linear(models_dict['base'].out_features, len(config_dict['feat_names']))
        )  
      
        # Setting up the optimizers
        param_lr_list = [
            {
                'params' : models_dict['base'].model_net.parameters(),
                'lr' : config_dict['lr_scheduler_config']['base']['init_lr']
            },
            {
                'params' : models_dict['diags'].parameters(), 
                'lr' : config_dict['end_fc_lr_scaler']['diags'] * config_dict['lr_scheduler_config']['diags']['init_lr']
            },
            {
                'params' : models_dict['feats'].parameters(), 
                'lr' : config_dict['end_fc_lr_scaler']['feats'] * config_dict['lr_scheduler_config']['diags']['init_lr']
            },    
        ]

        if config_dict['num_base_feats'] > 0:
            param_lr_list.append(
                {
                    'params' : models_dict['base'].end_fc.parameters(), 
                    'lr' : config_dict['end_fc_lr_scaler']['base'] * config_dict['lr_scheduler_config']['base']['init_lr']
                }  
            )

        full_model = MultiOutputModel(models_dict, output_as_dict=True, device=device)
        return full_model, param_lr_list


def loss_setup_helper(config_dict, **kwargs):

    pos_weights = kwargs.pop('pos_weights', None)

    if config_dict['arch_type'] == 'diags-only':
        train_losswrapper_dict = {}
        train_losswrapper_dict.update({
            'loss-diags' : GenericPointWiseLossWrapper(
                nn.CrossEntropyLoss(
                    weight=pos_weights['diags'], reduction='mean'), 
                    model_output_key='diags',
                    batch_target_key='diags'
                )                       
            }
        )  

        return train_losswrapper_dict

    if config_dict['arch_type'] == 'heirarchical':
        train_losswrapper_dict = {}
        pass2_losswrapper_dict = {}
        pass2_losswrapper_dict.update(
            {
                'loss-diags' : GenericPointWiseLossWrapper(
                    nn.CrossEntropyLoss(
                        weight=pos_weights['diags'], reduction='mean'), 
                    model_output_key='diags',
                    batch_target_key='diags'
                )                       
            }
        )   

        pass2_losswrapper_dict.update(
            {
                'loss-feats' : GenericPointWiseLossWrapper(
                    nn.BCEWithLogitsLoss(   
                        reduction='mean',
                        pos_weight=pos_weights['feats']
                    ), 
                    model_output_key='feats',
                    batch_target_key='feats'
                ),
                'loss-feats-l1': L1Regularizer(
                        model_output_key='feats',
                )
            }
        )

        train_losswrapper_dict.update(
            {
                'loss-total-pass2' : MultiLossWrapper(
                    pass2_losswrapper_dict, 
                    loss_weight_dict=config_dict['loss_weight_dict']['loss-total-pass2']
                )
            }
        )

        pass1_losswrapper_dict = {}
        pass1_losswrapper_dict.update(pass2_losswrapper_dict)
        pass1_losswrapper_dict.update(
            {
                'loss-fake-feats' : GenericPointWiseLossWrapper(
                    nn.MSELoss(
                            reduction='mean',
                        ),
                    model_output_key='noise',
                    batch_target_key='feats'
                )
            }
        )
        train_losswrapper_dict.update(
            {
                'loss-total-pass1': MultiLossWrapper(
                    pass1_losswrapper_dict,
                    loss_weight_dict=config_dict['loss_weight_dict']['loss-total-pass1']
                )
            }
        )
        train_losswrapper_dict.update(pass1_losswrapper_dict)

        return train_losswrapper_dict

    if config_dict['arch_type'] == 'multi-input':
        train_losswrapper_dict = {}
        train_losswrapper_dict.update(
            {
                'loss-diags': GenericPointWiseLossWrapper(
                    nn.CrossEntropyLoss(
                        weight=pos_weights['diags'], reduction='mean'),
                    model_output_key='diags',
                    batch_target_key='diags'
                )
            }
        )

        return train_losswrapper_dict

    if config_dict['arch_type'] == 'diags-feats':
        train_losswrapper_dict = {}
        losswrapper_dict = {}
        losswrapper_dict.update(
            {
                'loss-diags': GenericPointWiseLossWrapper(
                    nn.CrossEntropyLoss(
                        weight=pos_weights['diags'], reduction='mean'),
                    model_output_key='diags',
                    batch_target_key='diags'
                )
            }
        )

        losswrapper_dict.update(
            {
                'loss-feats': GenericPointWiseLossWrapper(
                    nn.BCEWithLogitsLoss(
                        reduction='mean',
                        pos_weight=pos_weights['feats']
                    ),
                    model_output_key='feats',
                    batch_target_key='feats'
                )
            }
        )

        losswrapper_dict.update(
            {
                'loss-feats-l1': L1Regularizer(
                    model_output_key='feats',
                )
            }
        )

        train_losswrapper_dict.update(
            {
                'loss-total-pass2' : MultiLossWrapper(
                    losswrapper_dict, 
                    loss_weight_dict=config_dict['loss_weight_dict']
                )
            }
        )
        train_losswrapper_dict.update(losswrapper_dict)

        return train_losswrapper_dict

    if config_dict['arch_type'] == 'diags-feats-pcgrad':
        train_losswrapper_dict = {}
        losswrapper_dict = {}
        losswrapper_dict.update(
            {
                'loss-diags': GenericPointWiseLossWrapper(
                    nn.CrossEntropyLoss(
                        weight=pos_weights['diags'], reduction='mean'),
                    model_output_key='diags',
                    batch_target_key='diags'
                )
            }
        )

        losswrapper_dict.update(
            {
                'loss-feats': GenericPointWiseLossWrapper(
                    nn.BCEWithLogitsLoss(
                        reduction='mean',
                        pos_weight=pos_weights['feats']
                    ),
                    model_output_key='feats',
                    batch_target_key='feats'
                )
            }
        )

#         losswrapper_dict.update(
#             {
#                 'loss-feats-l1': L1Regularizer(
#                     model_output_key='feats',
#                 )
#             }
#         )        

        train_losswrapper_dict.update(
            {
                'loss-total-val' : MultiLossWrapper(
                    losswrapper_dict, 
                    loss_weight_dict=config_dict['loss_weight_dict']
                )
            }
        )

        train_losswrapper_dict.update(
            {
                'loss-total' : MultiLossWrapper(
                    losswrapper_dict, 
                    loss_weight_dict=config_dict['loss_weight_dict'],
                    reduce=None
                )
            }
        )

        train_losswrapper_dict.update(losswrapper_dict)



        return train_losswrapper_dict
    
    
def display_save_model_info(config_dict, full_model, val_dataloader, npt_experiment, device):
    if config_dict['fold_num'] == 0:
        if config_dict['arch_type'] in ['heirarchical', 'diags-only', 'diags-feats', 'diags-feats-pcgrad']:
            # display model params
            dataiter = iter(val_dataloader)
            data_dict = dataiter.next()
            _img = data_dict['image'][0].unsqueeze(0).to(device=device)
            _model = full_model.to(device)
            _model.output_as_dict = False
            model_summary = summary(
                _model, 
                input_data=_img,
                col_names=["kernel_size", "output_size", "num_params", "mult_adds"],
                verbose=0
            )
            # Print Model summary
            print(model_summary)
            npt_experiment.set_property('trainable_params', model_summary.trainable_params)
            npt_experiment.set_property('total_params', model_summary.total_params)
            _model.output_as_dict = True

        elif config_dict['arch_type'] in ['multi-input']:
            # display model params
            dataiter = iter(val_dataloader)
            data_dict = dataiter.next()
            _img = data_dict['image'][0].unsqueeze(0).to(device=device)
            _feats = data_dict['feats'][0].unsqueeze(0).to(device=device)
            _model = full_model.to(device)
            _model.output_as_dict = False
            model_summary = summary(
                _model,
                input_data=[_img, _feats],
                col_names=["kernel_size", "output_size", "num_params", "mult_adds"],
                verbose=0
            )
            # Print Model summary
            print(model_summary)
            npt_experiment.set_property('trainable_params', model_summary.trainable_params)
            npt_experiment.set_property('total_params', model_summary.total_params)
            _model.output_as_dict = True


def train_callbacks_evals_helper(config_dict, train_losswrapper_dict, lr_scheduler_callback, fold_logdir, npt_experiment, device):
    environ_list = config_dict['environ_list']
    evals_dict = {}
    callbacks = {}
    chkpt_filepath = os.path.join(fold_logdir,'sys_fold{}.chkpt'.format(config_dict['fold_num']))

        
    

    if config_dict['arch_type'] in ['heirarchical', 'multi-input', 'diags-only', 'diags-feats', 'diags-feats-pcgrad']:
        # Setting up evaluators
        evals_dict['diags'] = get_diags_evaluators(config_dict, train_losswrapper_dict)            

        # add callbacks
        callbacks['diags'] = get_diags_callbacks(
            config_dict, 
            os.path.join(fold_logdir,'log_fold{}_diags.csv'.format(config_dict['fold_num'])), 
            npt_experiment
        )
        callbacks['diags'].insert(
            0, 
            SystemCheckpointResetCallback(
                tmp_state_filepath=chkpt_filepath,
                try_reset=config_dict['try_reset']
            )
        )
        callbacks['standard'] = [
            SystemCheckpointCallback(
                patience=config_dict['epoch_save_patience'], 
                tmp_state_filepath=chkpt_filepath,     
            ),
            ClearCacheCallback(device=device),
            NumberOfEpochsStoppingCriterionCallback(config_dict['n_epochs']),
            EarlyStoppingCriterionCallback(
                patience=config_dict['early_stopping_patience'],
                evaluation_data_loader_key='val',
                evaluator_key=config_dict['early_stopping_on'],
                tmp_best_state_filepath= os.path.join(environ_list['save_weights_to_path'],"model_chkpnt.pt")
            ),            
        ] 
        
        callbacks['standard'] += lr_scheduler_callback

    if config_dict['arch_type'] in ['heirarchical', 'diags-feats', 'diags-feats-pcgrad']:
        evals_dict['feats'] = get_feats_evaluators(config_dict, train_losswrapper_dict)
        callbacks['feats'] = get_feats_callbacks(
            config_dict, 
            os.path.join(fold_logdir,'log_fold{}_feats.csv'.format(config_dict['fold_num'])), 
            environ_list['tensorboard_log_path'],
            npt_experiment
        )
    
    return evals_dict, callbacks

def test_callbacks_evals_helper(config_dict, fold_logdir, test_dls, npt_experiment):
    test_evals = {}
    test_callbacks = {}    

    if config_dict['arch_type'] in ['heirarchical', 'multi-input', 'diags-only', 'diags-feats', 'diags-feats-pcgrad']:
        diag_dl_keys = [key for key, dl in test_dls.items() if dl.dataset.diags]
        test_evals['diags'] = get_diags_test_evals(config_dict)
        test_callbacks['diags'] = get_diags_test_callbacks(config_dict, fold_logdir, diag_dl_keys, npt_experiment)

    if config_dict['arch_type'] in ['heirarchical', 'diags-feats', 'diags-feats-pcgrad']:
        feat_dl_keys = [key for key, dl in test_dls.items() if dl.dataset.feats]
        test_evals['feats'] = get_feats_test_evals(config_dict)
        test_callbacks['feats'] = get_feats_test_callbacks(config_dict, fold_logdir, feat_dl_keys, npt_experiment)
    
    return test_evals, test_callbacks

def system_run_helper(
    config_dict, 
    full_model,
    train_losswrapper_dict, 
    train_dataloader, 
    test_dls,
    optimizer_full_model, 
    evals_dict,
    callbacks, 
    npt_experiment,
    fold_num, 
    last_activation, 
    device 
):

    if config_dict['arch_type'] == 'heirarchical':
        full_system = HeirarchicalSystem(
            full_model, 
            evaluate_on_subsystem='standard',
            last_activation=last_activation, 
            device=device,
        )
        
        full_system.train(
            loss_dict=train_losswrapper_dict,
            train_data_loader=train_dataloader,
            evaluation_data_loaders=test_dls,
            optimizer=optimizer_full_model,
            batch_input_key='image',
            evaluators_dict=dict_merge(evals_dict['diags'], evals_dict['feats']),
            callbacks=callbacks['diags']+callbacks['feats']+callbacks['standard'],
            npt_expt=npt_experiment,
            fold_num=fold_num,
            verbose=True,
            eval_verbose=False,
            trainer_type=config_dict['trainer_type']    
        )
        

    elif config_dict['arch_type'] == 'diags-only':
        full_system = System(full_model, last_activation=last_activation, device=device)
        _ = full_system.train(
            loss_wrapper=train_losswrapper_dict['loss-diags'],
            optimizer=optimizer_full_model,
            train_data_loader=train_dataloader,
            evaluators=evals_dict['diags'],
            evaluation_data_loaders=test_dls,
            batch_input_key='image',
            callbacks=callbacks['diags'] + callbacks['standard'],
            verbose=True,
            eval_verbose=False
        )
    elif config_dict['arch_type'] == 'diags-feats-pcgrad':
        full_system = MultiTaskSystem(
            full_model, 
            last_activation=last_activation, 
            device=device,
        )
        
        full_system.train(
            loss_dict=train_losswrapper_dict,
            train_data_loader=train_dataloader,
            evaluation_data_loaders=test_dls,
            optimizer=optimizer_full_model,
            batch_input_key='image',
            evaluators_dict=dict_merge(evals_dict['diags'], evals_dict['feats']),
            callbacks=callbacks['diags']+callbacks['feats']+callbacks['standard'],
            npt_expt=npt_experiment,
            fold_num=fold_num,
            verbose=True,
            eval_verbose=False,
        )
        
        
    elif config_dict['arch_type'] == 'diags-feats':
        full_system = System(full_model, last_activation=last_activation, device=device)
        _ = full_system.train(
            loss_wrapper=train_losswrapper_dict['loss-total-pass2'],
            optimizer=optimizer_full_model,
            train_data_loader=train_dataloader,
            evaluators=dict_merge(evals_dict['diags'], evals_dict['feats']),
            evaluation_data_loaders=test_dls,
            batch_input_key='image',
            callbacks=callbacks['diags']+callbacks['feats']+callbacks['standard'],
            verbose=True,
            eval_verbose=False
        )

    elif config_dict['arch_type'] == 'multi-input':
        full_system = MultiInputSystem(
            model=full_model,
            loss_dict=train_losswrapper_dict,
            train_data_loader=train_dataloader,
            evaluation_data_loaders=test_dls,
            optimizer=optimizer_full_model,
            batch_input_keys=['image','feats'],
            evaluators_dict=evals_dict,
            callbacks_dict=callbacks,
            npt_expt=npt_experiment,
            fold_num=fold_num,
            last_activation=last_activation, 
            device=device    
        )
        full_system.train()
    else:
        print("......No system was run.....")
        full_system = None
    return full_system

def system_test_helper(
    config_dict, 
    test_dls,
    full_system,
    test_evals,
    test_callbacks,
    
):

    if config_dict['arch_type'] == 'heirarchical':
        return full_system.test(
            evaluators=dict_merge(test_evals['diags'],test_evals['feats']),
            evaluation_data_loaders=test_dls,
            batch_input_key='image',
            callbacks=test_callbacks['diags']+test_callbacks['feats'],
            verbose=True,
        )

    elif config_dict['arch_type'] == 'diags-only':
        
        test_evals = test_evals['diags']
        test_callbacks = test_callbacks['diags']
        return full_system.test(
            evaluators=test_evals,
            evaluation_data_loaders=test_dls,
            batch_input_key='image',
            callbacks=test_callbacks,
            verbose=True,
            eval_verbose=False
        )

    elif config_dict['arch_type'] in ['diags-feats', 'diags-feats-pcgrad']:
        
        return full_system.test(
            evaluators=dict_merge(test_evals['diags'],test_evals['feats']),
            evaluation_data_loaders=test_dls,
            batch_input_key='image',
            callbacks=test_callbacks['diags'] + test_callbacks['feats'],
            verbose=True,
        )

    elif config_dict['arch_type'] == 'multi-input':
        return full_system.test(
            evaluators=test_evals['diags'],
            evaluation_data_loaders=test_dls,
            batch_input_keys=['image', 'feats'],
            callbacks=test_callbacks['diags'],
            verbose=True,
        )
    else:
        print("......No system was tested.....")
        return None
