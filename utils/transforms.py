#%%
from albumentations import (CLAHE, Blur, CenterCrop, Compose, Flip, GaussNoise,
                            GridDistortion, HorizontalFlip, HueSaturationValue,
                            IAAAdditiveGaussianNoise, IAAEmboss,
                            IAAPerspective, IAAPiecewiseAffine, IAASharpen,
                            MedianBlur, MotionBlur, Normalize, OneOf,
                            OpticalDistortion, RandomBrightnessContrast,
                            RandomCrop, RandomRotate90, Resize,PadIfNeeded,
                            ShiftScaleRotate, Transpose)
# from albumentations.pytorch import ToTensor
from albumentations.pytorch import ToTensorV2 as ToTensor


(imagenet_mean, imagenet_std) = ([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]) 
def get_train_aug_list():
    return [
        Resize(556, 556), 
        RandomCrop(512, 512),
        HorizontalFlip(),
        IAAPerspective(p=0.25),
        IAAAdditiveGaussianNoise(p=0.25),
        OneOf([
            MotionBlur(p=.1),
            MedianBlur(blur_limit=3, p=0.05),
            Blur(blur_limit=3, p=0.05),
        ], p=0.5),
        OneOf([
            OpticalDistortion(p=0.3),
            GridDistortion(p=.1),
            IAAPiecewiseAffine(p=0.3),
        ], p=0.2),
        OneOf([
            CLAHE(clip_limit=2),
            IAASharpen(p=0.5),
            IAAEmboss(p=0.5),
            RandomBrightnessContrast(),            
        ], p=0.25),
        HueSaturationValue(p=0.3),
        Normalize(
            mean=imagenet_mean,
            std=imagenet_std,
        )
    ]

def get_val_aug_list():
    return [
        Resize(556, 556), 
        CenterCrop(512, 512),
        Normalize(
            mean=imagenet_mean,
            std=imagenet_std,
        )
    ]   

def albu_aug_train(img_pop_mean, img_pop_std, **kwargs):
    tr_Resize = kwargs.pop('tr_Resize', None)
    tr_RandomCrop = kwargs.pop('tr_RandomCrop', None)
    augmentation_type = kwargs.pop('augmentation_type', 'simple')

    if augmentation_type == 'v1':
        return Compose([
            Resize(tr_Resize, tr_Resize), 
            RandomCrop(tr_RandomCrop, tr_RandomCrop),
            HorizontalFlip(),
            IAAPerspective(p=0.25),
            IAAAdditiveGaussianNoise(p=0.25),
            OneOf([
                MotionBlur(p=.1),
                MedianBlur(blur_limit=3, p=0.05),
                Blur(blur_limit=3, p=0.05),
            ], p=0.5),
            OneOf([
                OpticalDistortion(p=0.3),
                GridDistortion(p=.1),
                IAAPiecewiseAffine(p=0.3),
            ], p=0.2),
            OneOf([
                CLAHE(clip_limit=2),
                IAASharpen(p=0.5),
                IAAEmboss(p=0.5),
                RandomBrightnessContrast(),            
            ], p=0.25),
            HueSaturationValue(p=0.3),
            Normalize(
                mean=img_pop_mean,
                std=img_pop_std,
            )
        ])
    elif augmentation_type == 'full_image_with_padding':
        
        return Compose([
            PadIfNeeded(1024, 1024), 
            HorizontalFlip(),
            IAAPerspective(p=0.25),
            IAAAdditiveGaussianNoise(p=0.25),
            OneOf([
                MotionBlur(p=.1),
                MedianBlur(blur_limit=3, p=0.05),
                Blur(blur_limit=3, p=0.05),
            ], p=0.5),
            OneOf([
                OpticalDistortion(p=0.3),
                GridDistortion(p=.1),
                IAAPiecewiseAffine(p=0.3),
            ], p=0.2),
            OneOf([
                CLAHE(clip_limit=2),
                IAASharpen(p=0.5),
                IAAEmboss(p=0.5),
                RandomBrightnessContrast(),            
            ], p=0.25),
            HueSaturationValue(p=0.3),
            Normalize(
                mean=img_pop_mean,
                std=img_pop_std,
            )
        ])
    else:
        return Compose([
            Resize(tr_Resize, tr_Resize), 
            RandomCrop(tr_RandomCrop, tr_RandomCrop),
            Flip(p=0.5),
            RandomRotate90(p=0.5),
            IAAPerspective(p=0.25),
            IAAAdditiveGaussianNoise(p=0.25),
            HueSaturationValue(p=0.3),
            Normalize(
                mean=img_pop_mean,
                std=img_pop_std,
            )
        ])




def albu_aug_val(img_pop_mean, img_pop_std, **kwargs):

    val_Resize = kwargs.pop('val_Resize', None)
    val_CenterCrop = kwargs.pop('val_CenterCrop', None)
    augmentation_type = kwargs.pop('augmentation_type', None)

    if augmentation_type == 'full_image_with_padding':
        return Compose([
            PadIfNeeded(1024,1024),        
            Normalize(
                mean=img_pop_mean,
                std=img_pop_std,
            )
        ])
    else:
        return Compose([
            Resize(val_Resize, val_Resize), 
            CenterCrop(val_CenterCrop, val_CenterCrop),        
            Normalize(
                mean=img_pop_mean,
                std=img_pop_std,
            )
        ])

    

def albu_toTensor():

    return Compose([ ToTensor() ])


# %%
