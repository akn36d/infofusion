
from pprint import pprint

import numpy as np
import pandas as pd


def my_csv2df(csv_file, class_names, data_dir):
    data_csv = csv_file
    data_df_header = pd.read_csv(data_csv, index_col=False)

    # removing classes we dont want (as per class names list above : so the "UNK")
    data_df_cols = list(data_df_header.columns)
    print('Columns in data df : {}'.format(data_df_cols))
    print('\nPeek data df : ')
    pprint(data_df_header.head())

    data_df = data_df_header.where((pd.notnull(data_df_header)), None)
            
    data_df.iloc[:,0] = data_df.iloc[:, 0].apply(lambda x : data_dir + '/' + str(x) + '.jpg') # appending image location to get full path to the data
                                                                            # for each image         
    
    print('\nPeek data df with filename : ')
    pprint(data_df.head())

    class_onehot = (data_df.iloc[:,1:1+len(class_names)].to_numpy()).astype(np.float).astype(np.uint16) # first float and then int is done for proper conversion of 
                                                                                                    # strings of type '0.0' to integer of form 0
    
    pprint('Size of onehots : {}'.format(class_onehot.shape))                                                                                                # '0.0' -> 0.0 -> 0
    return data_df, class_onehot




def calc_class_weights(class_onehot, class_names, verbose=True):
    # print('Converted class_ids : {}'.format(class_ids[100:105]))
    class_nums = []
    if verbose:
        print('Number of images in each class : ')
    for id_num, id_name in enumerate(class_names):
        class_nums.append(np.sum(class_onehot[:,id_num], dtype=np.uint16))
        if verbose:
            print('\t{} : {}'.format(id_name, class_nums[id_num]))

    class_out_weights = np.zeros((len(class_names)))    
    for id_, _ in enumerate(class_nums):
        class_nums_temp = class_nums.copy()
        class_num_val = class_nums_temp.pop(id_)

        if (class_num_val < 1):
            class_out_weights[id_]=0
        else:
            class_out_weights[id_]= float(class_onehot.shape[0])/class_num_val
    
    if verbose:    
        print('Class weights  : {}'.format(class_out_weights))

    return class_nums, class_out_weights
