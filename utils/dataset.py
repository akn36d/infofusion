import os
import pickle
from copy import deepcopy
import sys

import cv2
import numpy as np
import pandas as pd
import torch
import torch.utils.data
import torchvision
from torch.utils.data import DataLoader, Dataset
from utils.transforms import albu_toTensor


class ISIC_dataset(Dataset):
    """ISIC dataset class written to comply with my imgaug library usage"""

    def __init__(
                    self, 
                    File_list_csv, 
                    diag_list, 
                    feat_list, 
                    phase='train',
                    transform=None, 
                    no_feats_class=False, 
                    img_dir=None
                ):
        r"""
        Args:
            File_list_csv: A csv file with the following columns
                full\file\location\name.jpg,0,1,0,1,0
            diag_list: list of strings which has the diagnosis class names in it (or None)
            feat_list: list of strings with the feature class names in it (or None)
            phase    : the phase the dataset is used for can be 'train', 'eval' or 'test'
            transform (callable, optional): Optional transform to be applied
                on a sample
            no_feats_class: if True then a 'no_feats' class will be appended to the 
                feature labels and will be set to 1 for each image where no other feature
                labels are set to 1
            save_gen_imgs: if True given will save all the images generated 
                for each sample in the given 'log_path'. 
            save_gen_imgs_list: if True will save all the image ids and labels
                and save them in 'gen_imgs.csv' at the given path 'log_path'
            log_path: (accepts string or path) Path to directory to store generated images and/or the 
                annotated list of generated images
            img_dir: path where images are stored

        Returns:
            A dictionary which has the following keys
                'image_id' : (phase='test' or 'eval' only) the ISIC id of the image
                'image'    : the image with transforms(if specified) applied 
                'feats'    : (available if feat_list given and not None) the feature presence labels 
                                e.g. [[1,1,0,1],[1,0,1,0],[0,0,0,1]]
                'diags'    : (available if diag_list given and not None) the diagnosis labels 
                                e.g. [0,3,3,1,4]
                'diags-onehot': (available if diag_list given and not None) the one-hot diagnosis labels 
                                e.g.[[1,0,0,0,0],[0,0,1,0,0]]
        """
        self.File_list_csv = File_list_csv
        if diag_list:
            self.diags = diag_list.copy()
        else:
            self.diags = None

        if feat_list:
            self.feats = feat_list.copy()
        else:
            self.feats = None
        self.no_feats = no_feats_class
        self.data_df = self.get_data_df()

        # 'no_feats' class added only if feat_list is given
        if self.no_feats and 'no_feats' not in self.feats:
            if 'no_feats' not in self.data_df.columns.tolist():
                self.add_nofeat_class_to_feats()
            self.use_nofeat_class()
        self.class_nums = None
        self.transform = transform
        self.phase = phase

        if img_dir:
            self.append_img_dir_to_df(img_dir)


    def __len__(self):
        return len(self.data_df)

    
    def __getitem__(self, idx):    
        img_name = self.data_df.loc[idx, 'image']
        
        _, image_name_only = os.path.split(img_name)
        image_id, _ = os.path.splitext(image_name_only)
        image = cv2.imread(img_name) # This gives HxWxC shape 
        try:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        except:
            print("Unexpected error:", sys.exc_info()[0])
            print("Image : {}".format(img_name))

        sample = {}
        sample['image'] = image

        if self.feats:
            feat_labels = self.data_df.loc[idx, self.feats].tolist()
            feat_labels = np.array(feat_labels).astype(np.float32)
            sample['feats'] = torch.from_numpy(feat_labels).float() # float type for BCE/BCE with logits losses

        if self.diags:
            diag_labels = self.data_df.loc[idx, self.diags].tolist()
            diag_labels = np.array(diag_labels).astype(np.float32)
            sample['diags-onehot'] = torch.from_numpy(diag_labels).long() # long type for CE loss
            sample['diags'] = torch.from_numpy(diag_labels).long().argmax()         

        if self.transform:
            # Image to be transformed is a numpy array for albumentations 
            augmented = self.transform(image=image)
            sample['image'] = augmented['image']

        # Conversion of image to Tensor
        toTensor = albu_toTensor()
        img_tensor = toTensor(image=sample['image'])
        sample['image'] = img_tensor['image']   
        sample['image_id'] = image_id

        return sample


    def add_nofeat_class_to_feats(self):
        """
        Args:
            df: dataframe with feature class names as column names in its header

        Returns:
            df: dataframe with the 'no_feats' class attached 

        Description:
            This attaches an 'no_feats' class to the feature labels.
            If no features are labelled as present for an image then 
            the 'no_feats' label is set to a 1.
        """
        if self.feats:
            feat_df = self.data_df.loc[:, self.feats]
            condition = feat_df.any(axis='columns')
            self.data_df['no_feats'] = (~condition).astype(np.int16)

    def use_nofeat_class(self):
        self.feats += ['no_feats']

    def get_data_df(self):
        data_df = pd.read_csv(self.File_list_csv,  skipinitialspace=True)
        data_df = data_df.where((pd.notnull(data_df)), 0)

        columns = data_df.columns.values
        _class_list = []

        if self.diags:
            _class_list += self.diags

        if self.feats:
            _class_list += self.feats

        # check to catch if clases given are different from the dataframe headers
        check_classes = []
        for _c in _class_list:
            if _c in columns:
                check_classes += [1]
            else:
                print("'{}' not found in df columns {}".format(_c,columns))
                check_classes += [0]

        if not all(check_classes):
            raise ValueError('columns and class list dont match')

        data_df.loc[:,_class_list] = data_df.loc[:,_class_list].astype(np.int16) 
        # data_df = data_df.loc[0:128,:] # uncomment to speed up testing
        return data_df

    def save_img(self, image, save_fname, image_id):
        savefpath = os.path.join(save_fname,image_id + '.jpg')
        image_sv = image.copy()
        cv2.normalize(image_sv,  image_sv, 0, 255, cv2.NORM_MINMAX)
        image_sv = cv2.cvtColor(image_sv, cv2.COLOR_RGB2BGR)
        cv2.imwrite(savefpath, image_sv)

    def append_img_dir_to_df(self, img_dir):
        # appending image data location to get full path to the images
        self.data_df.loc[:, 'image'] = self.data_df.loc[:, 'image'].apply(lambda x: img_dir + '/' + str(x) + '.jpg') 

    def get_class_nums(self):
        _extract_list = []
        if self.diags:
            _extract_list += self.diags
        if self.feats:
            _extract_list += self.feats

        class_nums = self.data_df.loc[:, _extract_list].sum(numeric_only=True, min_count=0)
        self.class_nums = {} 
        if self.diags:
            self.class_nums['diags'] = class_nums[self.diags]
        if self.feats:
            self.class_nums['feats'] = class_nums[self.feats]

    def get_pos_weights(self):
        self.get_class_nums()

        pos_weights = {}
        if self.diags:
            pos_weights['diags'] = (len(self.data_df)/self.class_nums['diags']) - 1
        if self.feats:
            pos_weights['feats'] = (len(self.data_df)/self.class_nums['feats']) - 1

        return pos_weights

    def save_dataset_mean_std(self, stats_savename, transform):
        """
        Args:
            stats_savename: Path to save the stats so that if same savename is 
                used we can reload the ones previously calculated and reduce time 
                consumed.

            transform : a list of transforms to use with the current dataset 
                before finding the dataset population mean and std. If not given it
                will use the transforms initialized in the dataset


        """
        self.stats_savename = stats_savename
        try:
            f = open(stats_savename, 'rb')
            pop_mean, pop_std0, pop_std1 = pickle.load(f)
            f.close()
        except(FileNotFoundError):
            (pop_mean, pop_std0, pop_std1) = self.find_pop_mean_std(transform)
            with open(stats_savename, 'wb') as f:
                pickle.dump((pop_mean, pop_std0, pop_std1), f)
        return pop_mean, pop_std0, pop_std1

    def find_pop_mean_std(self, transform=None):
        """
        Args:
            transform : a list of transforms to use with the current dataset 
                before finding the dataset population mean and std. If not given it
                will use the transforms initialized in the dataset

        """
        # If dataset can fit in Memory we can load all the images in a 
        # single batch and calculate the mean and std else split
        # them up into smaller batches

        if transform:
            transform = transform
        else:
            transform = self.transform

        _dataset = deepcopy(self)
        _dataset.transform = transform
        _dataloader = DataLoader(
            _dataset,
            batch_size=len(_dataset),
            shuffle=False,
            num_workers=0
        )

        print('Number of images in training population : {}'.format(len(_dataset)))

        # Init the population mean and stds
        pop_mean = []
        pop_std0 = []
        pop_std1 = []
        for _, data in enumerate(_dataloader):
            print('Processing ....')
            # shape (batch_size, 3, height, width)
            numpy_image = data['image'].numpy()
            print('\tShape of train set : {}'.format(numpy_image.shape))
            # shape (3,)
            batch_mean = np.mean(numpy_image, axis=(0, 2, 3))  # mean along channel and HxW
            batch_std0 = np.std(numpy_image, axis=(0, 2, 3))  # std along channel and HxW

            # The following std calc gives a better estimate of the std as the
            # one above might be an underestimation
            # Ref : https://stackoverflow.com/questions/27600207/why-does-numpy-std-give-a-different-result-to-matlab-std
            batch_std1 = np.std(numpy_image, axis=(0, 2, 3), ddof=1)
            pop_mean.append(batch_mean)
            pop_std0.append(batch_std0)
            pop_std1.append(batch_std1)
            print('Processing Finished ....')

        # shape (num_iterations, 3) -> (mean across 0th axis) -> shape (3,)
        pop_mean = np.array(pop_mean).mean(axis=0)
        pop_std0 = np.array(pop_std0).mean(axis=0)
        pop_std1 = np.array(pop_std1).mean(axis=0)
        return (pop_mean, pop_std0, pop_std1)

def get_label_from_ISIC_Dataset(dataset, idx):
    diag_label = dataset.data_df.loc[idx, dataset.diags].tolist()
    diag_label = np.array(diag_label).astype(np.uint8)
    label_idx = np.argmax(diag_label)
    return label_idx


# REF: https://github.com/ufoym/imbalanced-dataset-sampler/blob/master/sampler.py
class ImbalancedDatasetSampler(torch.utils.data.sampler.Sampler):
    """Samples elements randomly from a given list of indices for imbalanced dataset
    Arguments:
        indices (list, optional): a list of indices
        num_samples (int, optional): number of samples to draw
        callback_get_label func: a callback-like function which takes two arguments - dataset and index
    """

    def __init__(self, dataset, indices=None, num_samples=None, callback_get_label=None):

        # if indices is not provided, 
        # all elements in the dataset will be considered
        self.indices = list(range(len(dataset))) \
            if indices is None else indices

        # define custom callback
        self.callback_get_label = callback_get_label

        # if num_samples is not provided, 
        # draw `len(indices)` samples in each iteration
        self.num_samples = len(self.indices) \
            if num_samples is None else num_samples

        # distribution of classes in the dataset 
        label_to_count = {}
        for idx in self.indices:
            label = self._get_label(dataset, idx)
            if label in label_to_count:
                label_to_count[label] += 1
            else:
                label_to_count[label] = 1

        # weight for each sample
        weights = [1.0 / label_to_count[self._get_label(dataset, idx)]
                   for idx in self.indices]
        self.weights = torch.DoubleTensor(weights)

    def _get_label(self, dataset, idx):
        if self.callback_get_label:
            return self.callback_get_label(dataset, idx)
        elif isinstance(dataset, torchvision.datasets.MNIST):
            return dataset.train_labels[idx].item()
        elif isinstance(dataset, torchvision.datasets.ImageFolder):
            return dataset.imgs[idx][1]
        elif isinstance(dataset, torch.utils.data.Subset):
            return dataset.dataset.imgs[idx][1]
        else:
            raise NotImplementedError
                
    def __iter__(self):
        return (self.indices[i] for i in torch.multinomial(
            self.weights, self.num_samples, replacement=True))

    def __len__(self):
        return self.num_samples


