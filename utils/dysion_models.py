# Ref : https://github.com/ngessert/isic2019/blob/master/models.py

import functools
from collections import OrderedDict

import pretrainedmodels
import torch
from efficientnet_pytorch import EfficientNet
from torchvision import models


def Dense121():
    return models.densenet121(pretrained=True)

def Dense161():
    return models.densenet161(pretrained=True)
    
def Dense169():
    return models.densenet169(pretrained=True)

def Dense201():
    return models.densenet201(pretrained=True)

def Resnet50():
    return pretrainedmodels.__dict__['resnet50'](num_classes=1000, pretrained='imagenet')

def Resnet101():
    return pretrainedmodels.__dict__['resnet101'](num_classes=1000, pretrained='imagenet')

def InceptionV3():
    return models.inception_v3(pretrained=True)

def se_resnext50():
    return pretrainedmodels.__dict__['se_resnext50_32x4d'](num_classes=1000, pretrained='imagenet')

def se_resnext101():
    return pretrainedmodels.__dict__['se_resnext101_32x4d'](num_classes=1000, pretrained='imagenet')

def se_resnet50():
    return pretrainedmodels.__dict__['se_resnet50'](num_classes=1000, pretrained='imagenet')

def se_resnet101():
    return pretrainedmodels.__dict__['se_resnet101'](num_classes=1000, pretrained='imagenet')

def se_resnet152():
    return pretrainedmodels.__dict__['se_resnet152'](num_classes=1000, pretrained='imagenet')

def resnext101():
    return pretrainedmodels.__dict__['resnext101_32x4d'](num_classes=1000, pretrained='imagenet')

def resnext101_64():
    return pretrainedmodels.__dict__['resnext101_64x4d'](num_classes=1000, pretrained='imagenet')

def senet154():
    return pretrainedmodels.__dict__['senet154'](num_classes=1000, pretrained='imagenet')

def polynet():
    return pretrainedmodels.__dict__['polynet'](num_classes=1000, pretrained='imagenet')

def dpn92():
    return pretrainedmodels.__dict__['dpn92'](num_classes=1000, pretrained='imagenet+5k')

def dpn68b():
    return pretrainedmodels.__dict__['dpn68b'](num_classes=1000, pretrained='imagenet+5k')

def nasnetamobile():
    return pretrainedmodels.__dict__['nasnetamobile'](num_classes=1000, pretrained='imagenet')

def resnext101_32_8_wsl():
    return torch.hub.load('facebookresearch/WSL-Images', 'resnext101_32x8d_wsl')

def resnext101_32_16_wsl():
    return torch.hub.load('facebookresearch/WSL-Images', 'resnext101_32x16d_wsl')

def resnext101_32_32_wsl():
    return torch.hub.load('facebookresearch/WSL-Images', 'resnext101_32x32d_wsl')

def resnext101_32_48_wsl():
    return torch.hub.load('facebookresearch/WSL-Images', 'resnext101_32x48d_wsl')

def efficientnet_b0():
    return EfficientNet.from_pretrained('efficientnet-b0')

def efficientnet_b1():
    return EfficientNet.from_pretrained('efficientnet-b1')

def efficientnet_b2():
    return EfficientNet.from_pretrained('efficientnet-b2')

def efficientnet_b3():
    return EfficientNet.from_pretrained('efficientnet-b3')

def efficientnet_b4():
    return EfficientNet.from_pretrained('efficientnet-b4')

def efficientnet_b5():
    return EfficientNet.from_pretrained('efficientnet-b5')       

def efficientnet_b6():
    return EfficientNet.from_pretrained('efficientnet-b6')   

def efficientnet_b7():
    return EfficientNet.from_pretrained('efficientnet-b7')  

                                                                                                                

model_map = OrderedDict([
                        ('Dense121',  Dense121),
                        ('Dense169' , Dense169),
                        ('Dense161' , Dense161),
                        ('Dense201' , Dense201),
                        ('Resnet50' , Resnet50),
                        ('Resnet101' , Resnet101),   
                        ('InceptionV3', InceptionV3),# models.inception_v3(pretrained=True),
                        ('se_resnext50', se_resnext50),
                        ('se_resnext101', se_resnext101),
                        ('se_resnet50', se_resnet50),
                        ('se_resnet101', se_resnet101),
                        ('se_resnet152', se_resnet152),
                        ('resnext101', resnext101),
                        ('resnext101_64', resnext101_64),
                        ('senet154', senet154),
                        ('polynet', polynet),
                        ('dpn92', dpn92),
                        ('dpn68b', dpn68b),
                        ('nasnetamobile', nasnetamobile),
                        ('resnext101_32_8_wsl', resnext101_32_8_wsl),
                        ('resnext101_32_16_wsl', resnext101_32_16_wsl),
                        ('resnext101_32_32_wsl', resnext101_32_32_wsl),
                        ('resnext101_32_48_wsl', resnext101_32_48_wsl),
                        ('efficientnet-b0', efficientnet_b0), 
                        ('efficientnet-b1', efficientnet_b1), 
                        ('efficientnet-b2', efficientnet_b2), 
                        ('efficientnet-b3', efficientnet_b3),  
                        ('efficientnet-b4', efficientnet_b4), 
                        ('efficientnet-b5', efficientnet_b5),  
                        ('efficientnet-b6', efficientnet_b6), 
                        ('efficientnet-b7', efficientnet_b7),
                    ])


def getModel(model_type):
  """Returns a function for a model
  Args:
    model_type: string with model name from model_map dict above
  Returns:
    model: A class that builds the desired model
  Raises:
    ValueError: If model name is not recognized.
  """
  if model_type in model_map:
    func = model_map[model_type]
  else:
      raise ValueError('Name of model unknown %s' % model_type)
  return func()
