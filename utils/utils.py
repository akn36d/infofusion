from __future__ import division, print_function

import datetime
import os
import sys


def makefolder_ifnotexists(foldername):
    if not os.path.exists(foldername):
        os.makedirs(foldername)

def get_timed_name(prefix):
    now = datetime.datetime.now()
    return prefix + str(now)[:19]


def dict_merge(dict1, dict2):
    res = {**dict1, **dict2}
    return res

def get_timed_name_v2(prefix):
    now = datetime.datetime.now()
    return '{}_{}_{}_{}_{}_{}_{}'.format(prefix, 
                                         str(now.year), 
                                         str(now.month), 
                                         str(now.day), 
                                         str(now.hour), 
                                         str(now.minute), 
                                         str(now.second))

class Environ():
    def __init__(self, run_prefix='test_'):
        self.models_folder = 'Models' 
        makefolder_ifnotexists(self.models_folder)

        # folder name for storing info about all models of the same model type name
        self.suite_type = None
        self.model_name_list = None
        self.suite_dirname = None

        self.run_prefix = run_prefix
        self.run_name = None
        self.run_dirname = None

        self.log_path = None
        self.save_weights_to_path = None
        self.tensorboard_log_path = None
        self.config_path = None

    def create_suite_folders(self, suite_type):
        # Suite Name and directory declaration (The place where all the info for a given run 
        # of the same task_type type will be stored)  

        self.suite_type = suite_type

        # Specify Model name to save model run information in Model_runs folder        
        self.suite_dirname = os.path.join(self.models_folder, self.suite_type)
        makefolder_ifnotexists(self.suite_dirname)


    def create_run_folders(self, run_prefix=None):
        if run_prefix is not None:
            self.run_prefix = run_prefix
        self.run_name = get_timed_name(self.run_prefix)
        self.run_dirname = os.path.join(self.suite_dirname, self.run_name)

        # settings/config folder for a run of model
        self.config_path = os.path.join(self.run_dirname, 'configs')
        makefolder_ifnotexists(self.config_path)

        # log folder for run of models
        self.log_path = os.path.join(self.run_dirname,'logs',)
        makefolder_ifnotexists(self.log_path)

        # model folder for saving weights in a run of model
        self.save_weights_to_path = os.path.join(self.run_dirname, 'weights')
        makefolder_ifnotexists(self.save_weights_to_path)

        self.tensorboard_log_path = os.path.join(self.run_dirname, 'tensorboard_logs')
        makefolder_ifnotexists(self.save_weights_to_path)


            


def recursive_glob(root_dir, file_template="*.tif"):
    """Traverse directory recursively. Starting with Python version 3.5, the glob module supports the "**" directive"""

    if sys.version_info[0] * 10 + sys.version_info[1] < 35:
        import fnmatch
        import os
        matches = []
        for root, dirnames, filenames in os.walk(root_dir):
            for filename in fnmatch.filter(filenames, file_template):
                matches.append(os.path.join(root, filename))
        return matches
    else:
        import glob
        file_list = glob.glob(root_dir + "/**/" + file_template, recursive=True)
        # print(file_list)
        return file_list

def get_folder_above(fpath):
    path_part1, _ = os.path.split(fpath)
    _, folder_above = os.path.split(path_part1)
    return folder_above


# def layergroups(model, is_trainable=False):
#     pytorch_total_params = sum(p.numel() for p in model.parameters())
#     if is_trainable:
#         pytorch_total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
        
#     return pytorch_total_params

def remove_logs(experiment, prefix=''):
    remove_list = []
    log_list = experiment.get_logs().keys()

    for log_ in log_list:
        if prefix in log_:
            experiment.reset_log(log_)
            remove_list.append(log_)
    print('The list of removed tags are : {}'.format(remove_list))
