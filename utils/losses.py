from pytorch_wrapper.loss_wrappers import AbstractLossWrapper
import torch


class SumLossWrapper(AbstractLossWrapper):
    def __init__(self, loss1, loss2, model_output_key=None, batch_target_key='target'):
        super(SumLossWrapper, self).__init__()
        self._loss1 = loss1
        self._loss2 = loss2
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key

    def calculate_loss(self, output, batch, training_context, last_activation=None):
        if self._model_output_key is not None:
            output = output[self._model_output_key]

        batch_targets = batch[self._batch_target_key].to(output.device)

        return self._loss1(output, batch_targets) + self._loss2(output, batch_targets)


class MultiLossWrapper(AbstractLossWrapper):
    """
    Adapter that wraps multiple pointwise losses.
    Overwrite `calculate_loss` to change the way the losses are combined
    """

    def __init__(self, losswrapper_dict, loss_weight_dict, reduce='sum'):
        """
        :param losswrapper_dict: dictionary of loss wrappers of class
            `GenericPointWiseLossWrapper`.
        """
        super(MultiLossWrapper, self).__init__()
        self._losswrapper_dict = losswrapper_dict
        self._loss_weight_dict = loss_weight_dict
        self._reduce = reduce

    def calculate_loss(self, output, batch, training_context, last_activation=None):

        if self._reduce == 'sum':
            loss = 0
            for _key, _loss_wrapper in self._losswrapper_dict.items():
                loss += self._loss_weight_dict[_key] * _loss_wrapper.calculate_loss(output, batch, training_context, last_activation=last_activation)
            return loss
        elif self._reduce is None:
            loss = []
            for _key, _loss_wrapper in self._losswrapper_dict.items():
                loss.append(self._loss_weight_dict[_key] * _loss_wrapper.calculate_loss(output, batch, training_context, last_activation=last_activation))
            return loss
        else:
            print('Invalude value for `reduce`, given `{}`'.format(self._reduce))


class L1Regularizer(AbstractLossWrapper):
    def __init__(self, model_output_key=None):
        super(L1Regularizer, self).__init__()
        self.l1loss = torch.nn.L1Loss(size_average=None, reduce=None, reduction='mean')
        self._model_output_key = model_output_key

    def calculate_loss(self, output, batch, training_context, last_activation=None):
        if self._model_output_key is not None:
            output = output[self._model_output_key]

        return self.l1loss(output, torch.zeros_like(output, device=output.device))


