#%%
from itertools import groupby
from collections import OrderedDict
import re
#%%
# REF : https://www.geeksforgeeks.org/python-grouping-similar-substrings-in-list/
# REF : https://stackoverflow.com/questions/24310945/group-items-by-string-pattern-in-python/24311596
# REF : https://stackoverflow.com/questions/36300158/split-text-after-the-second-occurrence-of-character

model_map_block_lambdas = OrderedDict([('Dense121',  lambda text: (re.findall("(denseblock[\d]+\.denselayer[\d]+)", text) + [text])[0]),
                        ('Dense169' , lambda text: (re.findall("(denseblock[\d]+\.denselayer[\d]+)", text) + [text])[0]),
                        ('Dense161' , lambda text: (re.findall("(denseblock[\d]+\.denselayer[\d]+)", text) + [text])[0]),
                        ('Dense201' , lambda text: (re.findall("(denseblock[\d]+\.denselayer[\d]+)", text) + [text])[0]),
                        ('Resnet50' , lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('Resnet101' , lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),   
                        ('InceptionV3', None),# models.inception_v3(pretrained=True),
                        ('se_resnext50', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('se_resnext101', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('se_resnet50', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('se_resnet101', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('se_resnet152', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('resnext101', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('resnext101_64', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('senet154', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('polynet', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('dpn92', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('dpn68b', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('nasnetamobile', lambda text: (re.findall("(cell_[\w]+)", text) + [text])[0]),
                        ('resnext101_32_8_wsl', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('resnext101_32_16_wsl', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('resnext101_32_32_wsl', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('resnext101_32_48_wsl', lambda text: (re.findall("(layer[\d]+\.[\d]+)", text) + [text])[0]),
                        ('efficientnet-b0', lambda a: a.partition('._')[0]), 
                        ('efficientnet-b1', lambda a: a.partition('._')[0]), 
                        ('efficientnet-b2', lambda a: a.partition('._')[0]), 
                        ('efficientnet-b3', lambda a: a.partition('._')[0]),  
                        ('efficientnet-b4', lambda a: a.partition('._')[0]), 
                        ('efficientnet-b5', lambda a: a.partition('._')[0]),  
                        ('efficientnet-b6', lambda a: a.partition('._')[0]), 
                        ('efficientnet-b7', lambda a: a.partition('._')[0]),
                    ])


#%%

# # Used for testing
# model_type = 'nasnetamobile'
# mod = model(num_classes=2, model_type=model_type, input_image_size=224)

def get_param_blocks(model_type, model_net):
    params_names_list = []
    for i, (name, module) in enumerate(model_net.named_parameters()):
        # print('{} : {}'.format(i,name))
        params_names_list.append(name)

    keyf = model_map_block_lambdas[model_type]
    res = [list(i) for j, i in groupby(params_names_list,keyf)] 

    if keyf:
        res = [list(i) for j, i in groupby(params_names_list,keyf)] 
    else:
        print('lambda for extracting block ids not defined for `{}`'.format(model_type))

    # print('Before second pass list {}'.format(res))

    new_list = []
    temp_block = []
    keyf =  lambda a: a.partition('.')[0] # lambda for secondary grouping
    for i,_ in enumerate(res):
        
        if len(res[i]) == 1:
            temp_block.append(res[i][0])   
            if i == len(res)-1:
                temp_block = [list(i) for j, i in groupby(temp_block,keyf)]
                new_list.extend(temp_block)     
        else:
            if len(temp_block) >= 1:

                temp_block = [list(i) for j, i in groupby(temp_block,keyf)]

                new_list.extend(temp_block)
                temp_block = []
            new_list.append(res[i])
    # print('Final list {}'.format(new_list))
    return new_list
