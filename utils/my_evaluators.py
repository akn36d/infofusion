import numpy as np
from sklearn import metrics
from pytorch_wrapper.evaluators import AbstractEvaluator, AbstractEvaluatorResults, GenericEvaluatorResults 

def get_multilabel_confusion_matrix(y_true, y_pred, sample_weight=None, samplewise=False):
    mcon = metrics.multilabel_confusion_matrix(y_true, y_pred, sample_weight=None, samplewise=False)
    true_positives = mcon[:,1,1]
    false_positives = mcon[:,0,1]
    true_negatives = mcon[:,0,0]
    false_negatives =  mcon[:,1,0]

    return true_positives,false_positives, true_negatives, false_negatives


# TODO:  Add class based 'is_better_than' and 'compare_to' functions
class GenericMultiLabelEvaluatorResults(AbstractEvaluatorResults):
    """
    Generic multi-label evaluator results.
    """

    def __init__(self, score, label='mc-score', num_classes=0, class_names=None, is_max_better=True):
        """
        :param score: Numeric value that represents the score.
        :param label: String used in the str representation.
        :param num_classes: Numeric value that specifies number of classes.
        :param class_names: list of strings with the class names.
        """

        super(GenericMultiLabelEvaluatorResults, self).__init__()
        self._score = score
        self._label = label
        self._is_max_better = is_max_better

        if class_names:
            self._class_names = class_names
            if not (len(self._score) == len(class_names)):
                raise ('length of scores calculated {} and number of classes {} not matching '.format(len(self._score), len(class_names)))
        elif (num_classes>0):
            self._num_classes = num_classes
            if not (len(self._score) == num_classes):
                raise ('length of scores calculated {} and number of classes {} not matching '.format(len(self._score), num_classes))
        else:
            raise ('Need to give one of num_classes or class_names got {} and {}'.format(num_classes, class_names))

    @property
    def score(self):
        return self._score
    
    def is_better_than(self, other_results_object):
        """
        Compares these results with the results of another object.

        :param other_results_object: Object of the `GenericMultiLabelEvaluatorResults` class or `GenericEvaluatorResults` class.
        """

        pass

    def compare_to(self, other_results_object):
        """
        Compares these results with the results of another object.

        :param other_results_object: Object of the same class.
        """

        pass

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        if self._class_names:
            results_str =  self._label + ': \n'
            for i, _class in enumerate(self._class_names):
                results_str += '\t{} : {}\n'.format(_class, self._score[i])
        elif (self._num_classes==0):
            results_str =  self._label + ': \n'
            for c_idx in range(self._num_classes):
                results_str += '\t{} : {}\n'.format(c_idx, self._score[c_idx])

        return results_str




class GenericPointWiseMultiLabelLossEvaluator(AbstractEvaluator):
    """
    Adapter that uses an object of a class derived from AbstractLossWrapper to calculate the class-wise loss during evaluation.
    """

    def __init__(self, 
                loss_wrapper, 
                label='classwise-loss', 
                batch_target_key='target', 
                class_names=None):
        """
        :param loss_wrapper: AbstractLossWrapper object that calculates the loss.
        :param label: Str used as label during printing of the loss.
        :param score_format: Format used for str representation of the loss.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param class_names: list of class names 
        """

        super(GenericPointWiseMultiLabelLossEvaluator, self).__init__()
        self._loss_wrapper = loss_wrapper
        self._label = label
        self._batch_target_key = batch_target_key
        self._class_names = class_names
        self.reset()

    def reset(self):
        self._loss = 0
        self._examples_nb = 0 # tracking number of samples seen in total

    def step(self, output, batch, last_activation=None):
        current_loss = np.array(self._loss_wrapper.calculate_loss(output, batch, None, last_activation).squeeze().tolist()).astype(np.float)
        self._loss += current_loss.sum(axis=0)
        self._examples_nb += batch[self._batch_target_key].shape[0]

    def calculate(self):
        return GenericMultiLabelEvaluatorResults(
            self._loss / self._examples_nb,
            self._label,
            class_names=self._class_names
        )


class MultilabelStepwisePrecisionEvaluator(AbstractEvaluator):
    """
    Evaluator class for binary and Multi-Class Precision.
    """

    def __init__(self, model_output_key=None, batch_target_key='target', average='micro', class_names=None):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. Leave None
            if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param average : string, ['classwise', ‘micro’, ‘macro’, ‘weighted’]
            This parameter is required for multiclass/multilabel targets. If None, the scores for each class are returned. 
            Otherwise, this determines the type of averaging performed on the data:
                'classwise': Calculate the scores for each class separately
                'micro': Calculate metrics globally by counting the total true positives, false negatives and false positives.
                'macro': Calculate metrics for each label, and find their unweighted mean. This does not take label imbalance into account.
                'weighted': Calculate metrics for each label, and find their average weighted by support (the number of true instances for each label). 
                    This alters ‘macro’ to account for label imbalance; it can result in an F-score that is not between precision and recall.
        :param class_names : list of strings with the class names (Note: for binary classification this will be taken as 2 classes ['class1', 'class2'])
        Note:
            similar to precision_score for sklearn
            REF: https://datascience.stackexchange.com/questions/66388/what-is-the-formula-to-calculate-the-precision-recall-f-measure-with-macro-mi
        """

        super(MultilabelStepwisePrecisionEvaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._average = average
        self._class_names = class_names
        self._warn_for = ('precision') # used internally
        self.reset()

    def reset(self):
        self._examples_nb = 0 # tracking number of samples seen in total = 0
        self._steps = 0 # tracing number of batches
        self._tp_cumulative = 0
        self._fp_cumulative = 0
        self._fn_cumulative = 0 # needed to calculate true number of samples for weighted metric case

    def step(self, output, batch, last_activation=None):
        self._steps += 1
        if self._model_output_key is not None:
            output = output[self._model_output_key]
        if last_activation is not None:
            output = last_activation(output)
        
        self._examples_nb += output.shape[0]

        outputs = output.tolist()
        targets = batch[self._batch_target_key].tolist()

        tp, fp, _, fn = get_multilabel_confusion_matrix(y_true = targets, y_pred=outputs)
        
        self._tp_cumulative += tp 
        self._fp_cumulative += fp
        self._fn_cumulative += fn


    def calculate(self):
        tp = self._tp_cumulative
        fp = self._fp_cumulative
        fn = self._fn_cumulative

        if self._average in ['classwise', 'macro', 'weighted'] :
            precision = metrics.classification._prf_divide(tp, tp+fp , 'precision',
                            'predicted', self._average, warn_for=self._warn_for )                
            if self._average == 'classwise':
                return GenericMultiLabelEvaluatorResults(precision, label=self._average+'-precision', class_names=self._class_names)
            if self._average == 'weighted':
                weights = tp + fn
                precision = np.dot(precision, weights)
                return GenericEvaluatorResults(precision ,self._average + '-precision', '%5.4f', is_max_better=True)                    
            if self._average == 'macro':
                precision = metrics.classification._prf_divide(precision.sum(), len(self._class_names), 'precision',
                            'predicted', self._average, warn_for=self._warn_for )
                return GenericEvaluatorResults(precision ,self._average + '-precision', '%5.4f', is_max_better=True)

        if self._average == 'micro':
            precision = metrics.classification._prf_divide(np.sum(tp), np.sum(tp+fp) , 'precision',
                            'predicted', self._average, warn_for=self._warn_for )
            return GenericEvaluatorResults(precision ,self._average + '-precision', '%5.4f', is_max_better=True)


class MultilabelStepwiseRecallEvaluator(AbstractEvaluator):
    """
    Evaluator class for binary and Multi-Class Recall.
    """

    def __init__(self, model_output_key=None, batch_target_key='target', average='micro', class_names=None):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. Leave None
            if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param average : string, ['classwise', ‘micro’, ‘macro’, ‘samples’, ‘weighted’]
            This parameter is required for multiclass/multilabel targets. If None, the scores for each class are returned. 
            Otherwise, this determines the type of averaging performed on the data:
                'classwise': Calculate the scores for each class separately
                'micro': Calculate metrics globally by counting the total true positives, false negatives and false positives.
                'macro': Calculate metrics for each label, and find their unweighted mean. This does not take label imbalance into account.
                'weighted': Calculate metrics for each label, and find their average weighted by support (the number of true instances for each label). 
                    This alters ‘macro’ to account for label imbalance; it can result in an F-score that is not between precision and recall.
                'samples': Calculate metrics for each instance, and find their average (only meaningful for multilabel classification where this differs from accuracy_score).
        :param sample_weight : array-like of shape = [n_samples], optional Sample weights.
        :param class_names : list of strings with the class names (Note: for binary classification this will be taken as 2 classes ['class1', 'class2'])
        Note:
            similar to recall_score for sklearn
            REF: https://datascience.stackexchange.com/questions/66388/what-is-the-formula-to-calculate-the-precision-recall-f-measure-with-macro-mi
        """

        super(MultilabelStepwiseRecallEvaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._average = average
        self._class_names = class_names
        self._warn_for = ('recall')
        self.reset()

    def reset(self):
        self._examples_nb = 0 # tracking number of samples seen in total = 0
        self._steps = 0 # tracing number of batches
        self._tp_cumulative = 0
        self._fn_cumulative = 0
        self._r_cumulative = 0

    def step(self, output, batch, last_activation=None):
        self._steps += 1
        if self._model_output_key is not None:
            output = output[self._model_output_key]
        if last_activation is not None:
            output = last_activation(output)
        
        self._examples_nb += output.shape[0]

        outputs = output.tolist()
        targets = batch[self._batch_target_key].tolist()

        tp, _, _, fn = get_multilabel_confusion_matrix(y_true = targets, y_pred=outputs)
        
        self._tp_cumulative += tp 
        self._fn_cumulative += fn


    def calculate(self):
        tp = self._tp_cumulative
        fn = self._fn_cumulative

        if self._average in ['classwise', 'macro', 'weighted'] :
            recall = metrics.classification._prf_divide(tp, tp+fn, 'recall',
                            'true', self._average, warn_for=self._warn_for )                
            if self._average == 'classwise':
                return GenericMultiLabelEvaluatorResults(recall, label=self._average+'-recall', class_names=self._class_names)
            if self._average == 'weighted':
                weights = tp + fn 
                recall = np.dot(recall, weights)
                return GenericEvaluatorResults(recall ,self._average + '-recall', '%5.4f', is_max_better=True)                    
            if self._average == 'macro':
                recall = metrics.classification._prf_divide(recall.sum(), len(self._class_names), 'recall',
                            'true', self._average, warn_for=self._warn_for )
                return GenericEvaluatorResults(recall ,self._average + '-recall', '%5.4f', is_max_better=True)

        if self._average == 'micro':
            recall = metrics.classification._prf_divide(np.sum(tp), np.sum(tp+fn) , 'recall',
                            'true', self._average, warn_for=self._warn_for )
            return GenericEvaluatorResults(recall ,self._average + '-recall', '%5.4f', is_max_better=True)



class MultilabelStepwiseFscoreEvaluator(AbstractEvaluator):
    """
    Evaluator class for binary and Multi-Class F-score.
    """

    def __init__(self, model_output_key=None, batch_target_key='target', beta=1.0, average='micro', class_names=None):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. Leave None
            if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param beta: The F-beta score weights recall more than precision by a factor of
            ``beta``. ``beta == 1.0`` means recall and precision are equally important
        :param average : string, ['classwise', ‘micro’, ‘macro’, ‘weighted’]
            This parameter is required for multiclass/multilabel targets. If None, the scores for each class are returned. 
            Otherwise, this determines the type of averaging performed on the data:
                'classwise': Calculate the scores for each class separately
                'micro': Calculate metrics globally by counting the total true positives, false negatives and false positives.
                'macro': Calculate metrics for each label, and find their unweighted mean. This does not take label imbalance into account.
                'weighted': Calculate metrics for each label, and find their average weighted by support (the number of true instances for each label). 
                    This alters ‘macro’ to account for label imbalance; it can result in an F-score that is not between precision and recall.
        :param class_names : list of strings with the class names (Note: for binary classification this will be taken as 2 classes ['class1', 'class2'])
        Note:
            Same as the fscore for sklearn
            Parts fo code taken from version 0.21.3 of sklearn
            REF: https://github.com/scikit-learn/scikit-learn/blob/fd237278e895b42abe8d8d09105cbb82dc2cbba7/sklearn/metrics/_classification.py
        """

        super(MultilabelStepwiseFscoreEvaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._average = average
        self._beta = beta
        self._class_names = class_names
        self._warn_for = ('precision', 'recall', 'f-score')

        self.precision_callback = MultilabelStepwisePrecisionEvaluator(
                                                                model_output_key=self._model_output_key,
                                                                batch_target_key=self._batch_target_key,
                                                                average='classwise',
                                                                class_names=self._class_names
                                                            )
        self.recall_callback = MultilabelStepwiseRecallEvaluator(
                                                                model_output_key=self._model_output_key,
                                                                batch_target_key=self._batch_target_key,
                                                                average='classwise',
                                                                class_names=self._class_names
                                                            )
        
        self.reset()

    def reset(self):
        self.precision_callback.reset()
        self.recall_callback.reset()

    def step(self, output, batch, last_activation=None):
        self.precision_callback.step(output,batch,last_activation)
        self.recall_callback.step(output,batch,last_activation)

    def calculate(self):
        beta2 = self._beta ** 2
        tp = self.precision_callback._tp_cumulative
        fn = self.recall_callback._fn_cumulative
        fp = self.precision_callback._fp_cumulative

        pred_values = tp + fp
        true_values = tp + fn

        if self._average == 'micro':
            tp = np.array([tp.sum()])
            pred_values = np.array([pred_values.sum()])
            true_values = np.array([true_values.sum()])

        # Divide, and on zero-division, set scores and/or warn according to
        # zero_division:
        precision = metrics.classification._prf_divide(tp, pred_values, 'precision',
                                'predicted', self._average, self._warn_for)
        recall = metrics.classification._prf_divide(tp, true_values, 'recall',
                            'true', self._average, self._warn_for)

        denom = beta2 * precision + recall
        denom[denom == 0.] = 1  # avoid division by 0
        f_score = (1 + beta2) * precision * recall / denom

        # Average the results
        if self._average == 'weighted':
            weights = true_values
            if weights.sum() == 0:
                return GenericEvaluatorResults(0 ,self._average + '-f_score', '%5.4f', is_max_better=True)
        else:
            weights = None

        if self._average is not 'classwise':
            f_score = np.average(f_score, weights=weights)
            return GenericEvaluatorResults(f_score ,self._average + '-f_score', '%5.4f', is_max_better=True)
        else:
            return GenericMultiLabelEvaluatorResults(f_score, label=self._average+'-f_score', class_names=self._class_names)

# Non stepwise evaluators
class MultilabelPrecisionEvaluator(AbstractEvaluator):
    """
    Evaluator class for binary and Multi-Class Precision.
    """

    def __init__(self, model_output_key=None, batch_target_key='target', average='micro', class_names=None):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. Leave None
            if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param average : string, ['classwise', ‘micro’, ‘macro’, ‘weighted’]
            This parameter is required for multiclass/multilabel targets. If None, the scores for each class are returned. 
            Otherwise, this determines the type of averaging performed on the data:
                'classwise': Calculate the scores for each class separately
                'micro': Calculate metrics globally by counting the total true positives, false negatives and false positives.
                'macro': Calculate metrics for each label, and find their unweighted mean. This does not take label imbalance into account.
                'weighted': Calculate metrics for each label, and find their average weighted by support (the number of true instances for each label). 
                    This alters ‘macro’ to account for label imbalance; it can result in an F-score that is not between precision and recall.
                'samples': Calculate metrics for each instance, and find their average 
                    (only meaningful for multilabel classification where this differs from accuracy_score).
        :param class_names : list of strings with the class names (Note: for binary classification this will be taken as 2 classes ['class1', 'class2'])
        Note:
            similar to precision_score for sklearn
            REF: https://datascience.stackexchange.com/questions/66388/what-is-the-formula-to-calculate-the-precision-recall-f-measure-with-macro-mi
        """

        super(MultilabelPrecisionEvaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._average = average
        self._class_names = class_names
        self.reset()

    def reset(self):
        self._examples_nb = 0 # tracking number of samples seen in total = 0
        self._steps = 0 # tracing number of batches
        self._tp_cumulative = 0
        self._fp_cumulative = 0
        self._fn_cumulative = 0 # needed to calculate true number of samples for weighted metric case

    def step(self, output, batch, last_activation=None):
        self._steps += 1
        if self._model_output_key is not None:
            output = output[self._model_output_key]
        if last_activation is not None:
            output = last_activation(output)
        
        self._examples_nb += output.shape[0]

        outputs = output.tolist()
        targets = batch[self._batch_target_key].tolist()

        tp, fp, _, fn = get_multilabel_confusion_matrix(y_true = targets, y_pred=outputs)
        
        self._tp_cumulative += tp 
        self._fp_cumulative += fp
        self._fn_cumulative += fn

    def calculate(self):
        tp = self._tp_cumulative
        fp = self._fp_cumulative
        fn = self._fn_cumulative

        if self._average in ['classwise', 'macro', 'weighted'] :
            precision = metrics.classification._prf_divide(tp, tp+fp , 'precision',
                            'predicted', self._average, warn_for=self._warn_for )                
            if self._average == 'classwise':
                return GenericMultiLabelEvaluatorResults(precision, label=self._average+'-precision', class_names=self._class_names)
            if self._average == 'weighted':
                weights = tp + fn
                precision = np.dot(precision, weights)
                return GenericEvaluatorResults(precision ,self._average + '-precision', '%5.4f', is_max_better=True)                    
            if self._average == 'macro':
                precision = metrics.classification._prf_divide(precision.sum(), len(self._class_names), 'precision',
                            'predicted', self._average, warn_for=self._warn_for )
                return GenericEvaluatorResults(precision ,self._average + '-precision', '%5.4f', is_max_better=True)

        if self._average == 'micro':
            precision = metrics.classification._prf_divide(np.sum(tp), np.sum(tp+fp) , 'precision',
                            'predicted', self._average, warn_for=self._warn_for )
            return GenericEvaluatorResults(precision ,self._average + '-precision', '%5.4f', is_max_better=True)


class MultilabelRecallEvaluator(AbstractEvaluator):
    """
    Evaluator class for binary and Multi-Class Recall.
    """ 

    def __init__(self, model_output_key=None, batch_target_key='target', average='micro', class_names=None):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. Leave None
            if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param average : string, ['classwise', ‘micro’, ‘macro’, ‘samples’, ‘weighted’]
            This parameter is required for multiclass/multilabel targets. If None, the scores for each class are returned. 
            Otherwise, this determines the type of averaging performed on the data:
                'classwise': Calculate the scores for each class separately
                'micro': Calculate metrics globally by counting the total true positives, false negatives and false positives.
                'macro': Calculate metrics for each label, and find their unweighted mean. This does not take label imbalance into account.
                'weighted': Calculate metrics for each label, and find their average weighted by support (the number of true instances for each label). 
                    This alters ‘macro’ to account for label imbalance; it can result in an F-score that is not between precision and recall.
                'samples': Calculate metrics for each instance, and find their average (only meaningful for multilabel classification where this differs from accuracy_score).
        :param sample_weight : array-like of shape = [n_samples], optional Sample weights.
        :param class_names : list of strings with the class names (Note: for binary classification this will be taken as 2 classes ['class1', 'class2'])
        Note:
            similar to recall_score for sklearn
            REF: https://datascience.stackexchange.com/questions/66388/what-is-the-formula-to-calculate-the-precision-recall-f-measure-with-macro-mi
        """

        super(MultilabelStepwiseRecallEvaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._average = average
        self._class_names = class_names
        self._warn_for = ('recall')
        self.reset()

    def reset(self):
        self._examples_nb = 0 # tracking number of samples seen in total = 0
        self._steps = 0 # tracing number of batches
        self._tp_cumulative = 0
        self._fn_cumulative = 0
        self._r_cumulative = 0

    def step(self, output, batch, last_activation=None):
        self._steps += 1
        if self._model_output_key is not None:
            output = output[self._model_output_key]
        if last_activation is not None:
            output = last_activation(output)
        
        self._examples_nb += output.shape[0]

        outputs = output.tolist()
        targets = batch[self._batch_target_key].tolist()

        tp, _, _, fn = get_multilabel_confusion_matrix(y_true = targets, y_pred=outputs)
        
        self._tp_cumulative += tp 
        self._fn_cumulative += fn

    def calculate(self):
        tp = self._tp_cumulative
        fn = self._fn_cumulative

        if self._average in ['classwise', 'macro', 'weighted'] :
            recall = metrics.classification._prf_divide(tp, tp+fn, 'recall',
                            'true', self._average, warn_for=self._warn_for )                
            if self._average == 'classwise':
                return GenericMultiLabelEvaluatorResults(recall, label=self._average+'-recall', class_names=self._class_names)
            if self._average == 'weighted':
                weights = tp + fn 
                recall = np.dot(recall, weights)
                return GenericEvaluatorResults(recall ,self._average + '-recall', '%5.4f', is_max_better=True)                    
            if self._average == 'macro':
                recall = metrics.classification._prf_divide(recall.sum(), len(self._class_names), 'recall',
                            'true', self._average, warn_for=self._warn_for )
                return GenericEvaluatorResults(recall ,self._average + '-recall', '%5.4f', is_max_better=True)

        if self._average == 'micro':
            recall = metrics.classification._prf_divide(np.sum(tp), np.sum(tp+fn) , 'recall',
                            'true', self._average, warn_for=self._warn_for )
            return GenericEvaluatorResults(recall ,self._average + '-recall', '%5.4f', is_max_better=True)



class MultilabelFscoreEvaluator(AbstractEvaluator):
    """
    Evaluator class for binary and Multi-Class F-score.
    """

    def __init__(self, model_output_key=None, batch_target_key='target', beta=1.0, average='micro', class_names=None):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. Leave None
            if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param beta: The F-beta score weights recall more than precision by a factor of
            ``beta``. ``beta == 1.0`` means recall and precision are equally important
        :param average : string, ['classwise', ‘micro’, ‘macro’, ‘weighted’]
            This parameter is required for multiclass/multilabel targets. If None, the scores for each class are returned. 
            Otherwise, this determines the type of averaging performed on the data:
                'classwise': Calculate the scores for each class separately
                'micro': Calculate metrics globally by counting the total true positives, false negatives and false positives.
                'macro': Calculate metrics for each label, and find their unweighted mean. This does not take label imbalance into account.
                'weighted': Calculate metrics for each label, and find their average weighted by support (the number of true instances for each label). 
                    This alters ‘macro’ to account for label imbalance; it can result in an F-score that is not between precision and recall.
        :param class_names : list of strings with the class names (Note: for binary classification this will be taken as 2 classes ['class1', 'class2'])
        Note:
            Same as the fscore for sklearn
            Parts fo code taken from version 0.21.3 of sklearn
            REF: https://github.com/scikit-learn/scikit-learn/blob/fd237278e895b42abe8d8d09105cbb82dc2cbba7/sklearn/metrics/_classification.py
        """
        super(MultilabelStepwiseFscoreEvaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._average = average
        self._beta = beta
        self._class_names = class_names
        self._warn_for = ('precision', 'recall', 'f-score')        
        self.reset()

    def reset(self):
        self.precision_callback.reset()
        self.recall_callback.reset()

    def step(self, output, batch, last_activation=None):
        self.precision_callback.step(output,batch,last_activation)
        self.recall_callback.step(output,batch,last_activation)

    def calculate(self):
        beta2 = self._beta ** 2
        tp = self.precision_callback._tp_cumulative
        fn = self.recall_callback._fn_cumulative
        fp = self.precision_callback._fp_cumulative

        pred_values = tp + fp
        true_values = tp + fn

        if self._average == 'micro':
            tp = np.array([tp.sum()])
            pred_values = np.array([pred_values.sum()])
            true_values = np.array([true_values.sum()])

        # Divide, and on zero-division, set scores and/or warn according to
        # zero_division:
        precision = metrics.classification._prf_divide(tp, pred_values, 'precision',
                                'predicted', self._average, self._warn_for)
        recall = metrics.classification._prf_divide(tp, true_values, 'recall',
                            'true', self._average, self._warn_for)

        denom = beta2 * precision + recall
        denom[denom == 0.] = 1  # avoid division by 0
        f_score = (1 + beta2) * precision * recall / denom

        # Average the results
        if self._average == 'weighted':
            weights = true_values
            if weights.sum() == 0:
                return GenericEvaluatorResults(0 ,self._average + '-f_score', '%5.4f', is_max_better=True)
        else:
            weights = None

        if self._average is not 'classwise':
            f_score = np.average(f_score, weights=weights)
            return GenericEvaluatorResults(f_score ,self._average + '-f_score', '%5.4f', is_max_better=True)
        else:
            return GenericMultiLabelEvaluatorResults(f_score, label=self._average+'-f_score', class_names=self._class_names)
