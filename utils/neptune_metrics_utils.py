# Credits : https://ui.neptune.ai/o/neptune-ai/org/binary-classification-metrics/e/BIN-101/logs

import tempfile
import joblib

import matplotlib.pyplot as plt
import neptune
from neptunecontrib.monitoring.utils import send_figure
import pandas as pd
import scikitplot.metrics as plt_metrics
from scikitplot.helpers import binary_ks_curve
import seaborn as sns
import sklearn.metrics as sk_metrics

plt.rcParams.update({'font.size': 18})
plt.rcParams.update({'figure.figsize': [16, 12]})


def log_binary_classification_metrics(y_true, y_pred, threshold=0.5):
    # Confusion Matrix
    fig, ax = plt.subplots()
    plot_confusion_matrix(y_true, y_pred[:, 1] > threshold, ax=ax)
    send_figure(fig, channel_name='metric_charts')
    plt.close()

    # Plot classification report
    fig = plot_classification_report(y_true, y_pred[:, 1] > threshold)
    send_figure(fig, channel_name='metric_charts')
    plt.close()

    scores = _class_metrics(y_true, y_pred[:, 1], threshold)
    for metric_name, score in scores.items():
        print('{} :{}'.format(metric_name, score))
        neptune.log_metric(metric_name, score)

    # ROC AUC
    roc_auc = sk_metrics.roc_auc_score(y_true, y_pred[:, 1])
    print('ROC AUC score {}'.format(roc_auc))
    neptune.log_metric('roc_auc', roc_auc)

    fig, ax = plt.subplots()
    plt_metrics.plot_roc(y_true, y_pred, ax=ax)
    send_figure(fig, channel_name='metric_charts')
    plt.close()

    # PR AUC
    avg_precision = sk_metrics.average_precision_score(y_true, y_pred[:, 1])
    print('Average precision score {}'.format(avg_precision))
    neptune.log_metric('avg_precision', avg_precision)

    fig, ax = plt.subplots()
    plt_metrics.plot_precision_recall(y_true, y_pred, ax=ax)
    send_figure(fig, channel_name='metric_charts')
    plt.close()

    # Brier loss
    brier = sk_metrics.brier_score_loss(y_true, y_pred[:, 1])
    print('Brier loss {}'.format(brier))
    neptune.log_metric('brier_loss', brier)

    # Log loss
    log_loss = sk_metrics.log_loss(y_true, y_pred)
    print('Log loss {}'.format(log_loss))
    neptune.log_metric('log_loss', log_loss)

    # Kolmogorov Smirnow
    res = binary_ks_curve(y_true, y_pred[:, 1])
    ks_stat = res[3]
    print('KS stat {}'.format(ks_stat))
    neptune.log_metric('ks_statistic', ks_stat)

    fig, ax = plt.subplots()
    plt_metrics.plot_ks_statistic(y_true, y_pred, ax=ax)
    send_figure(fig, channel_name='metric_charts')
    plt.close()

    # Cumulative gain
    fig, ax = plt.subplots()
    plt_metrics.plot_cumulative_gain(y_true, y_pred, ax=ax)
    send_figure(fig, channel_name='metric_charts')
    plt.close()

    # Lift curve
    fig, ax = plt.subplots()
    plt_metrics.plot_lift_curve(y_true, y_pred, ax=ax)
    send_figure(fig, channel_name='metric_charts')
    plt.close()

    # Plots by threshold
    figs = plot_metrics_per_threshold(y_true, y_pred[:, 1])

    for fig in figs:
        send_figure(fig, channel_name='metrics_by_threshold')
        plt.close()


def plot_confusion_matrix(y_true, y_pred_class, ax=None):
    cmap = plt.get_cmap('Blues')
    cm = sk_metrics.confusion_matrix(y_true, y_pred_class)
    sns.heatmap(cm, cmap=cmap, annot=True, fmt='g', ax=ax)


def plot_metrics_per_threshold(y_true, y_pred_positive):
    scores_by_thres = _class_metrics_by_threshold(y_true, y_pred_positive)
    figs = []
    for name in scores_by_thres.columns:
        if name == 'threshold':
            continue
        else:
            best_thres, best_score = _get_best_thres(scores_by_thres, name)
            fig, ax = plt.subplots()
            ax.plot(scores_by_thres['threshold'], scores_by_thres[name])
            ax.set_title('{} by threshold'.format(name))
            ax.axvline(x=best_thres, color='red')
            ax.text(x=best_thres, y=0.96 * best_score,
                    s='thres={:.4f}\nscore={:.4f}'.format(best_thres, best_score),
                    color='red')
            figs.append(fig)
    return figs


def plot_classification_report(y_true, y_pred_class):
    report = sk_metrics.classification_report(y_true, y_pred_class, output_dict=True)
    report_df = pd.DataFrame(report).transpose().round(4)

    fig, ax = plt.subplots()
    ax.axis('off')
    ax.axis('tight')
    ax.table(cellText=report_df.values,
             colLabels=report_df.columns,
             rowLabels=report_df.index,
             loc='center',
             bbox=[0.2, 0.2, 0.8, 0.8])
    fig.tight_layout()

    return fig


def get_artifact(exp, path):
    with tempfile.TemporaryDirectory() as d:
        exp.download_artifact(path, d)
        filename = os.path.join(d, path)
        artifact = joblib.load(filename)
    return artifact


def _class_metrics(y_true, y_pred, threshold):
    y_pred_class = y_pred > threshold

    tn, fp, fn, tp = sk_metrics.confusion_matrix(y_true, y_pred_class).ravel()

    true_positive_rate = tp / (tp + fn)
    true_negative_rate = tn / (tn + fp)
    positive_predictive_value = tp / (tp + fp)
    negative_predictive_value = tn / (tn + fn)
    false_positive_rate = fp / (fp + tn)
    false_negative_rate = fn / (tp + fn)
    false_discovery_rate = fp / (tp + fp)

    scores = {'accuracy': sk_metrics.accuracy_score(y_true, y_pred_class),
              'precision': sk_metrics.precision_score(y_true, y_pred_class),
              'recall': sk_metrics.recall_score(y_true, y_pred_class),
              'f1_score': sk_metrics.fbeta_score(y_true, y_pred_class, beta=1),
              'f2_score': sk_metrics.fbeta_score(y_true, y_pred_class, beta=2),
              'matthews_corrcoef': sk_metrics.matthews_corrcoef(y_true, y_pred_class),
              'cohen_kappa': sk_metrics.cohen_kappa_score(y_true, y_pred_class),
              'true_positive_rate': true_positive_rate,
              'true_negative_rate': true_negative_rate,
              'positive_predictive_value': positive_predictive_value,
              'negative_predictive_value': negative_predictive_value,
              'false_positive_rate': false_positive_rate,
              'false_negative_rate': false_negative_rate,
              'false_discovery_rate': false_discovery_rate,
              }

    return scores


def _class_metrics_by_threshold(y_true, y_pred_positive, thres_nr=100):
    thresholds = [i / thres_nr for i in range(1, thres_nr, 1)]

    scores_per_thres = []
    for thres in thresholds:
        scores = _class_metrics(y_true, y_pred_positive, thres)
        scores['threshold'] = thres
        scores_per_thres.append(pd.Series(scores))

    return pd.DataFrame(scores_per_thres)


def _get_best_thres(scores_by_thres, name):
    best_res = scores_by_thres[scores_by_thres[name] == scores_by_thres[name].max()][['threshold', name]]
    position = len(best_res) // 2
    result = best_res.iloc[position].to_dict()
    return result['threshold'], result[name]
