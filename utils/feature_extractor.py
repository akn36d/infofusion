#%%
import torch

# from models import model

#%%


class GetLayerOutputHook():
    def __init__(self, model, target_layer_name):
        self._model = model
        self.target_layer_name = target_layer_name
        self.target_activations = None

        self.activations_hook  = None

    def get_activations(self, module, input, output):
        self.target_activations = output
        
    def apply_hooks(self):
        """
            Apply hooks at given layer
        """
        for name, module in self._model.named_modules():            
            if self.target_layer_name == name:
                self.activations_hook = module.register_forward_hook(self.get_activations)
    
    def remove_hooks(self):
        self.activations_hook.remove()


# def main():
#     model_type = 'Resnet50'
#     num_classes = 5
#     my_net = model(num_classes=num_classes, model_type=model_type)

#     my_net.model_net.param_blocks

#     device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
#     ftrs_to_diags_hook = GetLayerOutputHook(my_net.model_net, target_layer_name=my_net.model_net.module_name_list[-1])
#     ftrs_to_diags_hook.apply_hooks()

#     my_net.model_net.to(device)

#     x = torch.rand((1,3,224,224), dtype=torch.float)
#     x = x.to(device)
#     out = my_net.model_net(x)

#     ftrs_to_diags_hook.remove_hooks()
#     print(ftrs_to_diags_hook.target_activations)
    
# #%%
# if __name__=='__main__':
#     main()

#%%
