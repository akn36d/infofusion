# %% [markdown]
# ### Building our model
from __future__ import division, print_function

import types

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.modules.linear import Identity

from .dysion_models import getModel
from .model_blocks import get_param_blocks
from .feature_extractor import GetLayerOutputHook


def tag_modules_with_names(model):
    module_name_list = []
    for name, module in model.named_modules():
        module.auto_name = name
        module_name_list.append(name)
    model.module_name_list = module_name_list

# #### Creating class to hold model and optimizer


def my_model(model_type, **kwargs):
    """Returns a function for a model
        Args:
            num_ftrs_new: the number of output features required, usually the number of output clases
            model_type: string with model name from model_map dict in the dysion_models file
                or from list ['featnet', 'featnetV2','myresnet50','myeffnetb0']
            kwargs:
                input_image_sz: size of the square input image 
                num_inputs: used when creating the `FeatNet` fully connected model   
        Returns:
            model: A class that builds the desired model
        Raises:
            ValueError: If model name is not recognized.
    """
    # all the conditions below are taken and modified slightly from the dysion code
    # Ref : https://github.com/ngessert/isic2019/blob/master/train.py
    if 'Dense' in model_type:
        input_image_sz = kwargs.pop('input_image_sz', None)
        net_model = getModel(model_type)
        if input_image_sz != 224:
            net_model = modify_densenet_avg_pool(net_model)
            # print(net_model)
        num_ftrs = net_model.classifier.in_features
        net_model.classifier = nn.Identity()
        net_model.out_features = num_ftrs
        # print(net_model)
    elif 'dpn' in model_type:
        net_model = getModel(model_type)
        num_ftrs = net_model.classifier.in_channels
        net_model.classifier = nn.Identity()
        net_model.out_features = num_ftrs
        # nn.Conv2d(num_ftrs,num_ftrs_new,[1,1])
        #net_model.add_module('real_classifier',nn.Linear(num_ftrs, num_classes))
        # print(net_model)
    elif 'efficient' in model_type:
        net_model = getModel(model_type)
        # Do nothing, output is prepared
        num_ftrs = net_model._fc.in_features
        net_model._dropout = nn.Dropout(p=0.6)
        net_model._fc = nn.Identity()
        net_model.out_features = num_ftrs
    elif 'wsl' in model_type or 'Inception' in model_type:
        net_model = getModel(model_type)
        num_ftrs = net_model.fc.in_features
        net_model.fc = nn.Identity()
        net_model.out_features = num_ftrs
    elif 'featnet' in model_type:
        num_in_features = kwargs.pop('num_inputs', None)
        num_out_features = kwargs.pop('num_outputs', None)
        if model_type == 'featnet':
            net_model = FeatNet(num_in_features, num_out_features)
            net_model.out_features = num_out_features
        elif model_type == 'featnetV2':
            net_model = FeatNetV2(num_in_features, num_out_features)
            net_model.out_features = num_out_features
        elif model_type == 'featnetV3':
            net_model = FeatNetV3(num_in_features, num_out_features)
            net_model.out_features = num_out_features
    else:
        net_model = getModel(model_type)
        num_ftrs = net_model.last_linear.in_features
        net_model.last_linear = nn.Identity()
        net_model.out_features = num_ftrs

    return net_model


def freeze_blocks(model_net, **kwargs):
    """Takes a model and freezes it in a blockwise manner
        Args:
            model_net: The pytorch model to freeze 
            kwargs : 
                unfreeze_blocks_len: Number of blocks to keep unfrozen starting from the end (or output) layers
                freeze_blocks_len: Number of blocks to freeze starting from the beginning (or input) layers
        Returns:
            None

    """
    unfreeze_blocks_len = kwargs.pop('unfreeze_blocks_len', None)
    freeze_blocks_len = kwargs.pop('freeze_blocks_len', None)

    param_block_list = get_param_blocks(model_net.model_type, model_net)
    model_net.param_blocks = param_block_list
    model_net.num_blocks = len(param_block_list)

    tag_modules_with_names(model_net)

    if freeze_blocks_len:
        if freeze_blocks_len > model_net.num_blocks:
            prnt_str = 'Warning : The given number of blocks to freeze {} is higher than the number of blocks in model {}, will freeze everything.'
            print(prnt_str.format(freeze_blocks_len, model_net.num_blocks))
            freeze_blocks_len = model_net.num_blocks
        else:
            freeze_blocks_len = freeze_blocks_len
    elif unfreeze_blocks_len:
        if unfreeze_blocks_len > model_net.num_blocks:
            prnt_str = 'Warning : The given number of blocks to keep unfrozen {} is higher than the number of blocks in model {}, will unfreeze everything.'
            freeze_blocks_len = 0
        else:
            freeze_blocks_len = model_net.num_blocks - unfreeze_blocks_len

    else:
        print("ERROR : Neither `unfreeze_blocks_len` or `freeze_blocks_len` specified.")

    for block_idx, block in enumerate(model_net.param_blocks):
        if block_idx < freeze_blocks_len:
            for _, (name, param) in enumerate(model_net.named_parameters()):
                for freeze_param_name in block:
                    if name in freeze_param_name:
                        param.requires_grad = False


def freeze_params(model_net, **kwargs):
    """Create a list of params to freeze before optimizing network

        unfreeze_len: the number of layers or blocks from the last_layer to keep unfrozen
    Notes:
        This turns off gradient calculation for the number of layers specified by 
        `unfreeze_len` starting from base of the model to the head           
    """
    unfreeze_len = kwargs.pop('unfreeze_len', None)
    freeze_len = kwargs.pop('freeze_len', 0)
    params_list = []
    for _, (name, param) in enumerate(model_net.named_parameters()):
        params_list.append(param)

    if unfreeze_len:
        freeze_len = len(params_list) - unfreeze_len

        if unfreeze_len > len(params_list):
            prnt_str = 'The given number of params to keep unfrozen {} is higher than the number of blocks in model {}'
            print(prnt_str.format(unfreeze_len, len(params_list)))

    for i in range(freeze_len):
        params_list[i].requires_grad = False
    return params_list

# def freeze_cnn(model_net):
#     # deactivate all
#     for param in model_net.parameters():
#         param.requires_grad = False

#     for param in model_net.end_fc.parameters():
#         param.requires_grad = True


# def tag_cnn_params(model_net, model_type):
#     # mark cnn parameters
#     for param in model_net.parameters():
#         param.is_cnn_param = True
#     if 'efficient' in model_type:
#         # Activate fc
#         for param in model_net._fc.parameters():
#             param.is_cnn_param = False
#     elif 'wsl' in model_type:
#         # Activate fc
#         for param in model_net.fc.parameters():
#             param.is_cnn_param = False
#     else:
#         # Activate fc
#         for param in model_net.last_linear.parameters():
#             param.is_cnn_param = False


class model(nn.Module):
    def __init__(self, model_type=None, **kwargs):
        super(model, self).__init__()

        self.model_type = model_type
        self.output_as_dict = kwargs.pop('output_as_dict', False)
        self.model_output_key = kwargs.pop('model_output_key', None)
        self.end_fc = kwargs.pop('end_fc', nn.Sequential(Identity()))

        if self.output_as_dict and not self.model_output_key:
            raise ValueError(
                '`output_as_dict` True but no `model_output_key` passed to be used as Key in dict ')

        input_image_sz = kwargs.pop('input_image_sz', None)
        num_inputs = kwargs.pop('num_inputs', None)
        # self.out_features = num_outputs

        if self.model_type:
            if input_image_sz:
                self.model_net = my_model(
                    self.model_type, input_image_sz=input_image_sz)
                self.out_features = self.model_net.out_features
            elif num_inputs:
                self.model_net = my_model(
                    self.model_type, num_inputs=num_inputs)
                self.out_features = self.model_net.out_features
            else:
                self.model_net = my_model(self.model_type)
                self.out_features = self.model_net.out_features
        else:
            self.model_net = nn.Identity()
            if num_inputs:
                self.out_features = num_inputs
            else:
                # print('No way to get number of output features so setting `self.out_features` as `None`')
                self.out_features = None

    def add_end_fc(self, end_fc, num_outputs):
        self.end_fc = end_fc
        self.out_features = num_outputs

    def forward(self, x):
        features = self.model_net(x)
        output = self.end_fc(features)

        if self.output_as_dict and self.model_output_key:
            return {self.model_output_key: output}
        else:
            return output


# %% Old or Experimental models
class multilevel_model(nn.Module):
    '''
        Note : If `feats_to_ftrs_model_type` and `num_secondary_feat_ftrs` are None then no secondary model is
            used for creating new features from the feature predictions. In this case the feature predictions 
            are directly used and fed into the final model.
    '''

    def __init__(self, input_image_sz,
                 num_diags, num_ftrs, num_secondary_img_ftrs, num_secondary_feat_ftrs,
                 image_to_ftrs_model_type,
                 feats_to_ftrs_model_type,
                 final_diags_model_type,
                 feats_detector_model_type,
                 image_to_ftrs_model_unfreeze_blocks_len=0,
                 feats_detector_model_unfreeze_blocks_len=0,
                 output_as_dict=False):

        super(multilevel_model, self).__init__()
        self.output_as_dict = output_as_dict

        self.model_type = {
            'image_to_ftrs_model': image_to_ftrs_model_type,
            'feats_to_ftrs_model': feats_to_ftrs_model_type,
            'feats_detector_model': feats_detector_model_type,
            'final_diags_model': final_diags_model_type
        }

        self.feats_detector_model = model(num_classes=num_ftrs,
                                          model_type=feats_detector_model_type,
                                          input_image_size=input_image_sz)

        if num_secondary_feat_ftrs and feats_to_ftrs_model_type:
            self.feats_to_ftrs_model = model(num_classes=num_secondary_feat_ftrs,
                                             model_type=feats_to_ftrs_model_type,
                                             num_inputs=num_ftrs)
        else:
            self.feats_to_ftrs_model = None
            num_secondary_feat_ftrs = num_ftrs

        self.image_to_ftrs_model = model(num_classes=num_secondary_img_ftrs,
                                         model_type=image_to_ftrs_model_type,
                                         input_image_size=input_image_sz)

        self.final_diags_model = model(num_classes=num_diags,
                                       model_type=final_diags_model_type,
                                       num_inputs=num_secondary_feat_ftrs + num_secondary_img_ftrs)

        # freezing model layers
        freeze_blocks(self.feats_detector_model,
                      feats_detector_model_unfreeze_blocks_len)
        freeze_blocks(self.image_to_ftrs_model,
                      image_to_ftrs_model_unfreeze_blocks_len)

    def forward(self, x):
        feat_predictions = self.feats_detector_model(x)
        ftrs_from_imgs = self.image_to_ftrs_model(x)

        if self.feats_to_ftrs_model:
            ftrs_from_feats = self.feats_to_ftrs_model(feat_predictions)
            combined_ftrs = torch.cat((ftrs_from_imgs, ftrs_from_feats), dim=1)
        else:
            combined_ftrs = torch.cat(
                (ftrs_from_imgs, feat_predictions), dim=1)
        output = self.final_diags_model(combined_ftrs)

        if self.output_as_dict:
            out = {'feats': feat_predictions, 'diags': output}
            self.model_output_keys = out.keys()
            return out
        else:
            return [feat_predictions, output]


class multilevel_model_v2(nn.Module):
    """
        possible kwargs:
            image_to_ftrs_model_freeze_blocks_len : Used in cases where models have blocks with layers that are interdependant (EfficientNet)
                Ref : https://keras.io/examples/vision/image_classification_efficientnet_fine_tuning/
            feats_to_ftrs_model_freeze_len
            final_diags_model_freeze_len 
            is_freeze_cnn
    """

    def __init__(self, input_image_sz,
                 num_diags, num_ftrs, num_secondary_img_ftrs, num_secondary_feat_ftrs,
                 image_to_ftrs_model_type, ftrs_to_feats_model_type, final_diags_model_type,
                 output_as_dict=False,
                 **kwargs):

        super(multilevel_model_v2, self).__init__()
        self.output_as_dict = output_as_dict

        self.model_type = {
            'image_to_ftrs_model': image_to_ftrs_model_type,
            'ftrs_to_feats_model': ftrs_to_feats_model_type,
            'final_diags_model': final_diags_model_type
        }

        self.ftrs_to_feats_model = model(num_classes=num_secondary_feat_ftrs,
                                         model_type=ftrs_to_feats_model_type,
                                         num_inputs=num_ftrs)

        self.image_to_ftrs_model = model(num_classes=num_secondary_img_ftrs,
                                         model_type=image_to_ftrs_model_type,
                                         input_image_size=input_image_sz)

        self.final_diags_model = model(num_classes=num_diags,
                                       model_type=final_diags_model_type,
                                       num_inputs=num_secondary_feat_ftrs + num_secondary_img_ftrs)

        # parse kwargs
        self.image_to_ftrs_model_unfreeze_blocks_len = kwargs.pop(
            'image_to_ftrs_model_unfreeze_blocks_len', None)
        self.ftrs_to_feats_model_unfreeze_len = kwargs.pop(
            'ftrs_to_feats_model_unfreeze_len', None)
        self.final_diags_model_unfreeze_len = kwargs.pop(
            'final_diags_model_unfreeze_len', None)

        # freezing model layers
        if self.ftrs_to_feats_model_unfreeze_len:
            freeze_params(self.ftrs_to_feats_model,
                          self.ftrs_to_feats_model_unfreeze_len)
        if self.final_diags_model_unfreeze_len:
            freeze_params(self.final_diags_model,
                          self.final_diags_model_unfreeze_len)
        if self.image_to_ftrs_model_unfreeze_blocks_len:
            freeze_blocks(self.image_to_ftrs_model,
                          self.image_to_ftrs_model_unfreeze_blocks_len)

    def forward(self, x):

        ftrs_from_imgs = self.image_to_ftrs_model(x)
        feat_predictions = self.ftrs_to_feats_model(ftrs_from_imgs)

        combined_ftrs = torch.cat((ftrs_from_imgs, feat_predictions), dim=1)

        output = self.final_diags_model(combined_ftrs)

        if self.output_as_dict:
            out = {'feats': feat_predictions, 'diags': output}
            self.model_output_keys = out.keys()
            return out
        else:
            return [feat_predictions, output]


def modify_densenet_avg_pool(model):
    def logits(self, features):
        x = F.relu(features, inplace=True)
        x = torch.mean(torch.mean(x, 2), 2)
        #x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x

    def forward(self, input):
        x = self.features(input)
        x = self.logits(x)
        return x

    # Modify methods
    model.logits = types.MethodType(logits, model)
    model.forward = types.MethodType(forward, model)
    return model

class SimpleFC(nn.Module):
    def __init__(self, in_features, out_features):
        super(SimpleFC, self).__init__()
        self.fc1 = nn.Linear(in_features, 32)
        self.fc2 = nn.Linear(32, out_features)

    # x represents our data
    def forward(self, x):
        # Pass data through fc1
        x = self.fc1(x)
        x = F.dropout(x)
        x = F.relu(x)

        x = self.fc2(x)
        x = F.relu(x)
        x = F.dropout(x)

        return x


class FeatNet(nn.Module):
    def __init__(self, in_features, out_features):
        super(FeatNet, self).__init__()
        self.fc1 = nn.Linear(in_features, 128)
        self.fc2 = nn.Linear(128, out_features)

    # x represents our data
    def forward(self, x):
        # Pass data through fc1
        x = self.fc1(x)
        x = self.fc2(x)
        x = F.relu(x)
        x = F.dropout(x)
        return x


class FeatNetV2(nn.Module):
    def __init__(self, in_features, out_features):
        super(FeatNetV2, self).__init__()
        self.fc1 = nn.Linear(in_features, 32)
        self.fc2 = nn.Linear(32, out_features)

    # x represents our data
    def forward(self, x):
        # Pass data through fc1
        x = self.fc1(x)
        x = self.fc2(x)
        x = F.relu(x)
        x = F.dropout(x)
        return x


class FeatNetV3(nn.Module):
    def __init__(self, in_features, out_features):
        super(FeatNetV2, self).__init__()
        self.fc1 = nn.Linear(in_features, 32)
        self.fc2 = nn.Linear(32, out_features)

    # x represents our data
    def forward(self, x):
        # Pass data through fc1
        x = self.fc1(x)
        x = self.fc2(x)
        x = F.relu(x)
        x = F.dropout(x)
        return x

# %%


def main():

    test_model = multilevel_model_v2(
        input_image_sz=224,
        num_diags=5,
        num_ftrs=5,
        num_secondary_img_ftrs=24,
        num_secondary_feat_ftrs=24,
        image_to_ftrs_model_type='efficientnet-b1',
        feats_to_ftrs_model_type='featnetV2',
        final_diags_model_type='featnetV2',
        output_as_dict=False,
        image_to_ftrs_model_freeze_blocks_len=15
    )


if __name__ == "__main__":
    main()
