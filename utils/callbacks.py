
import copy
import os
import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
from neptunecontrib.api import log_table
from pytorch_wrapper.training_callbacks import AbstractCallback
from torch.utils.tensorboard import SummaryWriter
from tqdm.auto import tqdm as auto_tqdm

from utils.metrics import (argmax_predictions, auc_thresholder,
                           my_plot_confusion_matrix, predictions_thresholder,
                           roc_auc_metric)
from utils.utils import makefolder_ifnotexists


class ClearCacheCallback(AbstractCallback):
    def __init__(self, device):
        super(ClearCacheCallback).__init__()
        self.device = device

    def on_evaluation_end(self, training_context):
        if self.device == torch.device('cuda'):
            torch.cuda.empty_cache()
            auto_tqdm.write('Cache emptied after epoch : {}'.format(training_context['_current_epoch']))


class TensorBoardGradCallBack(AbstractCallback):
    def __init__(self, log_dir, iter_patience=100):
        super(TensorBoardGradCallBack, self).__init__()
        self.writer = SummaryWriter(log_dir=log_dir)
        self.n_iter = 0
        self.n_iter_real = 0
        self.iter_patience = iter_patience

    def pre_optimization_step(self, training_context):
        """
        Called just before the optimization step.
        :param training_context: Dict containing information regarding the 
            training process.
        """
        model = training_context['system'].model
        self.n_iter_real += 1

        if self.n_iter_real % self.iter_patience == 0:
            self.n_iter += 1
            for name, param in model.named_parameters():
                if param.requires_grad:
                    self.writer.add_histogram(name, param.detach().cpu().numpy(), self.n_iter)

            for name, param in model.named_parameters():
                if param.requires_grad:
                    self.writer.add_histogram(name + '-grad', param.grad.detach().cpu().numpy(), self.n_iter)


class TensorBoardTrainLossCallBack(AbstractCallback):
    def __init__(self, log_dir, writer_suffix=''):
        super(TensorBoardTrainLossCallBack, self).__init__()
        self.writer = SummaryWriter(log_dir=log_dir)
        self.writer_suffix = writer_suffix
        self.n_iter = 0

    def post_loss_calculation(self, training_context):
        """
        Called just after the loss calculation step.
        :param training_context: Dict containing information regarding the
            training process.
        """
        cur_batch_loss = training_context['current_loss'].squeeze().detach().tolist()
        self.n_iter += 1
        self.writer.add_scalar('Train-Loss{}'.format(self.writer_suffix),  cur_batch_loss, self.n_iter)


class TensorBoardEvalScalarsCallBack(AbstractCallback):
    def __init__(self,
                 log_dir,
                 evaluation_data_loader_keys,
                 evaluator_keys,
                 class_names,
                 scalar_set_name='metrics'):
        super(TensorBoardEvalScalarsCallBack, self).__init__()
        self.writer = SummaryWriter(log_dir=log_dir)
        self._evaluation_data_loader_keys = evaluation_data_loader_keys
        self._evaluator_keys = evaluator_keys
        self.class_names = class_names
        self.scalar_set_name = scalar_set_name

    def on_evaluation_end(self, training_context):
        """
        Called just after the optimization step.
        :param training_context: Dict containing information regarding the 
            training process.
        """
        current_epoch = training_context['_current_epoch']
        for dl_key in self._evaluation_data_loader_keys:
            for eval_key in self._evaluator_keys:
                # catch and handle multi class metrics
                if 'classwise' in eval_key:
                    classwise_dict = {}
                    for i, class_ in enumerate(self.class_names):
                        classwise_dict[class_] = training_context['_results_history'][-1][dl_key][eval_key].score[i]
                    self.writer.add_scalars('{}_{}_{}'.format(self.scalar_set_name, eval_key, dl_key),  classwise_dict, current_epoch)
                else:
                    scalar = training_context['_results_history'][-1][dl_key][eval_key].score
                    self.writer.add_scalar('{}_{}_{}'.format(self.scalar_set_name, eval_key, dl_key),  scalar, current_epoch)


class TensorBoardEvalCombinedScalarsCallBack(AbstractCallback):
    def __init__(self,
                 log_dir,
                 dataloader_evaluator_dict,
                 evaluator_scaling_dict=None,
                 scalar_set_name='metrics',
                 metrics_set_name=None):
        """
        :param log_dir: The tensorboard log directory path.
        :param dataloader_evaluator_dict: Dictionary with the key as the
            dataloader key and the value as a list of evaluator keys of interest in
            that dataloader.
        :param evaluator_scaling_dict: A dict with the Key of the evaluator to
            be scaled and the scaling factor as the value. Useful when comparing
            disparate metrics like loss versus accuracy.
        :param scalar_set_name: Prefix for the tensorboard chart name.
            Usually the data fold identifier as a string.
        :param metrics_set_name: A name for the tensorboard chart. If not given
            then all the key-value pairs of `dataloader_evaluator_dict` will be
            concatenated and used as the name.
        """
        super(TensorBoardEvalCombinedScalarsCallBack, self).__init__()
        self.writer = SummaryWriter(log_dir=log_dir)
        self._dataloader_evaluator_dict = dataloader_evaluator_dict
        self.scalar_set_name = scalar_set_name
        if metrics_set_name:
            self._combined_name = metrics_set_name
        else:
            combined_scalars_list = []
            for dl_key, eval_keys in self._dataloader_evaluator_dict.items():
                combined_scalars_list += [dl_key+'_'+eval_key for eval_key in eval_keys]

            combined_name = ''
            for _idx in range(len(combined_scalars_list)-1):
                combined_name += combined_scalars_list[_idx] + '_vs_'
            combined_name += combined_scalars_list[len(combined_scalars_list)-1]
            self._combined_name = combined_name
        self._evaluator_scaling_dict = evaluator_scaling_dict

    def on_evaluation_end(self, training_context):
        """
        Called just before the optimization step.
        :param training_context: Dict containing information regarding the
            training process.
        """
        current_epoch = training_context['_current_epoch']
        combined_dict = {}

        for dl_key, eval_keys in self._dataloader_evaluator_dict.items():
            for eval_key in eval_keys:
                _key = dl_key + '_' + eval_key
                combined_dict[_key] = training_context['_results_history'][-1][dl_key][eval_key].score
                if self._evaluator_scaling_dict:
                    if eval_key in self._evaluator_scaling_dict:
                        combined_dict[_key] *= self._evaluator_scaling_dict[eval_key]

        self.writer.add_scalars('{}_{}'.format(self.scalar_set_name, self._combined_name), combined_dict, current_epoch)


class StatsLogCallBack(AbstractCallback):
    def __init__(self, 
                 log_filepath, 
                 evaluation_data_loader_keys, 
                 evaluator_keys, 
                 class_names):
        super(StatsLogCallBack, self).__init__()

        self._evaluation_data_loader_keys = evaluation_data_loader_keys
        self._evaluator_keys = evaluator_keys

        self.class_names = class_names

        if not log_filepath:
            self.log_filepath = os.path.join('.')
        else:
            self.log_filepath = log_filepath

        # opens/creates the log file and then add the header
        self.fill_log_header()

    def fill_log_header(self):
        log_file = open(self.log_filepath, 'w+')

        header = ''
        for dl_key in self._evaluation_data_loader_keys:
            for eval_key in self._evaluator_keys:
                # catch and handle multi class metrics
                if 'classwise' in eval_key:
                    for c_id in self.class_names:
                        header += '{}_{}_{},'.format(dl_key, eval_key, c_id)
                else:
                    header += '{}_{},'.format(dl_key, eval_key)

        header = header[:-1]  # remove last trailing comma

        log_file_header = 'epoch,' + header + '\n'
        log_file.write(log_file_header)
        log_file.close()

    def on_evaluation_end(self, training_context):
        current_epoch = training_context['_current_epoch']

        row_ = ''
        for dl_key in self._evaluation_data_loader_keys:
            for eval_key in self._evaluator_keys:
                # catch and handle multi class metrics
                if 'classwise' in eval_key:            
                    for c_id in range(len(self.class_names)):
                        row_ += '{:4f},'.format(training_context['_results_history'][current_epoch][dl_key][eval_key].score[c_id])
                else:
                    row_ += '{:4f},'.format(training_context['_results_history'][current_epoch][dl_key][eval_key].score)

        row_ = row_[:-1] # remove last trailing comma
        row_ = '{},'.format(current_epoch) + row_ + '\n'

        log_file = open(self.log_filepath, 'a')
        log_file.write(row_)


class PredictionsCallBack(AbstractCallback):
    """
    Predictions Callback.
    Goes in hand with the EvaluatorProbabsLogger. `use_thresh_from_dl_key` is the
    dataloader key to be used for getting thresholds. This threshold will be used
    to threshold predictions on data on all dataloader keys in `evaluation_data_loader_keys`.

    The callback also adds the thresholds, predictions and true labels to the 
    testing context so that it can be used by other callbacks, in such cases please
    ensure `PredictionsCallback` is called before the other dependant callbacks.
    They can be accessed as below with the corresponding probabilities evaluator key
    which is expected to include 'probabs-logger' as a substring in its key;
        testing_context['_results_history'][current_epoch][dl_key][eval_key]['preds']
            - This key is added after thresholding
        testing_context['_results_history'][current_epoch][dl_key][eval_key]['targets']
            - This key will be modified if `use_argmax` is true
        testing_context['_results_history'][current_epoch][dl_key][eval_key]['probabs']
            - This key is already present and does not change
        testing_context['_results_history'][current_epoch]['thresh']
            - This key holds the classwise thresholds if `use_thresh_from_dl_key`,
            `thresh` or `class_wise_thresh` are given by the user

    When selecting thresholding params the following param combos/sets can be used 
        1. use_argmax
        2. use_argmax, use_class_names_pred_column
        3. class_wise_thresh
        4. thresh
        5. use_thresh_from_dl_key
    Only the params in the second set can be used together

    NOTE: Use this callback only with the `Tester` class i.e. when calling `test()` on the corresponding
    `System` object. This is because it expects only a single epoch when reading from results history.
    """

    def __init__(self,  
                 batch_target_key,
                 batch_target_id_key,
                 evaluation_data_loader_keys,
                 evaluator_keys,
                 class_names,
                 use_argmax=False,
                 use_class_names_pred_column=False,
                 thresh=None,
                 class_wise_thresh=[],
                 use_thresh_from_dl_key=None,
                 use_neptune_for_log=False,
                 neptune_log_name=None,
                 log_dir=None,
                 experiment=None):

        super(PredictionsCallBack, self).__init__()

        self._evaluation_data_loader_keys = evaluation_data_loader_keys
        self._evaluator_keys = evaluator_keys
        self._batch_target_key = batch_target_key
        self._batch_target_id_key = batch_target_id_key
        self.class_names = class_names
        self._use_argmax = use_argmax
        self._use_class_names_pred_column = use_class_names_pred_column

        self._class_wise_thresh = class_wise_thresh
        self._thresh = thresh

        if self._thresh and any(self._class_wise_thresh):
            print('Expected either `thresh` or `class_wise_thresh` but recieved both')
        if self._thresh:
            self._class_wise_thresh = [thresh for _ in self.class_names]

        self._use_thresh_from_dl_key = use_thresh_from_dl_key
        if self._use_thresh_from_dl_key:
            self._evaluation_data_loader_keys.remove(self._use_thresh_from_dl_key)
            self._evaluation_data_loader_keys.insert(0, self._use_thresh_from_dl_key)

        self._thresh_from_dl_key_calculated = False
        self._use_neptune_for_log = use_neptune_for_log
        self._neptune_log_name = neptune_log_name

        if not log_dir and not self._use_neptune_for_log:
            self.log_dir = os.path.join('.') 
        else:
            self.log_dir = log_dir

        self.pred_dict = None # init variable to save the predictions as a dict for future use
        self._exp = experiment

    def preprocess_and_thresh(self, dl_key, probabs, targets):
        pred_dict = {}

        if self._use_class_names_pred_column:
            assert self._use_argmax, 'Class names column required but `use_argmax` not True'

        if self._use_argmax:
            [pred_class, pred_args] = argmax_predictions(probabs, self.class_names)
            pred_dict['preds'] = pred_args

            if self._use_class_names_pred_column:
                pred_dict['preds_class'] = pred_class
            return pred_dict


        elif self._use_thresh_from_dl_key and not self._thresh_from_dl_key_calculated:
            assert self._use_thresh_from_dl_key == self._evaluation_data_loader_keys[0], 'First key is different from key for thresholding'
            if dl_key == self._evaluation_data_loader_keys[0]:
                c_thresh, preds = auc_thresholder(y_true=targets,
                                    y_pred=probabs, 
                                    class_names=self.class_names)

                self._class_wise_thresh = c_thresh
                self._thresh_from_dl_key_calculated = True
                pred_dict['preds'] = preds
                return pred_dict

        elif any(self._class_wise_thresh) or self._thresh or self._thresh_from_dl_key_calculated:
            preds = predictions_thresholder(y_pred=probabs, 
                                class_names=self.class_names,
                                class_wise_thresh=self._class_wise_thresh)
            pred_dict['preds'] = preds
            return pred_dict
        else:
            preds = predictions_thresholder(y_pred=probabs, 
                                class_names=self.class_names)
            pred_dict['preds'] = preds
            return pred_dict

    def create_headers(self):
        header_dict = {}
        if self._batch_target_key:
            targets_header = []

            if self._use_argmax:
                targets_header += ['targets']
                if self._use_class_names_pred_column:
                    targets_header += ['targets_class']
                header_dict.update({'targets_header': targets_header})
            else:
                # handle multi-class or multi-label case
                for c_id in self.class_names:
                    targets_header += ['{}_targets'.format(c_id)]
                header_dict.update({'targets_header' : targets_header})

        preds_header = []
        if self._use_argmax:
            preds_header += ['preds']
            if self._use_class_names_pred_column:
                preds_header += ['preds_class']
            header_dict.update({'preds_header': preds_header})

        else:            
            for c_id in self.class_names:
                preds_header += ['{}_preds'.format(c_id)]
            header_dict.update({'preds_header' : preds_header})

        probabs_header = []
        for c_id in self.class_names:
            probabs_header += ['{}_probabs'.format(c_id)]
        header_dict.update({'probabs_header' : probabs_header})

        if self._batch_target_id_key:
            id_header = ['{}'.format(self._batch_target_id_key)] 
            header_dict.update({'id_header':id_header})
        
        return header_dict

    def on_evaluation_end(self, testing_context):
        '''
        `testing_context` is passed since we are working with the `Tester` class. 
        '''
        header_dict = self.create_headers()

        for dl_key in self._evaluation_data_loader_keys:
            for eval_key in self._evaluator_keys:
                if 'probabs-logger' in eval_key:          
                    current_epoch = 0 
                    cat_df = []

                    test_evaluator = testing_context['_results_history'][current_epoch][dl_key][eval_key]
                    probabs = testing_context['_results_history'][current_epoch][dl_key][eval_key]['probabs']   

                    if self._batch_target_id_key :
                        if 'target_ids' in test_evaluator:
                            target_ids = testing_context['_results_history'][current_epoch][dl_key][eval_key]['target_ids']
                            ids_df = pd.DataFrame(target_ids, columns=header_dict['id_header'])
                            cat_df += [ids_df]
                        else:
                            print('No `batch_target_id_key` specified in the evaluator `{}`'.format(eval_key))

                    targets = None # init a targets variable so that its not unbound
                    if self._batch_target_key: 
                        if 'targets' in test_evaluator:
                            targets = testing_context['_results_history'][current_epoch][dl_key][eval_key]['targets']

                            if self._use_argmax:
                                [target_class, target_args] = argmax_predictions(targets, self.class_names)
                                targets = target_args
                                testing_context['_results_history'][current_epoch][dl_key][eval_key]['targets'] = target_args

                                if self._use_class_names_pred_column:
                                    targets = np.column_stack((targets, np.array(target_class)))
                                    
                            targets_df = pd.DataFrame(targets, columns=header_dict['targets_header'])
                            cat_df += [targets_df]
                        else:
                            print('No `batch_target_key` specified in the evaluator `{}`'.format(eval_key))

                    pred_dict = self.preprocess_and_thresh(dl_key, probabs, targets)

                    self.pred_dict = pred_dict

                    if self._use_argmax:                        
                        preds = pred_dict['preds']
                        if self._use_class_names_pred_column:
                            preds = np.column_stack((preds, pred_dict['preds_class']))
                    else:
                        preds = pred_dict['preds']
                    preds_df = pd.DataFrame(preds, columns=header_dict['preds_header'])

                    testing_context['_results_history'][current_epoch][dl_key][eval_key]['preds'] = pred_dict['preds']

                    probabs_df = pd.DataFrame(probabs, columns=header_dict['probabs_header'])
                    cat_df += [preds_df, probabs_df]

                    full_df = pd.concat(cat_df, axis=1, sort=False)

                    if self._use_neptune_for_log:
                        if self._neptune_log_name:
                            log_table(self._neptune_log_name, full_df, experiment=self._exp)
                        else:
                            log_table('{}_{}'.format(dl_key, eval_key), full_df, experiment=self._exp)

                    if self.log_dir:
                        log_fpath = os.path.join(self.log_dir, '{}_{}.csv'.format(dl_key, eval_key))
                        full_df.to_csv(log_fpath)

                    if any(self._class_wise_thresh):
                        testing_context['_results_history'][current_epoch]['thresh'] = self._class_wise_thresh


class LearningRateSchedulerCallback(AbstractCallback):
    """
    A learning rate scheduler callback
    """

    def __init__(self, scheduler, dataloader_key, evaluator_key, use_neptune_for_log=False, neptune_log_prefix=None, experiment=None):
        super(LearningRateSchedulerCallback, self).__init__()
        self._scheduler = scheduler
        self._dataloader_key = dataloader_key
        self._evaluator_key = evaluator_key
        self._use_neptune_for_log = use_neptune_for_log
        self._neptune_log_prefix = neptune_log_prefix
        self._exp = experiment

    def on_evaluation_end(self, training_context):
        """
        Called at the end of the evaluation step.

        :param training_context: Dict containing information regarding the training process.
        """
        last_lr = [group['lr'] for group in self._scheduler.optimizer.param_groups]
        for group_id, lr in enumerate(last_lr):
            if self._use_neptune_for_log:
                if self._neptune_log_prefix:
                    self._exp.log_metric('{}_lr_gp{}'.format(self._neptune_log_prefix, group_id), last_lr[group_id])
                else:
                    print('No prefix given for neptune log. Skipping this.')
            # print('lr : {}'.format(last_lr))

        current_epoch = training_context['_current_epoch']
        if self._evaluator_key:
            scheduler_metric = training_context['_results_history'][-1][self._dataloader_key][self._evaluator_key].score
            self._scheduler.step(scheduler_metric)
        else:
            self._scheduler.step()


class NeptuneEvalScalarsCallback(AbstractCallback):

    def __init__(self,  
                evaluation_data_loader_keys, 
                evaluator_keys, 
                class_names, 
                scalar_set_name='metrics',
                experiment=None):
        super().__init__()
        self._exp = experiment
        self._evaluation_data_loader_keys = evaluation_data_loader_keys
        self._evaluator_keys = evaluator_keys
        self.class_names = class_names
        self.scalar_set_name = scalar_set_name

    def on_training_start(self, training_context):
        if 'try_reset' in training_context:
            if training_context['try_reset']:
                epoch_saved_on = training_context['_current_epoch']

                for dl_key in self._evaluation_data_loader_keys:
                    for eval_key in self._evaluator_keys:    
                        # catch and handle multi class metrics   
                        if 'classwise' in eval_key:
                            for i, class_ in enumerate(self.class_names):
                                cl_metric_name = '{}/{}/{}/{}'.format(self.scalar_set_name, dl_key, eval_key, class_)
                                try:
                                    cl_metric = self._exp.get_numeric_channels_values(cl_metric_name)
                                    cl_log_df = cl_metric[cl_metric['x']<= epoch_saved_on]
                                    self._exp.reset_log(cl_metric_name)

                                    for i in range(len(cl_log_df)):
                                        self._exp.log_metric(cl_metric_name, cl_log_df[cl_metric_name][i])

                                except KeyError:
                                    print('`{}` not in experiment {}'.format(cl_metric_name, self._exp.id))
                                    

                        else:
                            metric_name = '{}/{}/{}'.format(self.scalar_set_name, dl_key, eval_key)
                            try:
                                metric = self._exp.get_numeric_channels_values(metric_name)
                                log_df = metric[metric['x']<= epoch_saved_on]
                                self._exp.reset_log(metric_name)

                                for i in range(len(log_df)):
                                    self._exp.log_metric(metric_name, log_df[metric_name][i])

                            except KeyError:
                                print('`{}` not in experiment {}'.format(metric_name, self._exp.id))

    def on_evaluation_end(self, training_context):
        """
        Called just before the optimization step.
        :param training_context: Dict containing information regarding the training process.
        """

        for dl_key in self._evaluation_data_loader_keys:
            for eval_key in self._evaluator_keys:    
                # catch and handle multi class metrics   
                if 'classwise' in eval_key:
                    for i, class_ in enumerate(self.class_names):
                        class_score = training_context['_results_history'][-1][dl_key][eval_key].score[i]
                        self._exp.log_metric('{}/{}/{}/{}'.format(self.scalar_set_name, dl_key, eval_key, class_), class_score)
                else:
                    scalar = training_context['_results_history'][-1][dl_key][eval_key].score
                    self._exp.log_metric('{}/{}/{}'.format(self.scalar_set_name, dl_key, eval_key), scalar)

                    
class FoldEvalsCallback():
    def __init__(self,  
                class_names, 
                scalar_set_name,
                config_dict,
                eval_keys=None, 
                dl_keys=None):
        
        self._eval_keys = eval_keys
        self._dl_keys = dl_keys
        self._class_names = class_names
        self.scalar_set_name = scalar_set_name
        self.config_dict = config_dict 
        if self._dl_keys and self._eval_keys:
            self.set_dl_eval_keys(self._eval_keys, self._dl_keys)
        self.last_saved_on_fold = 0
        self.save_file = os.path.join(self.config_dict['environ_list']['log_path'], scalar_set_name + '.pkl')

    def check_if_save_exists(self):
        if os.path.exists(self.save_file):
            with open(self.save_file, 'rb') as fp:
                loaded_callback_inst = pickle.load(fp)
            fold_is_correct = self.config_dict['fold_num'] == loaded_callback_inst.last_saved_on_fold

            return fold_is_correct, loaded_callback_inst
        else:
            return True, self

    def set_dl_eval_keys(self, eval_keys, dl_keys):
        self._eval_keys = eval_keys
        self._dl_keys = dl_keys

        self._fold_eval_results = {}
        for dl_key in self._dl_keys:
            self._fold_eval_results[dl_key] = {}
            for eval_key in self._eval_keys:        
                self._fold_eval_results[dl_key][eval_key] = None

    def on_every_fold_end(self, results_history_list):
        for dl_key in self._dl_keys:
            for eval_key in self._eval_keys:  
                if eval_key in results_history_list[0][dl_key]:  
                    if self._fold_eval_results[dl_key][eval_key] is None:
                        self._fold_eval_results[dl_key][eval_key] = 0
                    self._fold_eval_results[dl_key][eval_key] += results_history_list[0][dl_key][eval_key].score

        with open(self.save_file, 'wb') as fp:        
            pickle.dump(self, fp)
        self.last_saved_on_fold += 1

    def on_all_folds_end(self, npt_exp):
        for dl_key in self._dl_keys:
            for eval_key in self._eval_keys:    
                # catch and handle multi class metrics   
                if 'classwise' in eval_key:
                    for i, class_ in enumerate(self.class_names):
                        if eval_key in self._fold_eval_results[dl_key]:
                            if self._fold_eval_results[dl_key][eval_key]:
                                class_score = self._fold_eval_results[dl_key][eval_key][i] / self.config_dict['num_folds']
                                npt_exp.log_metric('{}/{}/{}/{}'.format(self.scalar_set_name, dl_key, eval_key, class_), class_score)
                else:
                    if eval_key in self._fold_eval_results[dl_key]:
                        if self._fold_eval_results[dl_key][eval_key]:
                            scalar = self._fold_eval_results[dl_key][eval_key] / self.config_dict['num_folds']
                            npt_exp.log_metric('{}/{}/{}'.format(self.scalar_set_name, dl_key, eval_key), scalar)


class NeptuneMacroAUCPlotsCallback(AbstractCallback):
    """
    NeptuneMacroAUCPlotsCallback.
    Goes in hand with the EvaluatorProbabsLogger. `

    NOTE: Use this callback only with the `Tester` class i.e. when calling `test()` on the corresponding
    `System` object. This is because it expects only a single epoch when reading from results history.
    """

    def __init__(self,
                 batch_target_key,
                 eval_key,
                 class_names,
                 dl_keys=None,
                 scalar_set_name='metric',
                 experiment=None,
                 use_neptune_for_log=True,
                 log_dir=None,
                 ):
        super(NeptuneMacroAUCPlotsCallback, self).__init__()
        self._exp = experiment
        self._batch_target_key = batch_target_key
        self.class_names = class_names
        self._eval_key = eval_key
        self._scalar_set_name = scalar_set_name
        self._use_neptune_for_log = use_neptune_for_log
        self._dl_keys = dl_keys

        _id = self._exp.id
        if not log_dir and not self._use_neptune_for_log:
            self.log_dir = os.path.join('{}_roc_images'.format(_id))
            makefolder_ifnotexists(self.log_dir) 
        else:
            self.log_dir = log_dir

    def on_evaluation_end(self, testing_context):
        '''
        `testing_context` is passed since we are working with the `Tester` class.
        '''

        for dl_key in self._dl_keys:
            for eval_key in self._eval_key:
                if 'probabs-logger' in eval_key:
                    current_epoch = 0

                    test_evaluator = testing_context['_results_history'][current_epoch][dl_key][eval_key]
                    probabs = testing_context['_results_history'][current_epoch][dl_key][eval_key]['probabs']

                    targets = None # init a targets variable so that its not unbound
                    if self._batch_target_key:
                        if 'targets' in test_evaluator:
                            targets = testing_context['_results_history'][current_epoch][dl_key][eval_key]['targets']
                        else:
                            raise "'No `batch_target_key` specified in the evaluator `{}`'.format(eval_key)"

                    roc_fig, ax = roc_auc_metric(targets, probabs, self.class_names, plot_curves=True)

                    image_name = '{}_{}'.format(self._scalar_set_name, dl_key)
                    if self._use_neptune_for_log:
                        self._exp.log_image(image_name, roc_fig, image_name='roc_curves')

                    if self.log_dir:
                        log_fpath = os.path.join(self.log_dir, image_name + '.png')
                        handles, labels = ax.get_legend_handles_labels()
                        lgd = ax.legend(handles, labels, loc='upper left', bbox_to_anchor=(1.05, 1))
                        roc_fig.savefig(
                            log_fpath,
                            bbox_extra_artists=(lgd,),
                            bbox_inches='tight'
                        ) 

class NeptuneConfusionMatPlotsCallback(AbstractCallback):
    """
    NeptuneConfusionMatPlotsCallback.
    Goes in hand with the EvaluatorProbabsLogger. `

    NOTE: Use this callback only with the `Tester` class i.e. when calling `test()` on the corresponding
    `System` object. This is because it expects only a single epoch when reading from results history.
    """

    def __init__(self,
                 batch_target_key,
                 eval_key,
                 class_names,
                 dl_keys=None,
                 scalar_set_name='cm',
                 experiment=None,
                 use_neptune_for_log=True,
                 log_dir=None,
                 ):
        super(NeptuneConfusionMatPlotsCallback, self).__init__()
        self._exp = experiment
        self._batch_target_key = batch_target_key
        self.class_names = class_names
        self._eval_key = eval_key
        self._scalar_set_name = scalar_set_name
        self._use_neptune_for_log = use_neptune_for_log
        self._dl_keys = dl_keys

        _id = self._exp.id
        if not log_dir and not self._use_neptune_for_log:
            self.log_dir = os.path.join('{}_confMat_images'.format(_id))
            makefolder_ifnotexists(self.log_dir) 
        else:
            self.log_dir = log_dir

    def on_evaluation_end(self, testing_context):
        '''
        `testing_context` is passed since we are working with the `Tester` class.
        '''

        for dl_key in self._dl_keys:
            for eval_key in self._eval_key:
                if 'probabs-logger' in eval_key:
                    current_epoch = 0

                    test_evaluator = testing_context['_results_history'][current_epoch][dl_key][eval_key]
                    preds = test_evaluator['preds']

                    targets = None # init a targets variable so that its not unbound
                    if self._batch_target_key:
                        if 'targets' in test_evaluator:
                            targets = test_evaluator['targets']
                        else:
                            raise "'No `batch_target_key` specified in the evaluator `{}`'.format(eval_key)"

                    np.set_printoptions(precision=2)
                    # Plot non-normalized confusion matrix
                    title, normalize = ("Confusion matrix", None)
                    disp = my_plot_confusion_matrix(preds, targets,
                                                display_labels=self.class_names,
                                                cmap=plt.cm.Blues,
                                                normalize=normalize)
                    disp.ax_.set_title(title)

                    image_name = '{}_{}'.format(self._scalar_set_name, dl_key)
                    if self._use_neptune_for_log:
                        self._exp.log_image(image_name, disp.figure_, image_name='cm')

                    if self.log_dir:
                        log_fpath = os.path.join(self.log_dir, image_name + '.png')
                        disp.figure_.savefig(
                            log_fpath
                        ) 


class SystemCheckpointCallback(AbstractCallback):
    """
    Stops the training process if the results do not get better for a number of epochs.
    This should preferably be the last callback in the callbacks list.
    """

    def __init__(self, patience, tmp_state_filepath):
        """
        :param patience: How many epochs before saving model checkpoint.
        :param tmp_state_filepath: Path where the state of the model checkpoint will be saved.
        """
        self._patience = patience
        self._saved_on_epoch = 0
        self._current_patience = 0
        self._state_filepath = tmp_state_filepath

    def on_evaluation_end(self, training_context):
        self._current_patience += 1
        current_epoch = training_context['_current_epoch']

        if self._current_patience == self._patience:
            self._saved_on_epoch = current_epoch
            self._current_patience = 0

            save_model = training_context['system'].model
                    # saving the model on the current fold
            model_wts = copy.deepcopy(save_model.state_dict())
            save_optim = training_context['optimizer']
            torch.save(
                { 
                    'epoch': self._saved_on_epoch,
                    'model_state_dict': save_model.state_dict(),
                    'optimizer_state_dict': save_optim.state_dict(),
                }, 
                self._state_filepath
            )
            


class SystemCheckpointResetCallback(AbstractCallback):
    """
    Reloads the checkpointed model and optimizer saved by `ModelCheckpointCallback`.
    Needs to be called before all other callbacks. May not work as expected otherwise.
    """

    def __init__(self, tmp_state_filepath, try_reset=True):
        """
        :param patience: How many epochs before saving model checkpoint.
        :param tmp_state_filepath: Path where the state of the model checkpoint will be saved.
        """
        self._state_filepath = tmp_state_filepath
        self.try_reset = try_reset

    def on_training_start(self, training_context):
        if self.try_reset:
            training_context['try_reset'] = True
            try:
                checkpoint = torch.load(self._state_filepath)
                invalid_keys = training_context['system'].model.load_state_dict(checkpoint['model_state_dict'])
                print('Invalid keys during reset loading : '.format(invalid_keys))
                training_context['optimizer'].load_state_dict(checkpoint['optimizer_state_dict'])
                training_context['_current_epoch'] = checkpoint['epoch']                 
            except FileNotFoundError:
                print("No checkpoint file found. Will not reset, starting from scratch again.")
                training_context['try_reset'] = False

    def on_evaluation_end(self, training_context):
        if 'try_reset' in training_context:
            if training_context['try_reset']:
                training_context['try_reset'] = False
                self.try_reset = False
