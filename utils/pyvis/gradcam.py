"""
Created on Thu Oct 26 11:06:51 2017

@author: Utku Ozbulak - github.com/utkuozbulak
Modified by Anand K Nambisan 

More info on Hook call sequence at 
REF :https://discuss.pytorch.org/t/how-the-hook-works/2222/3
"""
from pandas.core.base import NoNewAttributesMixin
from utils.models import model
from PIL import Image
import numpy as np
import torch

from .misc_functions import get_example_params, save_class_activation_images

class GradCamHook():
    def __init__(self, model, target_layer_name):
        self._model = model
        self.target_layer_name = target_layer_name
        self.target_activations = None
        self.target_gradients = None

        self.activations_hook  = None
        self.gradients_hook = None

    def save_gradients(self, grad):
        print('hook called on backward for input tensor')
        self.target_gradients = grad


    def get_activations_and_gradients(self, module, input, output):
        print('Applied hooks on layer input and saved activations')
        self.gradients_hook = input[0].register_hook(self.save_gradients)
        self.target_activations = output
            
    def apply_hooks(self):
        """
            Apply hooks at given layer
        """
        for name, module in self._model.named_modules():            
            if self.target_layer_name == name:
                self.activations_hook = module.register_forward_hook(self.get_activations_and_gradients)
                print('\n\n\n------- Apllied hook to layer `{}` -------\n\n'.format(name))
    
    def remove_hooks(self):
        self.activations_hook.remove()
        self.gradients_hook.remove()


class GradCam():
    """
        Produces class activation map
    """
    def __init__(self, model, target_layer_name, model_output_key):
        self.model = model
        self.model.eval()
        self.target_layer_name = target_layer_name
        self.model_output_key = None
        if model_output_key:
            self.model_output_key = model_output_key

        self.gradcamhook = GradCamHook(self.model, self.target_layer_name)
        self.gradcamhook.apply_hooks()

    def generate_cam(self, input_image, target_class=None):
        # Full forward pass
        # conv_output is the output of convolutions at specified layer
        # model_output is the final output of the model (1, 1000)
        model_output = self.model(input_image)
        model_output = model_output[self.model_output_key]

        if target_class is None:
            target_class = np.argmax(model_output.data.numpy())
        
        # Target for backprop
        one_hot_output = torch.FloatTensor(1, model_output.size()[-1]).zero_()
        one_hot_output[0][target_class] = 1
        # Zero grads
        self.model.zero_grad()
        # Backward pass with specified target
        model_output.backward(gradient=one_hot_output, retain_graph=True)
        # Get hooked gradients
        guided_gradients = self.gradcamhook.target_gradients.data.numpy()[0]
        # Get convolution outputs
        target = self.gradcamhook.target_activations.data.numpy()[0]
        # Get weights from gradients
        weights = np.mean(guided_gradients, axis=(1, 2))  # Take averages for each gradient
        # Create empty numpy array for cam
        cam = np.ones(target.shape[1:], dtype=np.float32)
        # Multiply each weight with its conv output and then, sum
        for i, w in enumerate(weights):
            cam += w * target[i, :, :]
        cam = np.maximum(cam, 0)
        cam = (cam - np.min(cam)) / (np.max(cam) - np.min(cam))  # Normalize between 0-1
        cam = np.uint8(cam * 255)  # Scale between 0-255 to visualize
        cam = np.uint8(Image.fromarray(cam).resize((input_image.shape[2],
                       input_image.shape[3]), Image.ANTIALIAS))/255
        # ^ I am extremely unhappy with this line. Originally resizing was done in cv2 which
        # supports resizing numpy matrices with antialiasing, however,
        # when I moved the repository to PIL, this option was out of the window.
        # So, in order to use resizing with ANTIALIAS feature of PIL,
        # I briefly convert matrix to PIL image and then back.
        # If there is a more beautiful way, do not hesitate to send a PR.

        # You can also use the code below instead of the code line above, suggested by @ ptschandl
        # from scipy.ndimage.interpolation import zoom
        # cam = zoom(cam, np.array(input_image[0].shape[1:])/np.array(cam.shape))

        self.gradcamhook.remove_hooks()
        
        return cam


# if __name__ == '__main__':
#     # Get params
#     target_example = 0  # Snake
#     (original_image, prep_img, target_class, file_name_to_export, pretrained_model) =\
#         get_example_params(target_example)
#     # Grad cam
#     grad_cam = GradCam(pretrained_model, target_layer=11)
#     # Generate cam mask
#     cam = grad_cam.generate_cam(prep_img, target_class)
#     # Save mask
#     save_class_activation_images(original_image, cam, file_name_to_export)
#     print('Grad cam completed')
