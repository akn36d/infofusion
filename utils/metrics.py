from itertools import cycle

import numpy as np
from matplotlib import pyplot as plt
from pytorch_wrapper.evaluators import (AbstractEvaluator,
                                        AbstractEvaluatorResults,
                                        GenericEvaluatorResults)
from numpy import interp
from sklearn.metrics import (accuracy_score, auc, confusion_matrix, f1_score,
                             hamming_loss, precision_score, recall_score,
                             roc_auc_score, roc_curve, ConfusionMatrixDisplay)
from sklearn.utils.multiclass import unique_labels


def my_plot_confusion_matrix(y_pred, y_true, *, labels=None,
                          sample_weight=None, normalize=None,
                          display_labels=None, include_values=True,
                          xticks_rotation='horizontal',
                          values_format=None,
                          cmap='viridis', ax=None, colorbar=True):
    """Plot Confusion Matrix.
    REF : https://github.com/scikit-learn/scikit-learn/blob/95119c13a/sklearn/metrics/_plot/confusion_matrix.py#L165
    Read more in the :ref:`User Guide <confusion_matrix>`.
    Parameters
    ----------
    y_pred : array-like of shape (n_samples,)
    y_true : array-like of shape (n_samples,)
        Target values.
    labels : array-like of shape (n_classes,), default=None
        List of labels to index the matrix. This may be used to reorder or
        select a subset of labels. If `None` is given, those that appear at
        least once in `y_true` or `y_pred` are used in sorted order.
    sample_weight : array-like of shape (n_samples,), default=None
        Sample weights.
    normalize : {'true', 'pred', 'all'}, default=None
        Normalizes confusion matrix over the true (rows), predicted (columns)
        conditions or all the population. If None, confusion matrix will not be
        normalized.
    display_labels : array-like of shape (n_classes,), default=None
        Target names used for plotting. By default, `labels` will be used if
        it is defined, otherwise the unique labels of `y_true` and `y_pred`
        will be used.
    include_values : bool, default=True
        Includes values in confusion matrix.
    xticks_rotation : {'vertical', 'horizontal'} or float, \
                        default='horizontal'
        Rotation of xtick labels.
    values_format : str, default=None
        Format specification for values in confusion matrix. If `None`,
        the format specification is 'd' or '.2g' whichever is shorter.
    cmap : str or matplotlib Colormap, default='viridis'
        Colormap recognized by matplotlib.
    ax : matplotlib Axes, default=None
        Axes object to plot on. If `None`, a new figure and axes is
        created.
    colorbar : bool, default=True
        Whether or not to add a colorbar to the plot.
        .. versionadded:: 0.24
    Returns
    -------
    display : :class:`~sklearn.metrics.ConfusionMatrixDisplay`
    See Also
    --------
    confusion_matrix : Compute Confusion Matrix to evaluate the accuracy of a
        classification.
    ConfusionMatrixDisplay : Confusion Matrix visualization.
    --------
   
    """

    cm = confusion_matrix(y_true, y_pred, sample_weight=sample_weight,
                          labels=labels, normalize=normalize)

    # if display_labels is None:
        # if labels is None:
    display_labels = unique_labels(y_true, y_pred)
        # else:
            # display_labels = labels

    disp = ConfusionMatrixDisplay(confusion_matrix=cm,
                                  display_labels=display_labels)
    return disp.plot(include_values=include_values,
                     cmap=cmap, ax=ax, xticks_rotation=xticks_rotation,
                     values_format=values_format, colorbar=colorbar)

def auc_thresholder(y_true, y_pred, class_names):
    ''' 
    Arguments:
    y_true - numpy array of shape (n_samples, n_classes) containing true one hot labels
    y_pred - numpy array of shape (n_samples, n_classes) containing predicted probabilities 
              for each class (normalized across classes for a probab of 1)
    class_names - list of class_names as string

    Returns:
    y_preds_thresh - the thresholded predictions

    '''
    class_wise_thresh = [ 0 for _ in class_names]

    for i in range(len(class_names)):
        fpr, tpr, threshs = roc_curve(y_true[:, i], y_pred[:, i])
        class_wise_thresh[i] = threshs[np.argmax(tpr - fpr)]
       
    y_preds_thresh = np.zeros_like(y_pred)
    c_thresh = zip(class_wise_thresh, class_names)

    for i,(thr_, _) in enumerate(c_thresh):
        y_preds_thresh[:,i] = (y_pred[:,i] > thr_).astype(np.int)
    
    return class_wise_thresh, y_preds_thresh


# TODO: work without 'class_names' by assigning class numbers instead
def predictions_thresholder(y_pred, class_names, class_wise_thresh=[]):
    ''' 
    Arguments:
    y_pred - numpy array of shape (n_samples, n_classes) containing predicted probabilities 
              for each class (normalized across classes for a probab of 1)
    class_names - list of class_names as string

    Returns:
    y_preds_thresh - the thresholded predictions

    '''
    if any(class_wise_thresh):      
        assert len(class_names) == len(class_wise_thresh), "class wise threshold list length {} not equal to number of classes {}".format(len(class_wise_thresh), len(class_names))
    else:
        print('No thresholds passed using 0.5 for each class as default')
        class_wise_thresh = [0.5 for _ in class_names]
    c_thresh = zip(class_wise_thresh, class_names)
    
    y_preds_thresh = np.zeros_like(y_pred)

    for i,(thr_, _) in enumerate(c_thresh):
        y_preds_thresh[:,i] = (y_pred[:,i] > thr_).astype(np.int)
    
    return y_preds_thresh

def argmax_predictions(y_pred, class_names):
    pred_args = np.argmax(y_pred, axis=1)
    pred_class = [class_names[x] for x in np.nditer(pred_args)]
    return pred_class, pred_args

# TODO: work without 'class_names' by assigning class numbers instead
def accuracy_score_metric(y_true, y_pred, class_names, mode, thresh=0.5, class_wise_thresh=[]):
    ''' 
    Arguments:
    y_true - numpy array of shape (n_samples, n_classes) containing true one hot labels
    y_pred - numpy array of shape (n_samples, n_classes) containing predicted probabilities 
              for each class (normalized across classes for a probab of 1)
    class_names - list of class_names as string
    mode - can be 'class-wise', 'total', 'all'

    Returns:
    acc_scores - dictionay of false positive rate 
    
    in the above dictionary the available keys are 
    (options: 'classwise', 'all', 'total' )

    '''
    acc_scores = {}

    if mode == 'classwise' or mode=='all':
        acc_c = [0 for _ in class_names]
        if any(class_wise_thresh):
            
            if len(class_names) == len(class_wise_thresh):
                c_thresh = zip(class_wise_thresh, class_names)
            else:
                raise ("class wise threshold list length {} not equal to number of classes {}".format(len(class_wise_thresh), len(class_names)))
        else:
            class_wise_thresh = [thresh for _ in class_names]
            c_thresh = zip(class_wise_thresh, class_names)
        
        for i,(thr_, _) in enumerate(c_thresh):
            y_true_ = y_true[:,i].astype(np.int)
            y_pred_ = (y_pred[:,i] > thr_).astype(np.int)
            acc_c[i] = accuracy_score(y_true_, y_pred_, normalize=True)
        
        acc_scores['classwise'] = acc_c
        
    if mode == 'total' or mode == 'all':
        # print('y_pred : {}'.format(y_pred))
        y_pred_ = (y_pred > thresh).astype(np.int)
        acc_scores['total'] = accuracy_score(y_true, y_pred_, normalize=True)

    if mode == 'total':
        return acc_scores['total']
    elif mode == 'classwise':
        return acc_scores['classwise']
    elif mode == 'all':
        return acc_scores    


# REF : https://scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html#sphx-glr-auto-examples-model-selection-plot-roc-py
def roc_auc_metric(y_true, y_pred, class_names, mode='macro', **kwargs):
    ''' 
    Arguments:
    y_true - numpy array of shape (n_samples, n_classes) containing true one hot labels
    y_pred - numpy array of shape (n_samples, n_classes) containing predicted probabilities 
              for each class (normalized across classes for a probab of 1)
    class_names - list of class_names as string

    kwargs:
        plot_curves - (default: False) if True will override everything and return a         
            figure handle to a plot of all roc curves
    Returns:
    fpr - dictionay of false positive rate 
    tpr - dictionary of true positive rate
    roc_auc - dictionary of roc auc values

    in the above dictionary the available keys are 
    (options: 'micro', 'macro', 'classwise', 'all')

    '''
    plot_curves = kwargs.pop('plot_curves', False)
    n_classes = len(class_names)

    fpr = {}
    tpr = {}
    roc_auc = {}

    if mode == 'classwise' or mode == 'macro' or mode=='all' or plot_curves:
        # Compute ROC curve and ROC area for each class
        fpr_c = [ 0 for _ in class_names]
        tpr_c = [ 0 for _ in class_names]
        roc_auc_c = [ 0 for _ in class_names]

        for i in range(len(class_names)):
            fpr_c[i], tpr_c[i], _ = roc_curve(y_true[:, i], y_pred[:, i])
            roc_auc_c[i] = auc(fpr_c[i], tpr_c[i])

        tpr['classwise'] = tpr_c
        fpr['classwise'] = fpr_c
        roc_auc['classwise'] = roc_auc_c

        # First aggregate all false positive rates
        all_fpr = np.unique(np.concatenate([fpr_c[i] for i in range(len(class_names))]))

        # Then interpolate all ROC curves at this points
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(len(class_names)):
            mean_tpr += interp(all_fpr, fpr_c[i], tpr_c[i])

        # Finally average it and compute AUC
        mean_tpr /= n_classes

        fpr["macro"] = all_fpr
        tpr["macro"] = mean_tpr
        roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

    if mode == 'micro' or mode == 'all' or plot_curves:
        # Compute micro-average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(y_true.ravel(), y_pred.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    if plot_curves:
        # Plot all ROC curves
        fig, ax = plt.subplots()
        plt.style.use('seaborn-darkgrid')
        ax.plot(fpr["micro"], tpr["micro"],
                label='micro-average ROC curve (area = {0:0.2f})'
                    ''.format(roc_auc["micro"]),
                color='deeppink', linestyle=':', linewidth=4)

        ax.plot(fpr["macro"], tpr["macro"],
                label='macro-average ROC curve (area = {0:0.2f})'
                    ''.format(roc_auc["macro"]),
                color='navy', linestyle=':', linewidth=4)
        
        # create a color palette
        cmap = plt.get_cmap('jet')
        colors = cmap(np.linspace(0, 1.0, len(class_names)))

        for i in range(n_classes):
            ax.plot(fpr_c[i], tpr_c[i], color=colors[i], lw=2,
                    label='ROC curve of class {0} (area = {1:0.2f})'
                    ''.format(class_names[i], roc_auc_c[i]))

        ax.plot([0, 1], [0, 1], 'k--', lw=2)
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.05])
        ax.set_xlabel('False Positive Rate')
        ax.set_ylabel('True Positive Rate')
        ax.set_title('Some extension of Receiver operating characteristic to multi-class')
        ax.legend(loc='upper left', bbox_to_anchor=(1.05, 1))
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        plt.close()
        return fig, ax

    if mode == 'all':
        return fpr, tpr, roc_auc
    else:
        return fpr[mode], tpr[mode], roc_auc[mode]





# TODO:  Add class based 'is_better_than' and 'compare_to' functions
class GenericMultiTargetEvaluatorResults(AbstractEvaluatorResults):
    """
    Generic evaluator results.
    """

    def __init__(self, score, label='mc-score', num_classes=0, class_names=None):
        """
        :param score: Numeric value that represents the score.
        :param label: String used in the str representation.
        :param class_names: list of strings with the class names
        """

        super(GenericMultiTargetEvaluatorResults, self).__init__()
        self._score = score
        self._label = label
        
        if class_names:
            self._class_names = class_names
            if not (len(self._score) == len(class_names)):
                raise ('length of scores calculated {} and number of classes {} not matching '.format(len(self._score), len(class_names)))
        elif (num_classes>0):
            self._num_classes = num_classes
            if not (len(self._score) == num_classes):
                raise ('length of scores calculated {} and number of classes {} not matching '.format(len(self._score), num_classes))
        else:
            raise ('Need to give one of num_classes or class_names got {} and {}'.format(num_classes, class_names))

    @property
    def score(self):
        return self._score
    
    def is_better_than(self, other_results_object):
        """
        Compares these results with the results of another object.

        :param other_results_object: Object of the same class.
        """

        pass

    def compare_to(self, other_results_object):
        """
        Compares these results with the results of another object.

        :param other_results_object: Object of the same class.
        """

        pass

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        results_str = self._label
        if self._class_names:
            results_str =  self._label + ': \n'
            for i, _class in enumerate(self._class_names):
                results_str += '\t{} : {}\n'.format(_class, self._score[i])
        elif self._num_classes:
            if self._num_clases > 0:
                results_str =  self._label + ': \n'
                for c_idx in range(self._num_classes):
                    results_str += '\t{} : {}\n'.format(c_idx, self._score[c_idx])
        else:
            results_str = self._label + ': {}'.format(self._score)
        return results_str


class MultiTargetAccuracyEvaluator(AbstractEvaluator):
    """
    Multi-Class Accuracy evaluator.
    """

    def __init__(self, model_output_key=None, batch_target_key='target', class_names=None, mode='total', thresh=0.5, class_wise_thresh=[]):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. Leave None
            if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param mode: Type ['classwise', 'total'] the kind of accuracy desired
        :param thresh: the threshold on the predicted probabs to calculate overall accuracy
        :param class_wise_thresh: class wise threshold values can only be used with mode='classwise' or 'all'
        """

        super(MultiTargetAccuracyEvaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._class_names = class_names
        self._mode = mode
        self._thresh = thresh
        self._class_wise_thresh = class_wise_thresh
        self.reset()

    def reset(self):
        self._outputs = []
        self._targets = []

    def step(self, output, batch, last_activation=None):
        if self._model_output_key is not None:
            output = output[self._model_output_key]
        if last_activation is not None:
            if isinstance(last_activation, dict):
                if last_activation[self._model_output_key] is not None:
                    last_activation = last_activation[self._model_output_key]
            output = last_activation(output)
        self._outputs.extend(output.detach().tolist())
        self._targets.extend(batch[self._batch_target_key].detach().tolist())

    def calculate(self):

        if not (self._mode in ['total', 'classwise']):
            raise ValueError("Recieved {} as mode, expected one of ['total', 'classwise']".format(self._mode))
        
        _y_true = np.array(self._targets)
        _y_pred = np.array(self._outputs)

        acc_score = accuracy_score_metric(
                                            y_true=_y_true,
                                            y_pred=_y_pred,
                                            class_names=self._class_names,
                                            mode=self._mode,
                                            thresh=self._thresh,
                                            class_wise_thresh=self._class_wise_thresh
                                        )
        # print('acc_score {} : {}\n\n\n'.format(self._mode, acc_score))

        if self._mode == 'classwise':        
            return GenericMultiTargetEvaluatorResults(
                                                        score=acc_score, 
                                                        label=self._mode+'_acc', 
                                                        class_names=self._class_names
                                                    )
        if self._mode == 'total':
            return GenericEvaluatorResults(
                                            score=acc_score,
                                            label=self._mode + '_acc',
                                            score_format='%5.2f%%',
                                            is_max_better=True
                                        )

class AUROCEvaluator(AbstractEvaluator):
    """
    AUROC evaluator.
    """

    def __init__(self, 
        model_output_key=None, 
        batch_target_key='target', 
        class_names=None, 
        average='macro', 
        sample_weight=None, 
        max_fpr=None, 
        multi_class='raise', 
        labels=None
        ):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. Leave None
            if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param class_names: list of strings with the class names
        :param average: is one of ['macro', 'micro', 'classwise']
        rest of the params are as used in the `roc_auc_score` metric in sklearn
        
        """

        super(AUROCEvaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self.average = average
        self.sample_weight = sample_weight
        self.max_fpr = max_fpr
        self.multi_class = multi_class
        self.labels = labels

        self._class_names = class_names
        self.reset()

    def reset(self):
        self._outputs = []
        self._targets = []

    def step(self, output, batch, last_activation=None):
        if self._model_output_key is not None:
            output = output[self._model_output_key]
        if last_activation is not None:
            if isinstance(last_activation, dict):
                if last_activation[self._model_output_key] is not None:
                    last_activation = last_activation[self._model_output_key]
            output = last_activation(output)
        
        self._outputs.append(output.cpu().detach().numpy())
        self._targets.append(batch[self._batch_target_key].detach().cpu().numpy())

    def calculate(self):
        if self.average == 'classwise':
            _average = None
        else:
            _average = self.average

        try:
            roc_auc = roc_auc_score(
                y_true=np.concatenate(self._targets),
                y_score=np.concatenate(self._outputs),
                average=_average,
                sample_weight=self.sample_weight,
                max_fpr=self.max_fpr,
                multi_class=self.multi_class,
                labels=self.labels
            )
        except ValueError as verr:
            print(verr)
            print('Because of error will set roc_auc_score to 0.')
            roc_auc = 0 
        

        if self.average == 'classwise':        
            return GenericMultiTargetEvaluatorResults(
                                                        score=roc_auc.tolist(), 
                                                        label='_auc', 
                                                        class_names=self._class_names
                                                    )
        else :
            return GenericEvaluatorResults(
                                            score=roc_auc,
                                            label=self.average+'_'+self.multi_class+'_'+'auc',
                                            score_format='%5.2f%%',
                                            is_max_better=True
                                        )

 
class GenericPointWiseMultiTargetLossEvaluator(AbstractEvaluator):
    """
    Adapter that uses an object of a class derived from AbstractLossWrapper to calculate the class-wise loss during evaluation.
    """

    def __init__(
                    self, 
                    loss_wrapper, 
                    label='classwise-loss', 
                    batch_target_key='target', 
                    class_names=None
                ):
        """
        :param loss_wrapper: AbstractLossWrapper object that calculates the loss.
        :param label: Str used as label during printing of the loss.
        :param score_format: Format used for str representation of the loss.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param class_names: list of class names 
        """

        super(GenericPointWiseMultiTargetLossEvaluator, self).__init__()
        self._loss_wrapper = loss_wrapper
        self._label = label
        self._batch_target_key = batch_target_key
        self._class_names = class_names
        self.reset()

    def reset(self):
        self._loss = 0
        self._examples_nb = 0 # tracking number of samples seen in total

    def step(self, output, batch, last_activation=None):
        current_loss = np.array(self._loss_wrapper.calculate_loss(output, batch, None, last_activation).squeeze().detach().tolist()).astype(np.float)
        self._loss += current_loss.sum(axis=0)
        self._examples_nb += batch[self._batch_target_key].shape[0]

    def calculate(self):
        return GenericMultiTargetEvaluatorResults(
                                                    self._loss / self._examples_nb,
                                                    self._label,
                                                    class_names=self._class_names
                                                )


class EvaluatorProbabsLogger(AbstractEvaluator):
    """
    Multi-Class multi-label evaluator output probabilities logger.
    Goes in hand with the `PredictionsCallback` class.
    """

    def __init__(
                    self, 
                    model_output_key=None, 
                    batch_target_key='target', 
                    batch_target_id_key=None,
                    class_names=None,
                ):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. 
            Leave None if the model returns only the predictions
        :param batch_target_key: Key where the dict (batch) contains the target values
        :param batch_target_id_key: Key where the dict (batch) contains the data ids
        :param class_names: list of class names 
        """
        super(EvaluatorProbabsLogger, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._batch_target_id_key = batch_target_id_key
        self._class_names = class_names

        self.reset()

    def reset(self):
        self._outputs = []
        self._targets = []
        self._target_ids = []
        
    def step(self, output, batch, last_activation=None):
        if self._model_output_key is not None:
            output = output[self._model_output_key]
        if last_activation is not None:
            if isinstance(last_activation, dict):
                if last_activation[self._model_output_key] is not None:
                    last_activation = last_activation[self._model_output_key]
            output = last_activation(output)
        
        self._outputs.extend(output.detach().tolist())

        if self._batch_target_key:
            self._targets.extend(batch[self._batch_target_key].detach().tolist())
        if self._batch_target_id_key:
            self._target_ids.extend(batch[self._batch_target_id_key])
    
    def calculate(self):
        preds_dict = {'probabs' : np.array(self._outputs)}
        if self._batch_target_key:
            preds_dict.update({'targets' : np.array(self._targets)})
        if self._batch_target_id_key:
            preds_dict.update({'target_ids' : np.array(self._target_ids)})
            
        return preds_dict

# INFO : https://stats.stackexchange.com/questions/336820/what-is-a-hamming-loss-will-we-consider-it-for-an-imbalanced-binary-classifier
class HammingLossEvaluator(AbstractEvaluator):
    def __init__(self, model_output_key=None, batch_target_key='target', class_names=None, **metric_kwargs):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. 
            Leave None if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param class_names: List of the classnames 
        :param metric_kwargs: Any additional arguments to pass to  the hamming loss function 
            used in sklearn
        """

        super(HammingLossEvaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._class_names = class_names
        self._metric_name = 'hamming-loss'
        self._metric_kwargs = metric_kwargs
        self.reset()

    def reset(self):
        self._outputs = []
        self._targets = []

    def step(self, output, batch, last_activation=None):
        if self._model_output_key is not None:
            output = output[self._model_output_key]
        if last_activation is not None:
            if isinstance(last_activation, dict):
                if last_activation[self._model_output_key] is not None:
                    last_activation = last_activation[self._model_output_key]
            output = last_activation(output)
        self._outputs.extend(output.detach().tolist())
        self._targets.extend(batch[self._batch_target_key].detach().tolist())

    def calculate(self):
        _y_true = np.array(self._targets)
        _y_pred = np.array(self._outputs)
              
        _, _y_pred_threshed=auc_thresholder(y_true=_y_true, y_pred=_y_pred, class_names=self._class_names)
        
        hamming_loss_score = hamming_loss(
                                            y_true=_y_true,
                                            y_pred=_y_pred_threshed,
                                            **self._metric_kwargs
                                        )
        return GenericEvaluatorResults(
                                        score=hamming_loss_score,
                                        label='hamming_loss',
                                        score_format='%5.2f%%',
                                        is_max_better=False
                                    )


class MultiLabelPrecisionEvaluator(AbstractEvaluator):
    """
    Multi-Label Precision evaluator. Uses `auc_thresholder` for each class prediction.
    """

    def __init__(self, model_output_key=None, batch_target_key='target',class_names=None, average='macro', **metric_kwargs):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. Leave None
            if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param average: {‘micro’, ‘macro’, ‘samples’,’weighted’, ‘binary’} or None, default=’binary’
        :param metric_kwargs: Any additional arguments to pass to  the `precision_score` function 
            used in sklearn
        """

        super(MultiLabelPrecisionEvaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._average = average
        self._class_names = class_names
        self._metric_kwargs = metric_kwargs
        self.reset()

    def reset(self):
        self._outputs = []
        self._targets = []

    def step(self, output, batch, last_activation=None):
        if self._model_output_key is not None:
            output = output[self._model_output_key]
        if last_activation is not None:
            output = last_activation(output)
        self._outputs.extend(output.detach().tolist())
        self._targets.extend(batch[self._batch_target_key].detach().tolist())

    def calculate(self):
        _y_true = np.array(self._targets)
        _y_pred = np.array(self._outputs)

        _, _y_pred_threshed=auc_thresholder(y_true=_y_true, y_pred=_y_pred, class_names=self._class_names)
       
        _score = precision_score(
                                            y_true=_y_true,
                                            y_pred=_y_pred_threshed,
                                            average=self._average,
                                            **self._metric_kwargs
                                        )
        return GenericEvaluatorResults(
                                        score=_score,
                                        label='{}-prec-'.format(self._average),
                                        score_format='%5.2f%%',
                                        is_max_better=False
                                    )

class MultiLabelRecallEvaluator(AbstractEvaluator):
    """
    Multi-Label Recall evaluator. Uses `auc_thresholder` for each class prediction.
    """

    def __init__(self, model_output_key=None, batch_target_key='target', class_names=None,average='macro', **metric_kwargs):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. Leave None
            if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param average: {‘micro’, ‘macro’, ‘samples’,’weighted’, ‘binary’} or None, default=’binary’
        :param metric_kwargs: Any additional arguments to pass to  the `recall_score` function 
            used in sklearn
        """

        super(MultiLabelRecallEvaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._class_names = class_names
        self._average = average
        self._metric_kwargs = metric_kwargs
        self.reset()

    def reset(self):
        self._outputs = []
        self._targets = []

    def step(self, output, batch, last_activation=None):
        if self._model_output_key is not None:
            output = output[self._model_output_key]
        if last_activation is not None:
            output = last_activation(output)
        self._outputs.extend(output.detach().tolist())
        self._targets.extend(batch[self._batch_target_key].detach().tolist())

    def calculate(self):
        _y_true = np.array(self._targets)
        _y_pred = np.array(self._outputs)

        _, _y_pred_threshed=auc_thresholder(y_true=_y_true, y_pred=_y_pred, class_names=self._class_names)
       
        _score = recall_score(
            y_true=_y_true,
            y_pred=_y_pred_threshed,
            average=self._average,
            **self._metric_kwargs
        )
        return GenericEvaluatorResults(
            score=_score,
            label='{}-rec-'.format(self._average),
            score_format='%5.2f%%',
            is_max_better=False
        )

class MultiLabelF1Evaluator(AbstractEvaluator):
    """
    Multi-Label F1 evaluator. Uses `auc_thresholder` for each class prediction.
    """

    def __init__(self, model_output_key=None, batch_target_key='target',class_names=None, average='macro', **metric_kwargs):
        """
        :param model_output_key: Key where the dict returned by the model contains the actual predictions. Leave None
            if the model returns only the predictions.
        :param batch_target_key: Key where the dict (batch) contains the target values.
        :param average: {‘micro’, ‘macro’, ‘samples’,’weighted’, ‘binary’} or None, default=’binary’
        :param metric_kwargs: Any additional arguments to pass to  the `f1_score` function 
            used in sklearn
        """

        super(MultiLabelF1Evaluator, self).__init__()
        self._model_output_key = model_output_key
        self._batch_target_key = batch_target_key
        self._class_names = class_names
        self._average = average
        self._metric_kwargs = metric_kwargs
        self.reset()

    def reset(self):
        self._outputs = []
        self._targets = []

    def step(self, output, batch, last_activation=None):
        if self._model_output_key is not None:
            output = output[self._model_output_key]
        if last_activation is not None:
            output = last_activation(output)
        self._outputs.extend(output.detach().tolist())
        self._targets.extend(batch[self._batch_target_key].detach().tolist())

    def calculate(self):
        _y_true = np.array(self._targets)
        _y_pred = np.array(self._outputs)

        _, _y_pred_threshed=auc_thresholder(y_true=_y_true, y_pred=_y_pred, class_names=self._class_names)
       
        _score = f1_score(
                                            y_true=_y_true,
                                            y_pred=_y_pred_threshed,
                                            average=self._average,
                                            **self._metric_kwargs
                                        )
        return GenericEvaluatorResults(
                                        score=_score,
                                        label='{}-f1-'.format(self._average),
                                        score_format='%5.2f%%',
                                        is_max_better=False
                                    )
