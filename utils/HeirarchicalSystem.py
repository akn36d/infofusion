#%%
from __future__ import print_function

import torch
import tqdm
import time
from itertools import cycle
from functools import partial
from collections import OrderedDict
from torch import nn
from tqdm.auto import tqdm as auto_tqdm
from pytorch_wrapper import System 
from torch.utils.data.dataloader import default_collate
from pytorch_wrapper.training_callbacks import AbstractCallback, NumberOfEpochsStoppingCriterionCallback, StoppingCriterionCallback


NCOLS = 80 if auto_tqdm is tqdm.std.tqdm else None

#%%
# class Pipeline(object):
#     def __init__():
# num_base_feats = 32
# base_type = 'efficientnet-b1'
# diag_names = ["MEL","NV","BKL"] 
# feat_names = [
#     'pigment_network', 
#     'dots_globules', 
#     'negative_network', 
#     'streaks_incl_rad_streaming',	                       
#     'milia_like_cyst', 
#     'granularity', 
#     'ker_plugs', 
#     'wobble', 
#     'scale', 
#     'blood', 
#     'atyp_net',
#     'blood_vessels', 
#     'scarlike_regression',
# ]


class HeirarchicalModel(nn.Module):
    def __init__(self, models_dict, output_as_dict=False, device=None):
        super(HeirarchicalModel, self).__init__()
        self.base_model = models_dict['base']
        self.feats_classifier = models_dict['feats']
        self.diags_classifier = models_dict['diags']
        self.output_as_dict = output_as_dict
        self.subsystem = 'standard' # or 'ganlike'
        self._device = device
        self._subsystems = ['standard', 'ganlike']

    def forward(self, x):

        assert self.subsystem in  self._subsystems, " Undefined model subsystem `{}`".format(self.subsystem)
        
        if self.subsystem == 'standard':
            base_feats = self.base_model(x)
            feats_preds = self.feats_classifier(base_feats)
        
            diag_model_input = torch.cat((base_feats ,feats_preds),dim=1)
            diag_preds = self.diags_classifier(diag_model_input)

            if self.output_as_dict:
                return { 'feats' : feats_preds, 'diags': diag_preds}
            else:
                return feats_preds, diag_preds

        elif self.subsystem == 'ganlike':
            base_feats = self.base_model(x)
            feats_preds = self.feats_classifier(base_feats)

            noise_feats = torch.randn(list(feats_preds.size()), device=self._device, requires_grad=False)

            diag_model_input = torch.cat((base_feats , noise_feats),dim=1)
            diag_preds = self.diags_classifier(diag_model_input)

            if self.output_as_dict:
                return { 'feats' : feats_preds, 'diags': diag_preds, 'noise':noise_feats}
            else:
                return feats_preds, diag_preds, noise_feats


class MultiInputModel(nn.Module):
    def __init__(self, models_dict, output_as_dict=False, device=None):
        super(MultiInputModel, self).__init__()
        self.base_model = models_dict['base']
        self.diags_classifier = models_dict['diags']
        self.output_as_dict = output_as_dict
        self.subsystem = 'standard' # or 'ganlike'
        self._device = device
        self._subsystems = ['standard', 'ganlike']

    def forward(self, image, feats):

        assert self.subsystem in self._subsystems ," Undefined model subsystem `{}`".format(self.subsystem)
        
        if self.subsystem == 'standard':
            base_feats = self.base_model(image)
        
            diag_model_input = torch.cat((base_feats ,feats),dim=1)
            diag_preds = self.diags_classifier(diag_model_input)

            if self.output_as_dict:
                return { 'diags': diag_preds}
            else:
                return diag_preds

        elif self.subsystem == 'ganlike':
            base_feats = self.base_model(image)

            noise_feats = torch.randn(list(feats.size()), device=self._device, requires_grad=False)

            diag_model_input = torch.cat((base_feats , noise_feats),dim=1)
            diag_preds = self.diags_classifier(diag_model_input)

            if self.output_as_dict:
                return {'diags': diag_preds}
            else:
                return diag_preds, noise_feats

class MultiOutputModel(nn.Module):
    def __init__(self, models_dict, output_as_dict=False, device=None):
        super(MultiOutputModel, self).__init__()
        self.base_model = models_dict['base']
        self.feats_classifier = models_dict['feats']
        self.diags_classifier = models_dict['diags']
        self.output_as_dict = output_as_dict
        self._device = device

    def forward(self, x):
      
            base_feats = self.base_model(x)
            feats_preds = self.feats_classifier(base_feats)
        
            diag_preds = self.diags_classifier(base_feats)

            if self.output_as_dict:
                return { 'feats' : feats_preds, 'diags': diag_preds}
            else:
                return feats_preds, diag_preds


# class HeirarchicalSystemTrainingScheduleCallback(AbstractCallback):
#     def __init__(self, schedule, num_epochs):
#         """
#         :param schedule: A list with as many entries as epochs
#             with each entry being  a dictionary of the form
#             {
#                 'batch_schedule ': {
#                                         'subsystem1': num_batches1,
#                                         'subsystem2': num_batches2
#                                     },
#                 'alternating_batches':True
#             }
            
#             keys:
#                 batch_schedule : specifies the number of consecutive batches (a batch-set) to train the subsystem
#                     or one pass on that subsystem
#                 alternating_batches : If this is True then each subsystem will be trained on different 
#                     batch-sets (alternating batch sets). If False then the same batch-set will be used for 
#                     all the subsystems in that pass over the subsystems.
#                 NOTE: A pass on a subssytem is defined here as the number of consecutive batches before being used 
#                 to train the subsytem before switching to another subsytem.

#             To get an idea about defining model subsystems look at `HeirarchicalModel`.
            
#         :param num_epochs: Total number of epochs to train. Refrain from using different schedules per epoch 
#             especially if using early stopping mechanisms.
#         NOTE : If entries in schedule are less than num_epochs will start cycling the given schedule starting 
#             from the beginning  
#             if entries in schedule exceed num_epochs they will be truncated
#         """


# 200 * [{'batch_schedule ': {'standard': 1, 'ganlike':1}, 'alternating_batches':True}]








class HeirarchicalSystem(object):
    def __init__(self, 
                 model,
                #  schedule,
                 loss_dict,
                 train_data_loader,
                 evaluation_data_loaders,
                 optimizer,
                 batch_input_key,
                 evaluators_dict,
                 callbacks_dict,
                 npt_expt,
                 fold_num,
                 last_activation=None, 
                 device=torch.device('cpu')
                ):
        
        self._device = device
        self.model = model # HeirarchicalModel(models_dict, output_as_dict=True, device=self._device)
        self.train_data_loader = train_data_loader
        self.batch_input_key = batch_input_key
        self.last_activation = last_activation
        self.npt_expt = npt_expt
        self.loss_dict = loss_dict
        self.fold_num = fold_num

        self.callbacks = callbacks_dict['standard']
        self.model.to(device)
        self.model.train(False)


        _evals = {}
        _evals.update(evaluators_dict['feats'])
        _evals.update(evaluators_dict['diags'])

        # create a System out of model
        self._Sys = System(self.model, last_activation=self.last_activation, device=self._device)

        _eval_callbacks = callbacks_dict['feats'] + callbacks_dict['diags']
        self.evaluation_data_loaders=evaluation_data_loaders
        self.evaluators=_evals
        self.eval_callbacks=_eval_callbacks
        self.evaluate_on_subsystem = 'standard'

        self.training_context = {
            # system
            'system' : self._Sys,
            # stop training
            'stop_training': False,
            # current_epoch
            '_current_epoch': -1,
            # results history
            '_results_history' : [],
            # optimizer
            'optimizer' : optimizer,
            # verbose
            '_verbose' : True
        }

    @property
    def device(self):
        return self._device


    def train(self):

        for callback in self.callbacks:
            callback.on_training_start(self.training_context)
            
        # Train the Model
        while not self.training_context['stop_training']:
            self.training_context['_current_epoch'] += 1

            self.model.train(True)


            for callback in self.callbacks:
                callback.on_epoch_start(self.training_context)

            pre_time = time.time()
            auto_tqdm.write('-' * 70)
            auto_tqdm.write('')
            auto_tqdm.write('Epoch: %d' % (self.training_context['_current_epoch']))
            auto_tqdm.write('')
            auto_tqdm.write('Training...')
            auto_tqdm.write('')

            pbar = auto_tqdm(total=len(self.train_data_loader), ncols=NCOLS)


            cum_pass1_loss = 0 
            cum_pass2_loss = 0 
           
            # create batch-wise loss histories for fakefeats, feats, diags and total loss
            for i, batch in enumerate(self.train_data_loader):
                # -----------------
                #  Train Subsystem 1 
                # -----------------
                self.training_context['optimizer'].zero_grad()

                self.model.subsystem = 'ganlike'
                outputs_pass1 = self.model(batch[self.batch_input_key].to(self.device))
                total_loss_pass1 = self.loss_dict['loss-total-pass1'].calculate_loss(
                    outputs_pass1, 
                    batch, 
                    self.training_context, 
                    last_activation=self.last_activation
                )
                # gradients for diags loss calculated
                total_loss_pass1.backward()
                cum_pass1_loss += total_loss_pass1.item()

                self.training_context['optimizer'].step()

                # -----------------
                #  Train  Subsystem 2 
                # -----------------
                self.training_context['optimizer'].zero_grad()
                self.model.subsystem = 'standard'
                outputs_pass2 = self.model(batch[self.batch_input_key].to(self.device))
                
                total_loss_pass2 = self.loss_dict['loss-total-pass2'].calculate_loss(
                    outputs_pass2, 
                    batch, 
                    self.training_context, 
                    last_activation=self.last_activation
                )

                # gradients for diags loss calculated
                total_loss_pass2.backward()

                cum_pass2_loss += total_loss_pass2.item()

                self.training_context['optimizer'].step()


                pbar.update(1)
                pbar.set_postfix(
                    ordered_dict=OrderedDict(
                        [   
                            ('total_loss_pass1', '{:5.4f}'.format(cum_pass1_loss/(i+1))),       
                            ('total_loss_pass2', '{:5.4f}'.format(cum_pass2_loss/(i+1)))
                        ]
                    )
                )

            pbar.close()
            auto_tqdm.write('Time elapsed: %d' % (time.time() - pre_time))
            auto_tqdm.write('')
            # call callbacks which control flow
            for callback in self.callbacks:
                callback.on_epoch_end(self.training_context)

            # call callbacks which control flow
            for callback in self.callbacks:
                callback.on_evaluation_start(self.training_context)

            self.model.subsystem = self.evaluate_on_subsystem
            self.evaluate_system()


            # call callbacks which control flow
            for callback in self.callbacks:
                callback.on_evaluation_end(self.training_context)

        # call callbacks which come at end of training
        for callback in self.callbacks:
            callback.on_training_end(self.training_context)


    def evaluate_system(self):
        if self.evaluation_data_loaders is not None and self.evaluators is not None:

            if self.training_context['_verbose']:
                auto_tqdm.write('Evaluating...')
                auto_tqdm.write('')

            for callback in self.eval_callbacks:
                callback.on_evaluation_start(self.training_context)

            current_results = {}
            for current_dataset_name in self.evaluation_data_loaders:
                auto_tqdm.write(current_dataset_name)
                current_dataset_results = self._Sys.evaluate(
                    self.evaluation_data_loaders[current_dataset_name],
                    self.evaluators,
                    self.batch_input_key,
                    self.training_context['_verbose']
                )
                current_results[current_dataset_name] = current_dataset_results

            self.training_context['_results_history'].append(current_results)

            for callback in self.eval_callbacks:
                callback.on_evaluation_end(self.training_context)

    def test(self, 
            evaluation_data_loaders,
            batch_input_key,
            evaluators,
            callbacks,
            verbose=False,
        ):

        self._Sys.model.subsytem = self.evaluate_on_subsystem
        return self._Sys.test(
            evaluation_data_loaders,
            batch_input_key,
            evaluators,
            callbacks,
            verbose
        )



class MultiInputSystem(object):
    def __init__(self, 
                 model,
                #  schedule,
                 loss_dict,
                 train_data_loader,
                 evaluation_data_loaders,
                 optimizer,
                 batch_input_keys,
                 evaluators_dict,
                 callbacks_dict,
                 npt_expt,
                 fold_num,
                 training_type='standard',
                 last_activation=None, 
                 device=torch.device('cpu')
                ):
        
        self._device = device      
        self.model = model # MultiInputModel(models_dict, output_as_dict=True, device=self._device)
        self.train_data_loader = train_data_loader
        self.batch_input_keys = batch_input_keys  
        self.last_activation = last_activation
        self.npt_expt = npt_expt
        self.loss_dict = loss_dict
        self.fold_num = fold_num

        self.callbacks = callbacks_dict['standard']
        self.model.to(device)
        self.model.train(False)

        self._training_types = ['standard','ganlike'] # 'standard'

        self.training_type = training_type # 'standard'
        
        assert self.training_type in self._training_types ," Undefined training type `{}`".format(self.training_type)

        _evals = {}
        _evals.update(evaluators_dict['diags'])
        
        # create a System out of model
        # create a System out of model
        self._Sys = System(self.model, last_activation=self.last_activation, device=self._device)

        _eval_callbacks = callbacks_dict['diags']
        self.evaluation_data_loaders=evaluation_data_loaders
        self.evaluators=_evals
        self.eval_callbacks=_eval_callbacks
        self.evaluate_on_subsystem = 'standard'

        self.training_context = {
            # system
            'system' : self._Sys,
            # stop training
            'stop_training': False,
            # current_epoch
            '_current_epoch': -1,
            # results history
            '_results_history' : [],
            # optimizer
            'optimizer' : optimizer,
            # verbose
            '_verbose' : True
        }

    @property
    def device(self):
        return self._device


    def train(self):

        for callback in self.callbacks:
            callback.on_training_start(self.training_context)
            
        # Train the Model
        while not self.training_context['stop_training']:
            self.training_context['_current_epoch'] += 1

            self.model.train(True)


            for callback in self.callbacks:
                callback.on_epoch_start(self.training_context)

            pre_time = time.time()
            auto_tqdm.write('-' * 70)
            auto_tqdm.write('')
            auto_tqdm.write('Epoch: %d' % (self.training_context['_current_epoch']))
            auto_tqdm.write('')
            auto_tqdm.write('Training...')
            auto_tqdm.write('')

            pbar = auto_tqdm(total=len(self.train_data_loader), ncols=NCOLS)


            cum_pass1_loss = 0 
            cum_pass2_loss = 0 
           
            # create batch-wise loss histories for fakefeats, feats, diags and total loss
            for i, batch in enumerate(self.train_data_loader):
                inputs_from_batch = [batch[key]  for key in self.batch_input_keys]
                # -----------------
                #  Train Subsystem 1 
                # -----------------                
                if self.training_type == 'ganlike':
                    self.training_context['optimizer'].zero_grad()

                    self.model.subsystem = 'ganlike'                    
                    outputs_pass1 = self.predict_batch(inputs_from_batch)
                    total_loss_pass1 = self.loss_dict['loss-diags'].calculate_loss(
                        outputs_pass1, 
                        batch, 
                        self.training_context, 
                        last_activation=self.last_activation
                    )

                    # gradients for diags loss calculated
                    total_loss_pass1.backward()
                    cum_pass1_loss += total_loss_pass1.item()

                    self.training_context['optimizer'].step()

                # -----------------
                #  Train  Subsystem 2 
                # -----------------
                self.training_context['optimizer'].zero_grad()
                
                self.model.subsystem = 'standard'
                outputs_pass2 = self.predict_batch(inputs_from_batch)                
                total_loss_pass2 = self.loss_dict['loss-diags'].calculate_loss(
                    outputs_pass2, 
                    batch, 
                    self.training_context, 
                    last_activation=self.last_activation
                )

                # gradients for diags loss calculated
                total_loss_pass2.backward()
                cum_pass2_loss += total_loss_pass2.item()

                self.training_context['optimizer'].step()


                pbar.update(1)
                if self.training_type == 'ganlike':

                    pbar.set_postfix(
                        ordered_dict=OrderedDict(
                            [   
                                ('total_loss_ganlike', '{:5.4f}'.format(cum_pass1_loss/(i+1))),       
                                ('total_loss_standard', '{:5.4f}'.format(cum_pass2_loss/(i+1)))
                            ]
                        )
                    )
                else:
                    pbar.set_postfix(
                        ordered_dict=OrderedDict(
                            [                                  
                                ('total_loss_standard', '{:5.4f}'.format(cum_pass2_loss/(i+1)))
                            ]
                        )
                    )

            pbar.close()
            auto_tqdm.write('Time elapsed: %d' % (time.time() - pre_time))
            auto_tqdm.write('')
            # call callbacks which control flow
            for callback in self.callbacks:
                callback.on_epoch_end(self.training_context)

            # call callbacks which control flow
            for callback in self.callbacks:
                callback.on_evaluation_start(self.training_context)

            self.model.subsystem = self.evaluate_on_subsystem
            self.evaluate_system()


            # call callbacks which control flow
            for callback in self.callbacks:
                callback.on_evaluation_end(self.training_context)

        # call callbacks which come at end of training
        for callback in self.callbacks:
            callback.on_training_end(self.training_context)


    def evaluate_system(self):
        if self.evaluation_data_loaders is not None and self.evaluators is not None:

            if self.training_context['_verbose']:
                auto_tqdm.write('Evaluating...')
                auto_tqdm.write('')

            for callback in self.eval_callbacks:
                callback.on_evaluation_start(self.training_context)

            current_results = {}
            for current_dataset_name in self.evaluation_data_loaders:
                auto_tqdm.write(current_dataset_name)
                current_dataset_results = self.evaluate(
                    self.evaluation_data_loaders[current_dataset_name],
                    self.evaluators,
                    self.batch_input_keys,
                    self.training_context['_verbose']
                )
                current_results[current_dataset_name] = current_dataset_results

            self.training_context['_results_history'].append(current_results)

            for callback in self.eval_callbacks:
                callback.on_evaluation_end(self.training_context)
    

    def evaluate(self, data_loader, evaluators, batch_input_keys=['input1'], verbose=True):
        """
        Evaluates the model on a dataset.

        :param data_loader: DataLoader object that generates batches of the evaluation dataset. Each batch must be a
            Dict that contains the input of the model (key=`batch_input_key`) as well as the information needed by
            the evaluators.
        :param evaluators: Dictionary containing objects derived from AbstractEvaluator. The keys are the evaluators'
            names.
        :param batch_input_key: The key of the batches returned by the data_loader that contains the input of the
            model.
        :param verbose: Whether to print progress info.
        :return: Dict containing an object derived from AbstractEvaluatorResults for each evaluator.
        """

        self.model.train(False)

        for evaluator_name in evaluators:
            evaluators[evaluator_name].reset()

        with torch.no_grad():
            gen = partial(auto_tqdm, ncols=NCOLS) if verbose else lambda x: x
            for i, batch in enumerate(gen(data_loader)):
                
                inputs_from_batch = [batch[key]  for key in batch_input_keys]
                outputs = self.predict_batch(inputs_from_batch)

                for evaluator_name in evaluators:
                    evaluators[evaluator_name].step(outputs, batch, self.last_activation)

        results = {}
        for evaluator_name in evaluators:
            results[evaluator_name] = evaluators[evaluator_name].calculate()

        return results

    def predict_batch(self, single_batch_input):
        """
        Computes the output of the model for a single batch of inputs.
        This works for multi input system as well.
        :param single_batch_input: Tensor or list of Tensors [tensor_1, tensor_2, ...] that correspond to the input of
            the model.
        :return: The output of the model.
        """

        batch_inputs = []

        if type(single_batch_input) is not list and type(single_batch_input) is not tuple:
            single_batch_input = [single_batch_input]

        for batch_input in single_batch_input:
            batch_input = batch_input.to(self._device)
            batch_inputs.append(batch_input)

        return self.model(*batch_inputs)


    def test(self, 
            evaluation_data_loaders,
            batch_input_keys,
            evaluators,
            callbacks,
            verbose=False,
        ):
        _tester = _Tester(
            self,
            evaluation_data_loaders,
            batch_input_keys,
            evaluators,
            callbacks,
            verbose=False,           
        )


        self.model.subsytem = self.evaluate_on_subsystem
        return _tester.run()

    def save(self, f):
        """
        Saves the System to a file.

        :param f: a file-like object (has to implement write and flush) or a string containing a file name.
        """

        torch.save({
            'model': self.model,
            'last_activation': self.last_activation
        }, f)

    @staticmethod
    def load(f):
        """
        Loads a System from a file. The model will reside in the CPU initially.

        :param f: a file-like object (has to implement write and flush) or a string containing a file name.
        """

        loaded_data = torch.load(f, map_location=torch.device('cpu'))
        return System(loaded_data['model'], loaded_data['last_activation'])

    def save_model_state(self, f):
        """
        Saves the model's state to a file.

        :param f: a file-like object (has to implement write and flush) or a string containing a file name.
        """

        if isinstance(self.model, nn.DataParallel):
            model_state = {k[len('module.'):]: v for k, v in self.model.state_dict().items()}
        else:
            model_state = self.model.state_dict()

        torch.save(model_state, f)

    def load_model_state(self, f, strict=True):
        """
        Loads the model's state from a file.

        :param f: a file-like object (has to implement write and flush) or a string containing a file name.
        :param strict: Whether the file must contain exactly the same weight keys as the model.
        :return: NamedTuple with two lists (`missing_keys` and `unexpected_keys`).
        """

        model_state = torch.load(f, map_location=torch.device('cpu'))
        if isinstance(self.model, nn.DataParallel):
            model_state = {'module.' + k: v for k, v in model_state.items()}

        invalid_keys = self.model.load_state_dict(model_state, strict)
        self.model.to(self._device)
        return invalid_keys

class _Tester(object):

    def __init__(self,
                 system,
                 evaluation_data_loaders,
                 batch_input_keys,
                 evaluators,
                 callbacks,
                 verbose,
                 eval_verbose=False):
        """
        Used to test or evaluate the model on a dataset with callbacks.

        :param system: The system object.
        :param evaluation_data_loaders: Dictionary containing the evaluation data-loaders. The keys are the datasets'
            names. Each batch generated by the dataloaders must be a  Dict that contains the input of the model
            (key=`batch_input_key`) as well as the information needed by the `evalurators`.
        :param batch_input_key: Key of the Dicts returned by the Dataloader objects that corresponds to the input of the
            model.
        :param evaluators: Dictionary containing objects derived from AbstractEvaluator. The keys are the evaluators'
            names.
        :param callbacks: List containing TrainingCallback objects. They are used in order to inject functionality at
            several points of the training process. 
        :param verbose: Whether to print progress info.
        :param eval_verbose: Whether to print the evaluateor scores info.
        :return: List containing the results.
        """

        self.testing_context = {

            'system': system,
            # list of all results
            '_results_history': [],
            # verbose
            '_verbose': verbose,
            # eval_verbose
            '_eval_verbose': eval_verbose,
            # epoch for test purposes set to 0
            '_current_epoch': 0
        }

        self.evaluation_data_loaders = evaluation_data_loaders
        self.batch_input_keys = batch_input_keys
        self.evaluators = evaluators
        self.callbacks = callbacks


    def run(self):

        # Test the Model
        self.testing_context['system'].model.train(False)
        self._test_evaluation()
        if self.testing_context['_verbose']:
            auto_tqdm.write('Evaluation Complete...')
            auto_tqdm.write('')
            
        return self.testing_context['_results_history']


    def _test_evaluation(self):
        """
        Evaluates or tests the model.
        """

        if self.evaluation_data_loaders is not None and self.evaluators is not None:

            if self.testing_context['_verbose']:
                auto_tqdm.write('Evaluating...')
                auto_tqdm.write('')

            current_results = {}
            for current_dataset_name in self.evaluation_data_loaders:
                auto_tqdm.write(current_dataset_name)
                current_dataset_results = self.testing_context['system'].evaluate(
                    self.evaluation_data_loaders[current_dataset_name],
                    self.evaluators,
                    self.batch_input_keys,
                    self.testing_context['_verbose']
                )
                current_results[current_dataset_name] = current_dataset_results

                if self.testing_context['_eval_verbose']:
                    for evaluator_name in self.evaluators:
                        auto_tqdm.write(str(current_results[current_dataset_name][evaluator_name]))

            self.testing_context['_results_history'].append(current_results)

            for callback in self.callbacks:
                callback.on_evaluation_end(self.testing_context)



# %%
